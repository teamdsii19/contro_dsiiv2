﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SessionManager
{
    class Permisos
    {
        String idPermiso;
        String idOpcion;
        String idRol;

        public string IdPermiso { get => idPermiso; set => idPermiso = value; }
        public string IdOpcion { get => idOpcion; set => idOpcion = value; }
        public string IdRol { get => idRol; set => idRol = value; }

        public static void Insertar(String idRol,string idOpcion){

            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into permisos(idOpcion, idRol) values("+idOpcion+","+idRol+");");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            int re=oOperacion.Insertar(Sentencia.ToString());
            if (re>0){
                MessageBox.Show("Permiso asignado.");
            }
        }

        public static void Revocar(String id)
        {
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("delete from permisos where idpermisos ="+id);
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            int re = oOperacion.Eliminar(Sentencia.ToString());
            if (re > 0)
            {
                MessageBox.Show("Permiso fue revocado.");
            }
        }

        public static void InsertarRoles(String nombre)
        {
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into roles (rol) value('" +nombre+"');");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            int re = oOperacion.Eliminar(Sentencia.ToString());
            if (re > 0)
            {
                MessageBox.Show("Operación realizada con éxito");
            }
        }

        public static void EditarRoles(String roles,String id)
        {
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("update roles set rol='"+ roles + "' where idrol =" + id);
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            int re = oOperacion.Eliminar(Sentencia.ToString());
            if (re > 0)
            {
                MessageBox.Show("Operación realizada con éxito");
            }
        }

        public static void EliminarRoles(String id)
        {
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("delete from roles where idrol =" + id);
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            int re = oOperacion.Eliminar(Sentencia.ToString());
            if (re > 0)
            {
                MessageBox.Show("Eliminación realizada con éxito");
            }
        }
    }
}
