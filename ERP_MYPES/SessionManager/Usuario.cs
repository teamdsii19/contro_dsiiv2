﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;

namespace SessionManager
{

    public class Usuario
    {
        DataTable _Permisos;
        String _IDUsuario;
        String _Usuario;
        String _IDRol;
        String _IDEmpleado;
        String _Empleado;
        String _Rol;
        byte[] _foto;

        public String Rol
        {
            get { return _Rol; }
            set { _Rol = value; }
        }

        public String Empleado
        {
            get { return _Empleado; }
            set { _Empleado = value; }
        }

        public String IDEmpleado
        {
            get { return _IDEmpleado; }
            set { _IDEmpleado = value; }
        }

        public String IDRol
        {
            get { return _IDRol; }
            set { _IDRol = value; }
        }


        public String NombreUsuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }

        public String IDUsuario
        {
            get { return _IDUsuario; }
            set { _IDUsuario = value; }
        }

        public byte[] Foto { get => _foto; set => _foto = value; }

        private void CargarPermisos()
        {
            _Permisos = CacheManager.SystemCache.PermisosUsuario(_Usuario);
        }

        public void ObtenerInfo()
        {
            CargarPermisos();
        }

		public bool VerificarPermiso(int pIDPermiso){
            bool resultado = false;
            foreach (DataRow fila in _Permisos.Rows){
                if (fila["IDOpcion"].ToString().Equals(pIDPermiso.ToString())){
                    resultado = true;
                    break;
                }
            }

            if (resultado==false){
                MessageBox.Show("El Usuario no tiene permisos para realizar esta accion");
            }
            return resultado;            
		}
    } 
}
