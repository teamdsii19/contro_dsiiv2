﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SessionManager.GUI
{
    public partial class GestionPermisos : Form
    {
        BindingSource _Permisos=new BindingSource();
     //   BindingSource _Permisos = new BindingSource();

        public GestionPermisos()
        {
            InitializeComponent();
        }
        private void CargarRoles(){
            try
            {
                this.comboRoles.DataSource = CacheManager.SystemCache.TODOSLOSROLES(); ;
                this.comboRoles.ValueMember = "idRol";
                this.comboRoles.DisplayMember = "rol";
                this.comboRoles.SelectedIndex = 0;                
            }
            catch { }
        }

        private void GestionPermisos_Load(object sender, EventArgs e)
        {
            this.CargarRoles();
        }

        private void CargarAsignaciones(String idper) {
            //this._Permisos.DataSource = CacheManager.SystemCache.PERMISOSASIGNADOS(idper);
            this.dataGridView1.DataSource = CacheManager.SystemCache.PERMISOSASIGNADOS(idper);
        }

        private void comboRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.CargarAsignaciones(this.comboRoles.SelectedValue.ToString());
            this.dataGridView1.Refresh();
        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(this.dataGridView1.CurrentCell.ColumnIndex == 0) {
                int valor =Convert.ToInt32(this.dataGridView1.CurrentRow.Cells[0].Value);
                Permisos permisos = new Permisos();

                if (valor.Equals(1))
                {
                    this.dataGridView1.CurrentRow.Cells[0].Value = 0;                    
                    this.dataGridView1.Refresh();
                    SessionManager.Permisos.Revocar(this.dataGridView1.CurrentRow.Cells["idAsignado"].Value.ToString());
                }
                else
                {
                    this.dataGridView1.CurrentRow.Cells[0].Value = 1;
                    this.dataGridView1.Refresh();
                    SessionManager.Permisos.Insertar(this.comboRoles.SelectedValue.ToString(),this.dataGridView1.CurrentRow.Cells["idOpcion"].Value.ToString());
                }
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
           EdicionRol edit = new EdicionRol();
            edit.txtID.Visible = false;
            edit.label1.Visible = false;
            edit.Text = "Agregar rol";
            edit.ShowDialog();
            this.CargarRoles();
            this.comboRoles.Refresh();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.comboRoles.SelectedIndex!=-1)
                {
                    if (Convert.ToInt32(this.comboRoles.SelectedValue) > 6)
                    {
                        EdicionRol edit = new EdicionRol();
                        edit.txtID.Text = this.comboRoles.SelectedValue.ToString();
                        edit.txtRol.Text = this.comboRoles.Text.ToString();
                        edit.Text = "Editar rol";
                        edit.ShowDialog();
                        this.CargarRoles();
                    }
                    else
                    {
                        MessageBox.Show("No es permitido editar ROLES POR DEFECTO del sistema. \n Solo válido para roles intróducidos por el usuario.",
                        "RESTRICION DE EDICION", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            catch { }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

            try
            {
                if (this.comboRoles.SelectedIndex != -1)
                {
                    if (Convert.ToInt32(this.comboRoles.SelectedValue) > 6)
                    {
                        Permisos.EliminarRoles(this.comboRoles.SelectedValue.ToString());
                        this.CargarRoles();
                    }
                    else
                    {
                        MessageBox.Show("No es permitido eliminar ROLES POR DEFECTO del sistema. \n Solo válido para roles intróducidos por el usuario.",
                        "RESTRICION DE ELIMINACION", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            catch { }
        }
    }
}
