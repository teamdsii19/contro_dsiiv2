﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SessionManager.GUI
{
    public partial class EdicionRol : Form
    {
        public EdicionRol()
        {
            InitializeComponent();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (txtID.TextLength==0){
                Permisos.InsertarRoles(this.txtRol.Text);
                this.Close();
            }
            else {
                Permisos.EditarRoles(this.txtRol.Text,this.txtID.Text);
                this.Close();
            }
        }
    }
}
