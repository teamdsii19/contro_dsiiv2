﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CacheManager;
using ModernGUI_V3.GUI;

namespace ModernGUI_V3
{
    public partial class FormPrincipal : Form
    {
        SessionManager.Sesion _SESION = SessionManager.Sesion.Instancia;
        //Capturar posicion y tamaño antes de maximizar para restaurar
        int lx, ly;
        int sw, sh;
        public FormPrincipal()
        {
            InitializeComponent();
        }

        private void FormPrincipal_Load(object sender, EventArgs e)
        {
            _SESION.OUsuario.ObtenerInfo();
            AbrirFormulario<Home>();
            btnHome.BackColor = Color.FromArgb(12, 61, 92);
            this.lbUser.Text=_SESION.OUsuario.NombreUsuario;
            if(_SESION.OUsuario.Foto != null){
            //this.pbUser.Image = Image.FromStream(Operaciones.PrepararImagenParaMostrar(_SESION.OUsuario.Foto));
                //            Operaciones.MostrarImagen(this.pbUser,_SESION.OUsuario.Foto);
            }
            lx = this.Location.X;
            ly = this.Location.Y;
            sw = this.Size.Width;
            sh = this.Size.Height;
            btnMaximizar.Visible = false;
            btnRestaurar.Visible = true;
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;            
        }

        #region Funcionalidades del formulario
        //RESIZE METODO PARA REDIMENCIONAR/CAMBIAR TAMAÑO A FORMULARIO EN TIEMPO DE EJECUCION ----------------------------------------------------------
        private int tolerance = 12;
        private const int WM_NCHITTEST = 132;
        private const int HTBOTTOMRIGHT = 17;
        private Rectangle sizeGripRectangle;

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_NCHITTEST:
                    base.WndProc(ref m);
                    var hitPoint = this.PointToClient(new Point(m.LParam.ToInt32() & 0xffff, m.LParam.ToInt32() >> 16));
                    if (sizeGripRectangle.Contains(hitPoint))
                        m.Result = new IntPtr(HTBOTTOMRIGHT);
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }
        //----------------DIBUJAR RECTANGULO / EXCLUIR ESQUINA PANEL 
        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            var region = new Region(new Rectangle(0, 0, this.ClientRectangle.Width, this.ClientRectangle.Height));

            sizeGripRectangle = new Rectangle(this.ClientRectangle.Width - tolerance, this.ClientRectangle.Height - tolerance, tolerance, tolerance);

            region.Exclude(sizeGripRectangle);
            this.panelContenedor.Region = region;
            this.Invalidate();
        }
        //----------------COLOR Y GRIP DE RECTANGULO INFERIOR
        protected override void OnPaint(PaintEventArgs e)
        {
            SolidBrush blueBrush = new SolidBrush(Color.FromArgb(244, 244, 244));
            e.Graphics.FillRectangle(blueBrush, sizeGripRectangle);

            base.OnPaint(e);
            ControlPaint.DrawSizeGrip(e.Graphics, Color.Transparent, sizeGripRectangle);
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        
        private void btnMaximizar_Click(object sender, EventArgs e)
        {
            lx = this.Location.X;
            ly = this.Location.Y;
            sw = this.Size.Width;
            sh = this.Size.Height;
            btnMaximizar.Visible = false;
            btnRestaurar. Visible = true;
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
        }

        private void btnRestaurar_Click(object sender, EventArgs e)
        {
            btnMaximizar.Visible = true ;
            btnRestaurar.Visible = false;
            this.Size = new Size(sw,sh);
            this.Location = new Point(lx,ly);
        }

        private void panelBarraTitulo_MouseMove(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        //METODO PARA ARRASTRAR EL FORMULARIO---------------------------------------------------------------------
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void BtnHome_Click(object sender, EventArgs e)
        {
            AbrirFormulario<Home>();
            this.ResetColorBoton(1);
        }

        private void BtnVentas_Click(object sender, EventArgs e)
        {
            AbrirFormulario<Ventas>();        
            this.ResetColorBoton(2);
        }

        private void ResetColorBoton(int posicion){
            switch (posicion) {
                case 1: {
                        btnHome.BackColor = Color.FromArgb(12, 61, 92);
                        btnVentas.BackColor = Color.FromArgb(4, 41, 68);
                        btnPedidos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEnvios.BackColor = Color.FromArgb(4, 41, 68);
                        btnInventario.BackColor = Color.FromArgb(4, 41, 68);
                        btnCompras.BackColor = Color.FromArgb(4, 41, 68);
                        btnClientes.BackColor = Color.FromArgb(4, 41, 68);
                        btnGastos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEmpleado.BackColor = Color.FromArgb(4, 41, 68);
                        btnPagos.BackColor = Color.FromArgb(4, 41, 68);
                        btnReportes.BackColor = Color.FromArgb(4, 41, 68);
                        btnFactura.BackColor = Color.FromArgb(4, 41, 68);
                        btnUsuarios.BackColor = Color.FromArgb(4, 41, 68);
                    }
                    break;
                case 2:
                    {
                        btnHome.BackColor = Color.FromArgb(4, 41, 68);
                        btnVentas.BackColor = Color.FromArgb(12, 61, 92);
                        btnPedidos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEnvios.BackColor = Color.FromArgb(4, 41, 68);
                        btnInventario.BackColor = Color.FromArgb(4, 41, 68);
                        btnCompras.BackColor = Color.FromArgb(4, 41, 68);
                        btnClientes.BackColor = Color.FromArgb(4, 41, 68);
                        btnGastos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEmpleado.BackColor = Color.FromArgb(4, 41, 68);
                        btnPagos.BackColor = Color.FromArgb(4, 41, 68);
                        btnReportes.BackColor = Color.FromArgb(4, 41, 68);
                        btnFactura.BackColor = Color.FromArgb(4, 41, 68);
                        btnUsuarios.BackColor = Color.FromArgb(4, 41, 68);
                    }
                    break;
                case 3:
                    {
                        btnHome.BackColor = Color.FromArgb(4, 41, 68);
                        btnVentas.BackColor = Color.FromArgb(4, 41, 68);
                        btnPedidos.BackColor = Color.FromArgb(12, 61, 92);
                        btnEnvios.BackColor = Color.FromArgb(4, 41, 68);
                        btnInventario.BackColor = Color.FromArgb(4, 41, 68);
                        btnCompras.BackColor = Color.FromArgb(4, 41, 68);
                        btnClientes.BackColor = Color.FromArgb(4, 41, 68);
                        btnGastos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEmpleado.BackColor = Color.FromArgb(4, 41, 68);
                        btnPagos.BackColor = Color.FromArgb(4, 41, 68);
                        btnReportes.BackColor = Color.FromArgb(4, 41, 68);
                        btnFactura.BackColor = Color.FromArgb(4, 41, 68);
                        btnUsuarios.BackColor = Color.FromArgb(4, 41, 68);
                    }
                    break;
                case 4:
                    {
                        btnHome.BackColor = Color.FromArgb(4, 41, 68);
                        btnVentas.BackColor = Color.FromArgb(4, 41, 68);
                        btnPedidos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEnvios.BackColor = Color.FromArgb(12, 61, 92);
                        btnInventario.BackColor = Color.FromArgb(4, 41, 68);
                        btnCompras.BackColor = Color.FromArgb(4, 41, 68);
                        btnClientes.BackColor = Color.FromArgb(4, 41, 68);
                        btnGastos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEmpleado.BackColor = Color.FromArgb(4, 41, 68);
                        btnPagos.BackColor = Color.FromArgb(4, 41, 68);
                        btnReportes.BackColor = Color.FromArgb(4, 41, 68);
                        btnFactura.BackColor = Color.FromArgb(4, 41, 68);
                        btnUsuarios.BackColor = Color.FromArgb(4, 41, 68);

                    }
                    break;
                case 5:
                    {
                        btnHome.BackColor = Color.FromArgb(4, 41, 68);
                        btnVentas.BackColor = Color.FromArgb(4, 41, 68);
                        btnPedidos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEnvios.BackColor = Color.FromArgb(4, 41, 68);
                        btnInventario.BackColor = Color.FromArgb(12, 61, 92);
                        btnCompras.BackColor = Color.FromArgb(4, 41, 68);
                        btnClientes.BackColor = Color.FromArgb(4, 41, 68);
                        btnGastos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEmpleado.BackColor = Color.FromArgb(4, 41, 68);
                        btnPagos.BackColor = Color.FromArgb(4, 41, 68);
                        btnReportes.BackColor = Color.FromArgb(4, 41, 68);
                        btnFactura.BackColor = Color.FromArgb(4, 41, 68);
                        btnUsuarios.BackColor = Color.FromArgb(4, 41, 68);

                    }
                    break;

                case 6:
                    {
                        btnHome.BackColor = Color.FromArgb(4, 41, 68);
                        btnVentas.BackColor = Color.FromArgb(4, 41, 68);
                        btnPedidos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEnvios.BackColor = Color.FromArgb(4, 41, 68);
                        btnInventario.BackColor = Color.FromArgb(4, 41, 68);
                        btnCompras.BackColor = Color.FromArgb(12, 61, 92);
                        btnClientes.BackColor = Color.FromArgb(4, 41, 68);
                        btnGastos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEmpleado.BackColor = Color.FromArgb(4, 41, 68);
                        btnPagos.BackColor = Color.FromArgb(4, 41, 68);
                        btnReportes.BackColor = Color.FromArgb(4, 41, 68);
                        btnFactura.BackColor = Color.FromArgb(4, 41, 68);
                        btnUsuarios.BackColor = Color.FromArgb(4, 41, 68);

                    }
                    break;
                case 7:
                    {
                        btnHome.BackColor = Color.FromArgb(4, 41, 68);
                        btnVentas.BackColor = Color.FromArgb(4, 41, 68);
                        btnPedidos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEnvios.BackColor = Color.FromArgb(4, 41, 68);
                        btnInventario.BackColor = Color.FromArgb(4, 41, 68);
                        btnCompras.BackColor = Color.FromArgb(4, 41, 68);
                        btnClientes.BackColor = Color.FromArgb(12, 61, 92);
                        btnGastos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEmpleado.BackColor = Color.FromArgb(4, 41, 68);
                        btnPagos.BackColor = Color.FromArgb(4, 41, 68);
                        btnReportes.BackColor = Color.FromArgb(4, 41, 68);
                        btnFactura.BackColor = Color.FromArgb(4, 41, 68);
                        btnUsuarios.BackColor = Color.FromArgb(4, 41, 68);

                    }
                    break;
                case 8:
                    {
                        btnHome.BackColor = Color.FromArgb(4, 41, 68);
                        btnVentas.BackColor = Color.FromArgb(4, 41, 68);
                        btnPedidos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEnvios.BackColor = Color.FromArgb(4, 41, 68);
                        btnInventario.BackColor = Color.FromArgb(4, 41, 68);
                        btnCompras.BackColor = Color.FromArgb(4, 41, 68);
                        btnClientes.BackColor = Color.FromArgb(4, 41, 68);
                        btnGastos.BackColor = Color.FromArgb(12, 61, 92);
                        btnEmpleado.BackColor = Color.FromArgb(4, 41, 68);
                        btnPagos.BackColor = Color.FromArgb(4, 41, 68);
                        btnReportes.BackColor = Color.FromArgb(4, 41, 68);
                        btnFactura.BackColor = Color.FromArgb(4, 41, 68);
                        btnUsuarios.BackColor = Color.FromArgb(4, 41, 68);

                    }
                    break;
                case 9:
                    {
                        btnHome.BackColor = Color.FromArgb(4, 41, 68);
                        btnVentas.BackColor = Color.FromArgb(4, 41, 68);
                        btnPedidos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEnvios.BackColor = Color.FromArgb(4, 41, 68);
                        btnInventario.BackColor = Color.FromArgb(4, 41, 68);
                        btnCompras.BackColor = Color.FromArgb(4, 41, 68);
                        btnClientes.BackColor = Color.FromArgb(4, 41, 68);
                        btnGastos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEmpleado.BackColor = Color.FromArgb(12, 61, 92);
                        btnPagos.BackColor = Color.FromArgb(4, 41, 68);
                        btnReportes.BackColor = Color.FromArgb(4, 41, 68);
                        btnFactura.BackColor = Color.FromArgb(4, 41, 68);
                        btnUsuarios.BackColor = Color.FromArgb(4, 41, 68);

                    }
                    break;
                case 10:
                    {
                        btnHome.BackColor = Color.FromArgb(4, 41, 68);
                        btnVentas.BackColor = Color.FromArgb(4, 41, 68);
                        btnPedidos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEnvios.BackColor = Color.FromArgb(4, 41, 68);
                        btnInventario.BackColor = Color.FromArgb(4, 41, 68);
                        btnCompras.BackColor = Color.FromArgb(4, 41, 68);
                        btnClientes.BackColor = Color.FromArgb(4, 41, 68);
                        btnGastos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEmpleado.BackColor = Color.FromArgb(4, 41, 68);
                        btnPagos.BackColor = Color.FromArgb(12, 61, 92);
                        btnReportes.BackColor = Color.FromArgb(4, 41, 68);
                        btnFactura.BackColor = Color.FromArgb(4, 41, 68);
                        btnUsuarios.BackColor = Color.FromArgb(4, 41, 68);

                    }
                    break;

                case 11:
                    {
                        btnHome.BackColor = Color.FromArgb(4, 41, 68);
                        btnVentas.BackColor = Color.FromArgb(4, 41, 68);
                        btnPedidos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEnvios.BackColor = Color.FromArgb(4, 41, 68);
                        btnInventario.BackColor = Color.FromArgb(4, 41, 68);
                        btnCompras.BackColor = Color.FromArgb(4, 41, 68);
                        btnClientes.BackColor = Color.FromArgb(4, 41, 68);
                        btnGastos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEmpleado.BackColor = Color.FromArgb(4, 41, 68);
                        btnPagos.BackColor = Color.FromArgb(4, 41, 68);
                        btnReportes.BackColor = Color.FromArgb(12, 61, 92);
                        btnFactura.BackColor = Color.FromArgb(4, 41, 68);
                        btnUsuarios.BackColor = Color.FromArgb(4, 41, 68);

                    }
                    break;
                case 12:
                    {
                        btnHome.BackColor = Color.FromArgb(4, 41, 68);
                        btnVentas.BackColor = Color.FromArgb(4, 41, 68);
                        btnPedidos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEnvios.BackColor = Color.FromArgb(4, 41, 68);
                        btnInventario.BackColor = Color.FromArgb(4, 41, 68);
                        btnCompras.BackColor = Color.FromArgb(4, 41, 68);
                        btnClientes.BackColor = Color.FromArgb(4, 41, 68);
                        btnGastos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEmpleado.BackColor = Color.FromArgb(4, 41, 68);
                        btnPagos.BackColor = Color.FromArgb(4, 41, 68);
                        btnReportes.BackColor = Color.FromArgb(4, 41, 68);
                        btnFactura.BackColor = Color.FromArgb(12, 61, 92);
                        btnUsuarios.BackColor = Color.FromArgb(4, 41, 68);

                    }
                    break;
                case 13:
                    {
                        btnHome.BackColor = Color.FromArgb(4, 41, 68);
                        btnVentas.BackColor = Color.FromArgb(4, 41, 68);
                        btnPedidos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEnvios.BackColor = Color.FromArgb(4, 41, 68);
                        btnInventario.BackColor = Color.FromArgb(4, 41, 68);
                        btnCompras.BackColor = Color.FromArgb(4, 41, 68);
                        btnClientes.BackColor = Color.FromArgb(4, 41, 68);
                        btnGastos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEmpleado.BackColor = Color.FromArgb(4, 41, 68);
                        btnPagos.BackColor = Color.FromArgb(4, 41, 68);
                        btnReportes.BackColor = Color.FromArgb(4, 41, 68);
                        btnFactura.BackColor = Color.FromArgb(4, 41, 68);
                        btnUsuarios.BackColor = Color.FromArgb(12, 61, 92);
                    }
                    break;
                default:
                    {
                        btnHome.BackColor = Color.FromArgb(12, 61, 92);
                        btnVentas.BackColor = Color.FromArgb(4, 41, 68);
                        btnPedidos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEnvios.BackColor = Color.FromArgb(4, 41, 68);
                        btnInventario.BackColor = Color.FromArgb(4, 41, 68);
                        btnCompras.BackColor = Color.FromArgb(4, 41, 68);
                        btnClientes.BackColor = Color.FromArgb(4, 41, 68);
                        btnGastos.BackColor = Color.FromArgb(4, 41, 68);
                        btnEmpleado.BackColor = Color.FromArgb(4, 41, 68);
                        btnPagos.BackColor = Color.FromArgb(4, 41, 68);
                        btnReportes.BackColor = Color.FromArgb(4, 41, 68);
                        btnFactura.BackColor = Color.FromArgb(4, 41, 68);
                        btnUsuarios.BackColor = Color.FromArgb(4, 41, 68);
                    }
                    break;
            }
        }

        private void BtnPedidos_Click(object sender, EventArgs e)
        {
            AbrirFormulario<GestionPedidos>();
            this.ResetColorBoton(3);
        }


        private void btnEmpleado_Click(object sender, EventArgs e)
        {
            AbrirFormulario<GestionarEmpleados>();
            this.ResetColorBoton(9);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnEnvios_Click(object sender, EventArgs e)
        {
            AbrirFormulario<GestionEnvios>();
            this.ResetColorBoton(4);
        }

        private void btnInventario_Click(object sender, EventArgs e)
        {
            AbrirFormulario<GestionProductos>();
            this.ResetColorBoton(5);
        }

        private void btnCompras_Click(object sender, EventArgs e)
        {
            AbrirFormulario<GestionCompras>();
            this.ResetColorBoton(6);
        }

        private void btnClientes_Click(object sender, EventArgs e)
        {
            AbrirFormulario<GestionCliente>();
            this.ResetColorBoton(7);
        }

        private void btnGastos_Click(object sender, EventArgs e)
        {
            AbrirFormulario<GestionGastos>();
            this.ResetColorBoton(8);
        }

        private void btnPagos_Click(object sender, EventArgs e)
        {
            AbrirFormulario<GestionPanillasPagos>();
            this.ResetColorBoton(10);
        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            AbrirFormulario<GestionReportes>();
            this.ResetColorBoton(11);
        }

        private void btnFactura_Click(object sender, EventArgs e)
        {
            AbrirFormulario<GestionFacturas>();
            this.ResetColorBoton(12);
        }

        private void btnUsuarios_Click(object sender, EventArgs e)
        {
            this.ResetColorBoton(13);
            AbrirFormulario<GestionUsuarios>();
            SessionManager.GUI.GestionPermisos permisos = new SessionManager.GUI.GestionPermisos();
            permisos.ShowDialog();

        }

        private void FormPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
        }
        #endregion
        //METODO PARA ABRIR FORMULARIOS DENTRO DEL PANEL
        private void AbrirFormulario<MiForm>() where MiForm : Form, new() {
            Form formulario;
            formulario = panelformularios.Controls.OfType<MiForm>().FirstOrDefault();//Busca en la colecion el formulario
            //si el formulario/instancia no existe
            if (formulario == null)
            {
                formulario = new MiForm();
                formulario.TopLevel = false;
                formulario.FormBorderStyle = FormBorderStyle.None;
                formulario.Dock = DockStyle.Fill;
                panelformularios.Controls.Add(formulario);
                panelformularios.Tag = formulario;
                formulario.Show();
                formulario.BringToFront();
                formulario.FormClosed += new FormClosedEventHandler(CloseForms);
            }
            //si el formulario/instancia existe
            else {
                formulario.BringToFront();
            }
        }
        private void CloseForms(object sender,FormClosedEventArgs e) {
            if (Application.OpenForms["Home"] == null) 
            btnHome.BackColor = Color.FromArgb(4, 41, 68);
            if (Application.OpenForms["GestionarEmpleados"] == null)
            btnEmpleado.BackColor = Color.FromArgb(4, 41, 68);
        }
    }
}
