﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CacheManager;
using ModernGUI_V3.EDC;

namespace ModernGUI_V3.GUI
{
    public partial class Ventas : Form
    {
        ArrayList detalles = new ArrayList();
        String entrada = "";
        bool VentaDesdeHome = false;
        BindingSource _tabla = new BindingSource();
        private static DataTable tablaProductos;
        //   private bool Nueva = false;
        public static int idVenta;

        public static DataTable TablaProductos { get => tablaProductos; set => tablaProductos = value; }


        //        ArrayList AClientes;
        public Ventas()
        {
            InitializeComponent();
        }
        public Ventas(String idProducto)
        {
            InitializeComponent();
            entrada = idProducto;
            this.PanelTop.Dispose();
            this.VentaDesdeHome = true;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            EDC.VentaEdicion ventas = new EDC.VentaEdicion();
            ventas.ShowDialog();
            this.dtgvDetalles.Refresh();
        }
        public void subtotal()
        {
            int fila = this.dtgvDetalles.RowCount;
            double cant = 0;
            Double pre = 0, res = 0;
            try
            {
                Double IVA = 0.13;
                Double e, g, s;
                e = this.Conversion(txtExento.Text);
                g = this.Conversion(txtGravado.Text);
                s = this.Conversion(txtSujeto.Text);
                for (int i = 0; i < fila; i++)
                {
                    cant = Convert.ToInt32(dtgvDetalles.Rows[i].Cells["Cantidad"].Value);
                    pre = Convert.ToDouble(dtgvDetalles.Rows[i].Cells["Precio"].Value);
                    res += Math.Round(cant * pre, 2);
                }
                //                res=Math.Ceiling(res);
                double des = Descuento();
                this.txtDescuentos.Text = des.ToString();
                this.txtSubtotal.Text = res.ToString();
                txtSujeto.Text = res.ToString();
                txtGravado.Text = (res / (1 + IVA)).ToString();
                this.txtIVA.Text = (res * IVA).ToString();
                /*                res -= e;
                                res -= g;
                                res -= s;
                                res -= des;*/
                txtTotal.Text = res.ToString();
            }
            catch
            {
                txtSubtotal.Text = "0";
            }
        }

        private double Descuento()
        {
            int fila = this.dtgvDetalles.RowCount;
            double cant = 0;
            Double pre = 0, res = 0;
            try
            {
                for (int i = 0; i < fila; i++)
                {
                    cant = Convert.ToInt32(dtgvDetalles.Rows[i].Cells["Cantidad"].Value);
                    pre = Convert.ToDouble(dtgvDetalles.Rows[i].Cells["Descuento"].Value);
                    res += Math.Round(cant * pre, 2);
                }
            }
            catch
            {
                txtSubtotal.Text = "0";
            }
            return res;
        }
        // FIN DE FUNCIONES PARA EL MANEJO DEL MONEY

        public void AgregarProductosATabla()
        {
            if (this.dtgvDetalles.Rows.Count > 0)
            {
                if (this.dtgvDetalles.CurrentRow.Index != -1)
                {
                    Int32 actual = Convert.ToInt32(this.dtgvDetalles.CurrentRow.Cells[3].Value);
                    actual += 1;
                    tablaProductos.Rows[this.dtgvDetalles.CurrentRow.Index][3] = actual;
                    String idPro = "";
                    idPro = dtgvDetalles.CurrentRow.Cells[0].Value.ToString();
                    //    MessageBox.Show(idPro);
                    int actualBD = Convert.ToInt32(CLS.Ordenes.TraerExistencias(idPro));
                    actualBD--;
                    CLS.Ordenes.ActualizarExistencias(idPro, actualBD);
                    this.CargarDatos();
                    double desco = (Convert.ToUInt32(this.dtgvDetalles.CurrentRow.Cells["Cantidad"].Value)) * (Convert.ToDouble(this.dtgvDetalles.CurrentRow.Cells["Descuento"].Value));
                    CLS.Ordenes.ActualizarDetalle(idVenta, this.dtgvDetalles.CurrentRow.Cells[0].Value.ToString(), Convert.ToInt32(actual), desco);
                }
            }
        }

        private void LimpiarTodo()
        {

        }
        /*        public void EliminarProductoTabla()
                {
                }*/
        private void RestarProducto()
        {
            if (this.dtgvDetalles.Rows.Count > 0)
            {
                if (this.dtgvDetalles.CurrentRow.Index != -1)
                {
                    Double actual = Convert.ToDouble(this.dtgvDetalles.CurrentRow.Cells[3].Value);
                    if (actual == 1)
                    {
                        MessageBox.Show("No puede seguir restando al llegar a uno, si desea eliminar este producto del carrito de click en el boton eliminar 'X'", "DEBE ELIMINAR");
                    }
                    else
                    {
                        actual -= 1;
                        tablaProductos.Rows[this.dtgvDetalles.CurrentRow.Index][3] = actual;
                        this.CargarDatos();
                        String idPro = "";
                        idPro = dtgvDetalles.CurrentRow.Cells[0].Value.ToString();
                        //  MessageBox.Show(idPro);
                        int actualBD = Convert.ToInt32(CLS.Ordenes.TraerExistencias(idPro));
                        actualBD++;
                        CLS.Ordenes.ActualizarExistencias(idPro, actualBD);
                        double desco = (Convert.ToUInt32(this.dtgvDetalles.CurrentRow.Cells["Cantidad"].Value)) * (Convert.ToDouble(this.dtgvDetalles.CurrentRow.Cells["Descuento"].Value));
                        CLS.Ordenes.ActualizarDetalle(idVenta, this.dtgvDetalles.CurrentRow.Cells[0].Value.ToString(), Convert.ToInt32(actual), desco);
                    }
                }
            }
        }

        private String[] PrepararDetalle(int pos)
        {
            String[] resul = new string[4];
            resul[0] = this.dtgvDetalles.Rows[pos].Cells["ID"].Value.ToString();
            resul[1] = this.dtgvDetalles.Rows[pos].Cells["Cantidad"].Value.ToString();
            resul[2] = this.dtgvDetalles.Rows[pos].Cells["Descuento"].Value.ToString();
            resul[3] = this.dtgvDetalles.Rows[pos].Cells["Precio"].Value.ToString();
            return resul;
        }


        public static bool BuscarEnTablaExistente(String co)
        {
            String re = "";
            bool encontrado = false;
            int con = 0;
            int fin = TablaProductos.Rows.Count;
            while (!encontrado & con < fin)
            {
                re = TablaProductos.Rows[con]["Co.Barras"].ToString();
                MessageBox.Show(re);
                if (re.Equals(co))
                {
                    encontrado = true;
                }
                else
                {
                    con++;
                }
            }
            return encontrado;
        }

        private void CargarDesdeCodigoBarras(String co)
        {
            DataTable temp = SystemCache.TODOSLOSPRODUCTOSCB(co);
            DataRow datos = TablaProductos.NewRow();
            if (temp.Rows.Count > 0)
            {
                if (BuscarEnTablaExistente(co))
                {
                    MessageBox.Show("Esta producto ya fue agregado al carrito...");
                }
                else
                {
                    if (Convert.ToInt32(temp.Rows[0]["UnidadesEnStock"]) > 0)
                    {
                        datos[0] = (temp.Rows[0]["idProducto"]);
                        datos[1] = (temp.Rows[0]["NombreProducto"]);
                        datos[2] = (temp.Rows[0]["codigoBarras"]);
                        //                    datos[3] = Convert.ToInt32(temp.Rows[0]["UnidadesEnStock"]);
                        datos[3] = Convert.ToInt32("1");
                        datos[4] = Convert.ToDouble(temp.Rows[0]["precioUnitario"]);
                        datos[5] = "0";
                        datos[6] = temp.Rows[0]["imagen"];
                        TablaProductos.Rows.Add(datos);
                        CLS.Ordenes.InsertarDetalle(idVenta, temp.Rows[0]["idProducto"].ToString(), "0", "1", (temp.Rows[0]["precioUnitario"]).ToString());
                        int actualBD = Convert.ToInt32(CLS.Ordenes.TraerExistencias(temp.Rows[0]["idProducto"].ToString()));
                        actualBD--;
                        CLS.Ordenes.ActualizarExistencias(temp.Rows[0]["idProducto"].ToString(), actualBD);
                    }
                    else
                    {
                        MessageBox.Show("No hay unidades en stock");
                    }
                }
            }
            else
            {
                MessageBox.Show("No se encontraron resultados...");
            }
            this.CargarDatos();
            this.subtotal();
        }

        private double Conversion(String txt)
        {
            Double re = 0;
            try
            {
                re = Convert.ToDouble(txt);
            }
            catch
            {
                re = 0;
            }
            return re;
        }



        private void PrepararTabla()
        {
            TablaProductos = new DataTable();
            DataColumn id = new DataColumn("ID", Type.GetType("System.String"));
            DataColumn Nombre = new DataColumn("Nombre", Type.GetType("System.String"));
            DataColumn Barras = new DataColumn("Co.Barras", Type.GetType("System.String"));
            DataColumn Cantidad = new DataColumn("Cantidad", Type.GetType("System.Int32"));
            DataColumn Descuento = new DataColumn("Descuento", Type.GetType("System.Double"));
            DataColumn precio = new DataColumn("Precio", Type.GetType("System.Double"));
            DataColumn img = new DataColumn("imagen", Type.GetType("System.Byte[]"));
            TablaProductos.Columns.Add(id);
            TablaProductos.Columns.Add(Nombre);
            TablaProductos.Columns.Add(Barras);
            TablaProductos.Columns.Add(Cantidad);
            TablaProductos.Columns.Add(precio);
            TablaProductos.Columns.Add(Descuento);
            TablaProductos.Columns.Add(img);
            CargarDatos();
            dtgvDetalles.Columns[0].Width = 50;
            dtgvDetalles.Columns[0].ReadOnly = true;
            dtgvDetalles.Columns[1].ReadOnly = true;
            dtgvDetalles.Columns[2].ReadOnly = true;
            dtgvDetalles.Columns[3].Width = 60;
            dtgvDetalles.Columns[6].ReadOnly = true;
            dtgvDetalles.Columns[6].Visible = false;
            Clipboard.Clear();
            if (!this.entrada.Equals(""))
            {
                CargarDesdeCodigoBarras(entrada);
            }
            comboEmpleado.DataSource = CacheManager.SystemCache.TODOSLOSEMPLEADOS();
            comboEmpleado.DisplayMember = "Completo";
            comboEmpleado.ValueMember = "IDEmpleado";
            comboEmpleado.SelectedValue = SessionManager.Sesion.Instancia.OUsuario.IDEmpleado;
            comboDocumento.SelectedIndex = 0;
            comboBox5.Items.Add(DBManager.CLS.AppConfiguration.RecuperarValor("IDSucursal", "2"));
            comboBox5.SelectedIndex = 0;
            comboEstado.SelectedIndex = 1;
            comboClientes.DataSource = CacheManager.SystemCache.TODOSLOSCLIENTES();
            comboClientes.DisplayMember = "NombreContacto";
            comboClientes.ValueMember = "IDCliente";
            comboClientes.SelectedIndex = 0;
            try
            {
                int nuevo = CLS.Ordenes.TraerNumeroActualFactura();
                if (
                CLS.Ordenes.InsertarOrden(this.comboEmpleado.SelectedValue.ToString(), comboBox5.SelectedItem.ToString(), comboEstado.SelectedItem.ToString(), comboDocumento.SelectedItem.ToString(), "1", nuevo))
                {
                    //                    MessageBox.Show("Correcto");
                    //                    int nuevo = CLS.Ordenes.TraerNumeroActualFactura();
                    txtNoFactura.Text = nuevo.ToString();
                    idVenta = CLS.Ordenes.TraerIDOrdenRecienIngresada(nuevo.ToString());
                    nuevo++;
                    CLS.Ordenes.ActualizarNumeroActualFactura(nuevo);
         //           MessageBox.Show(idVenta + " Es el idVenta");
                }
                else
                {
                    MessageBox.Show("Error");
                }
            }
            catch (Exception exe)
            {
                MessageBox.Show("Error al ingresar a la BD: \n\n" + exe.StackTrace.ToString());
            }
        }

        public static void AgregarFila(DataRow fila)
        {
            Ventas.TablaProductos.Rows.Add(fila);
        }

        private void Ventas_Load(object sender, EventArgs e)
        {
            PrepararTabla();
            Detector.Start();
        }

        public void CargarDatos()
        {
            this._tabla.DataSource = TablaProductos;
            this.dtgvDetalles.DataSource = _tabla;
            this.lblRegistros.Text = TablaProductos.Rows.Count + " Productos agregados.";
        }

        private void txtCodigoBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Operaciones.Enter(e))
            {
                this.CargarDesdeCodigoBarras(this.txtCodigoBarras.Text);
            }
        }

        private void dtgvDetalles_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                CacheManager.Operaciones.MostrarImagen(this.fotoProducto, (byte[])this.dtgvDetalles.CurrentRow.Cells["imagen"].Value);
                this.txtIDProducto.Text = this.dtgvDetalles.CurrentRow.Cells["ID"].Value.ToString();
                this.PrecioUnitario.Text = this.dtgvDetalles.CurrentRow.Cells["Precio"].Value.ToString();
            }
            catch
            {
            }
        }

        private void dtgvDetalles_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                CacheManager.Operaciones.MostrarImagen(this.fotoProducto, (byte[])this.dtgvDetalles.CurrentRow.Cells["imagen"].Value);
                this.txtIDProducto.Text = this.dtgvDetalles.CurrentRow.Cells["ID"].Value.ToString();
                this.PrecioUnitario.Text = this.dtgvDetalles.CurrentRow.Cells["Precio"].Value.ToString();
            }
            catch
            {
            }
        }

        private void dtgvDetalles_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            try
            {
                CacheManager.Operaciones.MostrarImagen(this.fotoProducto, (byte[])this.dtgvDetalles.CurrentRow.Cells["imagen"].Value);
                this.txtIDProducto.Text = this.dtgvDetalles.CurrentRow.Cells["ID"].Value.ToString();
                this.PrecioUnitario.Text = this.dtgvDetalles.CurrentRow.Cells["Precio"].Value.ToString();
            }
            catch
            {
            }
        }

        private void Detector_Tick(object sender, EventArgs e)
        {
            String coknow;
            if (Clipboard.ContainsText())
            {
                coknow = Clipboard.GetText();
                Detector.Stop();
                this.CargarDesdeCodigoBarras(coknow);
                Clipboard.Clear();
                Detector.Start();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dtgvDetalles.Rows.Count > 0)
                {
                    if (this.dtgvDetalles.CurrentRow.Index != -1)
                    {
                        String[] temp = PrepararDetalle(dtgvDetalles.CurrentRow.Index);
                        int actual = CLS.Ordenes.TraerExistencias(temp[0]);
                        actual += Convert.ToInt32(temp[1]);
                        CLS.Ordenes.ActualizarExistencias(temp[0], actual);
                        CLS.Ordenes.EliminarDetalle(idVenta.ToString(), temp[0]);
                        tablaProductos.Rows.Remove(tablaProductos.Rows[dtgvDetalles.CurrentRow.Index]);
                        this.CargarDatos();
                        this.subtotal();
                    }
                }
                else
                {
                    MessageBox.Show("No hay datos para eliminar...");
                }
            }
            catch { }
        }

        private void btnMenos_Click(object sender, EventArgs e)
        {
            this.RestarProducto(); //Visualmente       
            subtotal();
        }

        private void btnMas_Click(object sender, EventArgs e)
        {
            this.AgregarProductosATabla();//Visualmente           
            subtotal();

        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            CLS.Ordenes.ActualizarOrden(idVenta, comboEstado.SelectedItem.ToString(), comboClientes.SelectedValue.ToString(), comboEmpleado.SelectedValue.ToString(), comboDocumento.SelectedItem.ToString(), comboBox5.SelectedItem.ToString());
            FinalVenta final = new FinalVenta(comboDocumento.SelectedItem.ToString(), txtTotal.Text);
            final.ShowDialog();
        }

        private void btnQuitar_Click(object sender, EventArgs e)
        {
            for (int i = dtgvDetalles.Rows.Count - 1; i >= 0; i--)
            {
                String[] temp = PrepararDetalle(i);
                int actual = CLS.Ordenes.TraerExistencias(temp[0]);
                actual += Convert.ToInt32(temp[1]);
                CLS.Ordenes.ActualizarExistencias(temp[0], actual);
                CLS.Ordenes.EliminarDetalle(idVenta.ToString(), temp[0]);
                tablaProductos.Rows.Remove(tablaProductos.Rows[i]);
            }
            this.CargarDatos();
            this.subtotal();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                VentaEdicion f = new VentaEdicion();
                int indice = dtgvDetalles.CurrentRow.Index;
                Object[] temp = new object[7];
                for (int i = 0; i < 7; i++)
                {
                    temp[i] = tablaProductos.Rows[indice][i];
                }
                VentaEdicion.CargarATablaLocalPorEdicion(indice, temp);
                f.ShowDialog();
                CargarDatos();
                this.dtgvDetalles.Refresh();
                subtotal();
            }
            catch (Exception excep)
            {
                MessageBox.Show("Ha ocurrido un error  " + excep.StackTrace.ToString());
            }
        }

        private void Ventas_Enter(object sender, EventArgs e)
        {
            Clipboard.Clear();
            this.Detector.Start();
        }

        private void Ventas_Leave(object sender, EventArgs e)
        {
            this.Detector.Stop();
        }
    }
}
