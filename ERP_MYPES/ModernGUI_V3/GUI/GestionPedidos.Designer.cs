﻿namespace ModernGUI_V3.GUI
{
    partial class GestionPedidos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PanelBuscar = new System.Windows.Forms.Panel();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.btnPreparado = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFiltro = new System.Windows.Forms.TextBox();
            this.btnPerdidos = new System.Windows.Forms.Button();
            this.btnNoEntregado = new System.Windows.Forms.Button();
            this.btnEntregado = new System.Windows.Forms.Button();
            this.btnPendiente = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.PanelTop = new System.Windows.Forms.Panel();
            this.dtgvPedidos = new System.Windows.Forms.DataGridView();
            this.MunuLateral = new System.Windows.Forms.ToolStrip();
            this.btnAgregar = new System.Windows.Forms.ToolStripButton();
            this.btnEliminar = new System.Windows.Forms.ToolStripButton();
            this.btnEditar = new System.Windows.Forms.ToolStripButton();
            this.btnDetalle = new System.Windows.Forms.ToolStripButton();
            this.IDPedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDCliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreCliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDSucursal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sucursal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaPedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaEntrega = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EstadoPedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumPedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelBuscar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvPedidos)).BeginInit();
            this.MunuLateral.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelBuscar
            // 
            this.PanelBuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(17)))), ((int)(((byte)(38)))));
            this.PanelBuscar.Controls.Add(this.btnEnviar);
            this.PanelBuscar.Controls.Add(this.btnPreparado);
            this.PanelBuscar.Controls.Add(this.label1);
            this.PanelBuscar.Controls.Add(this.txtFiltro);
            this.PanelBuscar.Controls.Add(this.btnPerdidos);
            this.PanelBuscar.Controls.Add(this.btnNoEntregado);
            this.PanelBuscar.Controls.Add(this.btnEntregado);
            this.PanelBuscar.Controls.Add(this.btnPendiente);
            this.PanelBuscar.Controls.Add(this.label2);
            this.PanelBuscar.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelBuscar.Location = new System.Drawing.Point(0, 43);
            this.PanelBuscar.Name = "PanelBuscar";
            this.PanelBuscar.Size = new System.Drawing.Size(943, 105);
            this.PanelBuscar.TabIndex = 28;
            this.PanelBuscar.Paint += new System.Windows.Forms.PaintEventHandler(this.PanelBuscar_Paint);
            // 
            // btnEnviar
            // 
            this.btnEnviar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEnviar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(0)))), ((int)(((byte)(51)))));
            this.btnEnviar.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnviar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnEnviar.Image = global::ModernGUI_V3.Properties.Resources.envio24;
            this.btnEnviar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEnviar.Location = new System.Drawing.Point(741, 60);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(174, 33);
            this.btnEnviar.TabIndex = 21;
            this.btnEnviar.Text = "Enviar";
            this.btnEnviar.UseVisualStyleBackColor = false;
            // 
            // btnPreparado
            // 
            this.btnPreparado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPreparado.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnPreparado.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnPreparado.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPreparado.ForeColor = System.Drawing.SystemColors.Control;
            this.btnPreparado.Image = global::ModernGUI_V3.Properties.Resources.box24;
            this.btnPreparado.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPreparado.Location = new System.Drawing.Point(741, 18);
            this.btnPreparado.Name = "btnPreparado";
            this.btnPreparado.Size = new System.Drawing.Size(174, 35);
            this.btnPreparado.TabIndex = 20;
            this.btnPreparado.Text = "Preparados";
            this.btnPreparado.UseVisualStyleBackColor = false;
            this.btnPreparado.Click += new System.EventHandler(this.btnPreparado_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(12, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 18);
            this.label1.TabIndex = 19;
            this.label1.Text = "Filtrar";
            // 
            // txtFiltro
            // 
            this.txtFiltro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFiltro.Location = new System.Drawing.Point(65, 67);
            this.txtFiltro.Name = "txtFiltro";
            this.txtFiltro.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtFiltro.Size = new System.Drawing.Size(284, 20);
            this.txtFiltro.TabIndex = 18;
            this.txtFiltro.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFiltro.TextChanged += new System.EventHandler(this.txtFiltro_TextChanged);
            // 
            // btnPerdidos
            // 
            this.btnPerdidos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPerdidos.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnPerdidos.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnPerdidos.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPerdidos.ForeColor = System.Drawing.SystemColors.Control;
            this.btnPerdidos.Image = global::ModernGUI_V3.Properties.Resources.filebroken24;
            this.btnPerdidos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPerdidos.Location = new System.Drawing.Point(561, 59);
            this.btnPerdidos.Name = "btnPerdidos";
            this.btnPerdidos.Size = new System.Drawing.Size(174, 33);
            this.btnPerdidos.TabIndex = 17;
            this.btnPerdidos.Text = "Perdidos";
            this.btnPerdidos.UseVisualStyleBackColor = false;
            this.btnPerdidos.Click += new System.EventHandler(this.btnPerdidos_Click);
            // 
            // btnNoEntregado
            // 
            this.btnNoEntregado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNoEntregado.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnNoEntregado.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNoEntregado.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNoEntregado.ForeColor = System.Drawing.SystemColors.Control;
            this.btnNoEntregado.Image = global::ModernGUI_V3.Properties.Resources.cancel24;
            this.btnNoEntregado.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNoEntregado.Location = new System.Drawing.Point(561, 18);
            this.btnNoEntregado.Name = "btnNoEntregado";
            this.btnNoEntregado.Size = new System.Drawing.Size(174, 35);
            this.btnNoEntregado.TabIndex = 16;
            this.btnNoEntregado.Text = "NO Entregado";
            this.btnNoEntregado.UseVisualStyleBackColor = false;
            this.btnNoEntregado.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnEntregado
            // 
            this.btnEntregado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEntregado.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnEntregado.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEntregado.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEntregado.ForeColor = System.Drawing.SystemColors.Control;
            this.btnEntregado.Image = global::ModernGUI_V3.Properties.Resources.confirm24;
            this.btnEntregado.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEntregado.Location = new System.Drawing.Point(386, 59);
            this.btnEntregado.Name = "btnEntregado";
            this.btnEntregado.Size = new System.Drawing.Size(169, 33);
            this.btnEntregado.TabIndex = 15;
            this.btnEntregado.Text = "Entregados";
            this.btnEntregado.UseVisualStyleBackColor = false;
            this.btnEntregado.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnPendiente
            // 
            this.btnPendiente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPendiente.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnPendiente.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnPendiente.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPendiente.ForeColor = System.Drawing.SystemColors.Control;
            this.btnPendiente.Image = global::ModernGUI_V3.Properties.Resources.clock24;
            this.btnPendiente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPendiente.Location = new System.Drawing.Point(386, 18);
            this.btnPendiente.Name = "btnPendiente";
            this.btnPendiente.Size = new System.Drawing.Size(169, 35);
            this.btnPendiente.TabIndex = 14;
            this.btnPendiente.Text = "Pendientes";
            this.btnPendiente.UseVisualStyleBackColor = false;
            this.btnPendiente.Click += new System.EventHandler(this.btnPendiente_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(3, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(346, 38);
            this.label2.TabIndex = 13;
            this.label2.Text = "GESTIÓN DE PEDIDOS";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PanelTop
            // 
            this.PanelTop.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.PanelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelTop.Location = new System.Drawing.Point(0, 0);
            this.PanelTop.Name = "PanelTop";
            this.PanelTop.Size = new System.Drawing.Size(943, 43);
            this.PanelTop.TabIndex = 27;
            // 
            // dtgvPedidos
            // 
            this.dtgvPedidos.AllowUserToAddRows = false;
            this.dtgvPedidos.AllowUserToDeleteRows = false;
            this.dtgvPedidos.AllowUserToResizeRows = false;
            this.dtgvPedidos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvPedidos.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            this.dtgvPedidos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtgvPedidos.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.MenuHighlight;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgvPedidos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dtgvPedidos.ColumnHeadersHeight = 30;
            this.dtgvPedidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dtgvPedidos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDPedido,
            this.IDCliente,
            this.NombreCliente,
            this.IDSucursal,
            this.Sucursal,
            this.FechaPedido,
            this.FechaEntrega,
            this.EstadoPedido,
            this.NumPedido});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgvPedidos.DefaultCellStyle = dataGridViewCellStyle5;
            this.dtgvPedidos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgvPedidos.EnableHeadersVisualStyles = false;
            this.dtgvPedidos.GridColor = System.Drawing.Color.SteelBlue;
            this.dtgvPedidos.Location = new System.Drawing.Point(0, 148);
            this.dtgvPedidos.MultiSelect = false;
            this.dtgvPedidos.Name = "dtgvPedidos";
            this.dtgvPedidos.ReadOnly = true;
            this.dtgvPedidos.RowHeadersVisible = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dtgvPedidos.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dtgvPedidos.RowTemplate.Height = 28;
            this.dtgvPedidos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgvPedidos.Size = new System.Drawing.Size(768, 302);
            this.dtgvPedidos.TabIndex = 31;
            // 
            // MunuLateral
            // 
            this.MunuLateral.AutoSize = false;
            this.MunuLateral.Dock = System.Windows.Forms.DockStyle.Right;
            this.MunuLateral.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAgregar,
            this.btnEliminar,
            this.btnEditar,
            this.btnDetalle});
            this.MunuLateral.Location = new System.Drawing.Point(768, 148);
            this.MunuLateral.Name = "MunuLateral";
            this.MunuLateral.Size = new System.Drawing.Size(175, 302);
            this.MunuLateral.TabIndex = 30;
            this.MunuLateral.Text = "toolStrip1";
            // 
            // btnAgregar
            // 
            this.btnAgregar.AutoSize = false;
            this.btnAgregar.BackColor = System.Drawing.Color.LightGray;
            this.btnAgregar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregar.ForeColor = System.Drawing.Color.DarkGreen;
            this.btnAgregar.Image = global::ModernGUI_V3.Properties.Resources.plus;
            this.btnAgregar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnAgregar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAgregar.Margin = new System.Windows.Forms.Padding(6);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(170, 40);
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.AutoSize = false;
            this.btnEliminar.BackColor = System.Drawing.Color.LightGray;
            this.btnEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.ForeColor = System.Drawing.Color.Red;
            this.btnEliminar.Image = global::ModernGUI_V3.Properties.Resources.error;
            this.btnEliminar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(6);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(170, 40);
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.AutoSize = false;
            this.btnEditar.BackColor = System.Drawing.Color.LightGray;
            this.btnEditar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(17)))), ((int)(((byte)(38)))));
            this.btnEditar.Image = global::ModernGUI_V3.Properties.Resources.writing;
            this.btnEditar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditar.Margin = new System.Windows.Forms.Padding(6);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(170, 40);
            this.btnEditar.Text = "Editar";
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnDetalle
            // 
            this.btnDetalle.AutoSize = false;
            this.btnDetalle.BackColor = System.Drawing.Color.LightGray;
            this.btnDetalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDetalle.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.btnDetalle.Image = global::ModernGUI_V3.Properties.Resources.profiles;
            this.btnDetalle.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnDetalle.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDetalle.Margin = new System.Windows.Forms.Padding(6);
            this.btnDetalle.Name = "btnDetalle";
            this.btnDetalle.Size = new System.Drawing.Size(170, 40);
            this.btnDetalle.Text = "Detalle";
            // 
            // IDPedido
            // 
            this.IDPedido.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IDPedido.DataPropertyName = "IDPedido";
            this.IDPedido.FillWeight = 39.83813F;
            this.IDPedido.HeaderText = "ID";
            this.IDPedido.MinimumWidth = 50;
            this.IDPedido.Name = "IDPedido";
            this.IDPedido.ReadOnly = true;
            this.IDPedido.Width = 50;
            // 
            // IDCliente
            // 
            this.IDCliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IDCliente.DataPropertyName = "IDCliente";
            this.IDCliente.FillWeight = 161.3727F;
            this.IDCliente.HeaderText = "IDCliente";
            this.IDCliente.MinimumWidth = 50;
            this.IDCliente.Name = "IDCliente";
            this.IDCliente.ReadOnly = true;
            this.IDCliente.Width = 50;
            // 
            // NombreCliente
            // 
            this.NombreCliente.DataPropertyName = "NombreCliente";
            this.NombreCliente.FillWeight = 72.43296F;
            this.NombreCliente.HeaderText = "NombreCliente";
            this.NombreCliente.Name = "NombreCliente";
            this.NombreCliente.ReadOnly = true;
            // 
            // IDSucursal
            // 
            this.IDSucursal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IDSucursal.DataPropertyName = "IDSucursal";
            this.IDSucursal.FillWeight = 191.6243F;
            this.IDSucursal.HeaderText = "IDSucursal";
            this.IDSucursal.MinimumWidth = 50;
            this.IDSucursal.Name = "IDSucursal";
            this.IDSucursal.ReadOnly = true;
            this.IDSucursal.Width = 50;
            // 
            // Sucursal
            // 
            this.Sucursal.DataPropertyName = "Sucursal";
            this.Sucursal.FillWeight = 72.43296F;
            this.Sucursal.HeaderText = "Sucursal";
            this.Sucursal.Name = "Sucursal";
            this.Sucursal.ReadOnly = true;
            // 
            // FechaPedido
            // 
            this.FechaPedido.DataPropertyName = "FechaPedido";
            this.FechaPedido.FillWeight = 72.43296F;
            this.FechaPedido.HeaderText = "FechaPedido";
            this.FechaPedido.Name = "FechaPedido";
            this.FechaPedido.ReadOnly = true;
            // 
            // FechaEntrega
            // 
            this.FechaEntrega.DataPropertyName = "FechaEntrega";
            this.FechaEntrega.FillWeight = 72.43296F;
            this.FechaEntrega.HeaderText = "FechaEntrega";
            this.FechaEntrega.Name = "FechaEntrega";
            this.FechaEntrega.ReadOnly = true;
            // 
            // EstadoPedido
            // 
            this.EstadoPedido.DataPropertyName = "EstadoPedido";
            this.EstadoPedido.FillWeight = 72.43296F;
            this.EstadoPedido.HeaderText = "EstadoPedido";
            this.EstadoPedido.Name = "EstadoPedido";
            this.EstadoPedido.ReadOnly = true;
            // 
            // NumPedido
            // 
            this.NumPedido.DataPropertyName = "NumPedido";
            this.NumPedido.HeaderText = "NumPedido";
            this.NumPedido.Name = "NumPedido";
            this.NumPedido.ReadOnly = true;
            this.NumPedido.Visible = false;
            // 
            // GestionPedidos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(943, 450);
            this.Controls.Add(this.dtgvPedidos);
            this.Controls.Add(this.MunuLateral);
            this.Controls.Add(this.PanelBuscar);
            this.Controls.Add(this.PanelTop);
            this.Name = "GestionPedidos";
            this.Text = "GestionPedidos";
            this.Load += new System.EventHandler(this.GestionPedidos_Load);
            this.PanelBuscar.ResumeLayout(false);
            this.PanelBuscar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvPedidos)).EndInit();
            this.MunuLateral.ResumeLayout(false);
            this.MunuLateral.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelBuscar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel PanelTop;
        private System.Windows.Forms.DataGridView dtgvPedidos;
        private System.Windows.Forms.ToolStrip MunuLateral;
        private System.Windows.Forms.ToolStripButton btnAgregar;
        private System.Windows.Forms.ToolStripButton btnEliminar;
        private System.Windows.Forms.ToolStripButton btnEditar;
        private System.Windows.Forms.ToolStripButton btnDetalle;
        private System.Windows.Forms.Button btnPendiente;
        private System.Windows.Forms.Button btnPerdidos;
        private System.Windows.Forms.Button btnNoEntregado;
        private System.Windows.Forms.Button btnEntregado;
        private System.Windows.Forms.Button btnPreparado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFiltro;
        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDPedido;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDSucursal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sucursal;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaPedido;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaEntrega;
        private System.Windows.Forms.DataGridViewTextBoxColumn EstadoPedido;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumPedido;
    }
}