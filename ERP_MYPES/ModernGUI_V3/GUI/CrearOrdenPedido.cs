﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModernGUI_V3.CLS;

namespace ModernGUI_V3.GUI
{
    public partial class CrearOrdenPedido : Form
    {
        CLS.Pedido _NPedido = new CLS.Pedido();
        String NombreCliente;
        String IdPedido;
        BindingSource _Productos = new BindingSource();

        internal Pedido NPedido { get => _NPedido; set => _NPedido = value; }
        public string NombreCliente1 { get => NombreCliente; set => NombreCliente = value; }
        public string IdPedido1 { get => IdPedido; set => IdPedido = value; }

        public CrearOrdenPedido()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void CrearOrdenPedido_Load(object sender, EventArgs e)
        {
            CargarDatos();
            CargarEmpleados();
            txtNoFactura.Text = CLS.Ordenes.TraerNumeroActualFactura()+"";
            comboTipoFactura.SelectedIndex = 0;

        }

        private void CargarEmpleados()
        {
            try
            {
                this.comboEmpleados.DataSource = CacheManager.SystemCache.TODOSLOSEMPLEADOS();
                this.comboEmpleados.ValueMember = "IDEmpleado";
                this.comboEmpleados.DisplayMember = "NombreCompleto";
                this.comboEmpleados.SelectedIndex = 0;
            }
            catch
            {
            }
        }

        public void CargarDatos()
        {
            try
            {
                _Productos.DataSource = CacheManager.SystemCache.ProductosPorPedido(IdPedido);
                dtgvProductos.AutoGenerateColumns = false;
                dtgvProductos.DataSource = _Productos;

            }
            catch
            {
            }
        }

        private void comboEmpleados_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
