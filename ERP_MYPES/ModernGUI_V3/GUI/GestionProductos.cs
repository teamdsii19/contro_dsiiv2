﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.GUI
{
    public partial class GestionProductos : Form
    {
        public static String idSucursal;
        public static String Sucursal;
        private static bool modoSucursal1 = false;
        private static bool over = false;
        BindingSource _Productos = new BindingSource();
//        public static bool ModoSucursal { get => ModoSucursal1; set => ModoSucursal1 = value; }
       public static bool ModoSucursal1 { get => modoSucursal1; set => modoSucursal1 = value; }
        bool multisucursal;
        bool Variantes;
        public GestionProductos()
        {
            InitializeComponent();
        }
        public void CargarDatos(int opcion)
        {
            try
            {
                if (opcion == 1)
                {
                    _Productos.DataSource = CacheManager.SystemCache.TODOSLOSPRODUCTOS();
                    FiltrarLocalmente();
                }
                else
                { //Será en una sucursal
                    _Productos.DataSource = CacheManager.SystemCache.TODOSLOSPRODUCTOSSUCURSAL(DBManager.CLS.AppConfiguration.RecuperarValor("IDSucursal","1"));
                    FiltrarLocalmente();
                }
            }
            catch
            {
            }
        }

        public void FiltrarLocalmente()
        {
            try
            {
                if (txtFiltro.TextLength > 0)
                {
                    _Productos.Filter = "NombreProducto LIKE '%" + txtFiltro.Text + "%' OR Alias LIKE '% " + txtFiltro.Text + "%'";
                }
                else
                {
                    _Productos.RemoveFilter();
                }
                dtgvProductos.AutoGenerateColumns = false;
                dtgvProductos.DataSource = _Productos;
                tsRegistro1.Text = dtgvProductos.Rows.Count.ToString() + " Registro encontrados";
            }
            catch
            {
            }
        }

        private void btnProveedores_Click(object sender, EventArgs e)
        {
            EDC.PanelProveedores pro = new EDC.PanelProveedores();
            pro.ShowDialog();
        }

        private void btnCategorias_Click(object sender, EventArgs e)
        {
            EDC.PanelCategorias cat = new EDC.PanelCategorias();
            cat.ShowDialog();
        }

        private void GestionProductos_Load(object sender, EventArgs e)
        {
             multisucursal = DBManager.CLS.AppConfiguration.RecuperarValor("Multisucursal", false);
             Variantes = DBManager.CLS.AppConfiguration.RecuperarValor("ModalidadVariacionProducto", true);
            
            if (multisucursal)
            {
                if (Variantes)
                {

                }
                else
                {

                }
            }
            else
            {
                if (Variantes)
                {
                    this.checkInventario.Enabled = false;
                    this.checkInventario.Visible = false;
                    this.comboBox1.Enabled = false;
                    this.comboBox1.Visible = false;
                    this.button4.Enabled = false;
                    this.button4.Visible = false;
                }
                else
                {
                    this.checkInventario.Enabled = false;
                    this.checkInventario.Visible = false;
                    this.comboBox1.Enabled = false;
                    this.comboBox1.Visible = false;
                    this.button4.Enabled = false;
                    this.button4.Visible = false;
                    this.btnVariante.Enabled = false;
                    this.btnVariante.Visible = false;
                }
            }
                this.CargarDatos(1);
            CacheManager.Operaciones.LlenarComboBox(this.comboBox1, "idSucursal", "NombreSucursal", "Sucursales");
            this.previsualizacion.Visible = false;
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            this.FiltrarLocalmente();
        }

        private void dtgvProductos_MouseLeave(object sender, EventArgs e)
        {
            this.previsualizacion.Visible = false;
            over = false;
        }

        private void dtgvProductos_MouseHover(object sender, EventArgs e)
        {
            over = true;
            this.previsualizacion.Visible = true;
            try
            {
                this.previsualizacion.Location = new Point(this.MunuLateral.Location.X + 36, this.MunuLateral.Location.Y + 320);
                //                this.previsualizacion.Location = MousePosition;
                this.previsualizacion.Image = Image.FromStream(CacheManager.Operaciones.PrepararImagenParaMostrar((byte[])dtgvProductos["imagen", dtgvProductos.CurrentRow.Index].Value));
            }
            catch
            {
                this.previsualizacion.Visible = false;

            }
        }

        private void dtgvProductos_KeyDown(object sender, KeyEventArgs e)
        {
            if (over)
            {
                this.previsualizacion.Visible = true;
                try
                {
                    this.previsualizacion.Location = new Point(this.MunuLateral.Location.X + 36, this.MunuLateral.Location.Y + 320);                    //                    this.previsualizacion.Location = MousePosition;
                    this.previsualizacion.Image = Image.FromStream(CacheManager.Operaciones.PrepararImagenParaMostrar((byte[])dtgvProductos["imagen", dtgvProductos.CurrentRow.Index].Value));
                }
                catch
                {
                    //   this.previsualizacion.Image = previsualizacion.ErrorImage;
                    this.previsualizacion.Visible = false;
                }

            }

        }

        private void dtgvProductos_KeyUp(object sender, KeyEventArgs e)
        {
            if (over)
            {
                this.previsualizacion.Visible = true;
                try
                {
                    this.previsualizacion.Location = new Point(this.MunuLateral.Location.X + 36, this.MunuLateral.Location.Y + 320);
                    this.previsualizacion.Image = Image.FromStream(CacheManager.Operaciones.PrepararImagenParaMostrar((byte[])dtgvProductos["imagen", dtgvProductos.CurrentRow.Index].Value));
                }
                catch
                {
                    //   this.previsualizacion.Image = previsualizacion.ErrorImage;
                    this.previsualizacion.Visible = false;
                }

            }
        }

        private void dtgvProductos_MouseMove(object sender, MouseEventArgs e)
        {
            over = true;
            this.previsualizacion.Visible = true;
            try
            {
                this.previsualizacion.Location = new Point(this.MunuLateral.Location.X + 36, this.MunuLateral.Location.Y + 320);
                //                this.previsualizacion.Location = MousePosition;
                this.previsualizacion.Image = Image.FromStream(CacheManager.Operaciones.PrepararImagenParaMostrar((byte[])dtgvProductos["imagen", dtgvProductos.CurrentRow.Index].Value));
            }
            catch
            {
                this.previsualizacion.Visible = false;

            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            EDC.EdicionProductos f = new EDC.EdicionProductos();
            f.Text = "Agregar nuevo producto";
            f.ShowDialog();
            CargarDatos(1);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtgvProductos.CurrentRow.Index != -1)
                {
                    if (MessageBox.Show("¿Realmente desea ELIMINAR el registro seleccionado? \n Es posible que hayan registros basados en este producto: \n" +
                    " " + this.dtgvProductos.CurrentRow.Cells["NombreProducto"].Value.ToString(), "CONFIMACION DE ELIMINACION", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                    {
                        if (CLS.Producto.Eliminar(this.dtgvProductos.CurrentRow.Cells["idProducto"].Value.ToString()))
                        {
                            MessageBox.Show("Eliminación realizada con éxito");
                            this.CargarDatos(1);
                        }
                        else
                        {
                            MessageBox.Show("No se Eliminó");
                        }
                    }
                }
            }
            catch { }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
           // try
            //{
                if (dtgvProductos.CurrentRow.Index != -1)
                {
                    EDC.EdicionProductos f = new EDC.EdicionProductos();
                    f.Text = "Editar producto";
                    f.txtID.Text = this.dtgvProductos.CurrentRow.Cells["idProducto"].Value.ToString();
                    f.txtNombre.Text = this.dtgvProductos.CurrentRow.Cells["NombreProducto"].Value.ToString();
                    f.txtAlias.Text = this.dtgvProductos.CurrentRow.Cells["Alias"].Value.ToString();
                    f.txtCodigoBarras.Text = this.dtgvProductos.CurrentRow.Cells["codigoBarras"].Value.ToString();
                    f.txtPresentacion.Text = this.dtgvProductos.CurrentRow.Cells["MedidaPorUnidad"].Value.ToString();
                    f.txtPrecio.Text = this.dtgvProductos.CurrentRow.Cells["precioUnitario"].Value.ToString();
                    f.txtCosto.Text = this.dtgvProductos["costo", dtgvProductos.CurrentRow.Index].Value.ToString();
                    f.txtMinimo.Text = this.dtgvProductos["minimo", dtgvProductos.CurrentRow.Index].Value.ToString();
                    f.txtExistencias.Text = this.dtgvProductos.CurrentRow.Cells["UnidadesEnStock"].Value.ToString();
                    EDC.EdicionProductos.Categoria = this.dtgvProductos["NombreCategroria", dtgvProductos.CurrentRow.Index].Value.ToString();
                    EDC.EdicionProductos.Proveedor = this.dtgvProductos["NombreProveedor", dtgvProductos.CurrentRow.Index].Value.ToString();
                    try
                    {
                        f.fotoProductos.Image = Image.FromStream(CacheManager.Operaciones.PrepararImagenParaMostrar((byte[])dtgvProductos["imagen", dtgvProductos.CurrentRow.Index].Value));
                    }
                    catch { }
                    f.ShowDialog();
                    CargarDatos(1);
                }
          /*  }
            catch
            {
                MessageBox.Show("No hay registros para editar");
            }*/
        }

        private void btnDetalle_Click(object sender, EventArgs e) ///Ahora es el DE VARIANTES :V
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.FiltrarLocalmente();
        }

        private void dtgvProductos_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (this.dtgvProductos.Columns[e.ColumnIndex].Name == "unidadesEnStock") {
                try
                {
                    if (e.Value.GetType() != typeof(System.DBNull)) {
                        if (Convert.ToInt32(e.Value) == 0)
                        {
                            e.CellStyle.BackColor = Color.Red;
                            e.CellStyle.ForeColor = Color.Wheat;
                        }
                        else {
//                            if (Convert.ToInt32(e.Value) <= Convert.ToInt32(this.dtgvProductos.Columns[e.ColumnIndex].Name=="minimo") )
                                if (Convert.ToInt32(e.Value) <= Convert.ToInt32(this.dtgvProductos["minimo",e.RowIndex].Value))
                                {
                                e.CellStyle.BackColor = Color.Yellow;
                                e.CellStyle.ForeColor = Color.White;
                            }
                        }
                    }
                }
                catch (Exception)
                {

                }
            }
        }

        private void btnDetalle_Click_1(object sender, EventArgs e)
        {
            EDC.DetalleProducto f = new EDC.DetalleProducto();
            f.txtID.Text = this.dtgvProductos.CurrentRow.Cells["idProducto"].Value.ToString();
            f.txtNombre.Text = this.dtgvProductos.CurrentRow.Cells["NombreProducto"].Value.ToString();
            f.txtAlias.Text = this.dtgvProductos.CurrentRow.Cells["Alias"].Value.ToString();
            f.txtCodigoBarras.Text = this.dtgvProductos.CurrentRow.Cells["codigoBarras"].Value.ToString();
            f.txtPresentacion.Text = this.dtgvProductos.CurrentRow.Cells["MedidaPorUnidad"].Value.ToString();
            f.txtPrecio.Text = this.dtgvProductos.CurrentRow.Cells["precioUnitario"].Value.ToString();
            f.txtCosto.Text = this.dtgvProductos["costo", dtgvProductos.CurrentRow.Index].Value.ToString();
            EDC.DetalleProducto.Precio = this.dtgvProductos.CurrentRow.Cells["precioUnitario"].Value.ToString();
            EDC.DetalleProducto.Costo = this.dtgvProductos["costo", dtgvProductos.CurrentRow.Index].Value.ToString();
            f.txtExistencias.Text = this.dtgvProductos.CurrentRow.Cells["UnidadesEnStock"].Value.ToString();
            EDC.DetalleProducto.Categoria = this.dtgvProductos["NombreCategroria", dtgvProductos.CurrentRow.Index].Value.ToString();
            EDC.DetalleProducto.Proveedor = this.dtgvProductos["NombreProveedor", dtgvProductos.CurrentRow.Index].Value.ToString();
            f.txtFecha.Text = this.dtgvProductos["fecha", dtgvProductos.CurrentRow.Index].Value.ToString();
            f.txtMinimo.Text = this.dtgvProductos["minimo", dtgvProductos.CurrentRow.Index].Value.ToString();
            try
            {
                f.fotoProductos.Image = Image.FromStream(CacheManager.Operaciones.PrepararImagenParaMostrar((byte[])dtgvProductos["imagen", dtgvProductos.CurrentRow.Index].Value));
            }
            catch
            {
            }
            f.ShowDialog();
        }

        private void PanelBusqueda_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
