﻿using ModernGUI_V3.EDC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.GUI
{
    public partial class GestionUsuarios : Form
    {
        public GestionUsuarios()
        {
            InitializeComponent();
        }

        BindingSource _Usuarios = new BindingSource();
        public void CargarDatos()
        {
            try
            {
                _Usuarios.DataSource = CacheManager.SystemCache.TODOSLOSUSUARIOS();
                FiltrarLocalmente();
            }
            catch { }
        }
        public void FiltrarLocalmente()
        {
            try
            {
                if (txtFiltro.TextLength > 0)
                {
                    _Usuarios.Filter = "Usuario LIKE '%" + txtFiltro.Text + "%'";
                }
                else
                {
                    _Usuarios.RemoveFilter();
                }
                dtgvUsuarios.AutoGenerateColumns = false;
                dtgvUsuarios.DataSource = _Usuarios;
                tsRegistro1.Text = dtgvUsuarios.Rows.Count.ToString() + " Registro encontrados";
            }
            catch
            {

            }
        }
        private void GestionUsuarios_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void dtgvUsuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            EdicionUsuarios us = new EdicionUsuarios();
            us.Text = "Agregar Usuario";
            us.checkpassword.Visible = false;
            us.checkCambiarRol.Text = "Asignar Rol";
            us.checkEstado.Text = "Asignar Estado";
            us.ShowDialog();
            CargarDatos();

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea EDITAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                EdicionUsuarios us = new EdicionUsuarios();
                CLS.Usuarios oUsu = new CLS.Usuarios();
                us.Text = "Editar Usuario";
                us.txtCredencial.Text = dtgvUsuarios.CurrentRow.Cells["Credencial"].Value.ToString();
                us.txtIDUsuario.Text = dtgvUsuarios.CurrentRow.Cells["idUsuario"].Value.ToString();
                us.txtUser.Text = dtgvUsuarios.CurrentRow.Cells["usuario"].Value.ToString();
                us.lblEstaAc.Text = dtgvUsuarios.CurrentRow.Cells["Estado"].Value.ToString();
                us.lblRolActual.Text = dtgvUsuarios.CurrentRow.Cells["Rol"].Value.ToString();
                us.dtgvEmpleados.Enabled = false;
                us.txtFiltro.Enabled = false;
                //Creamos un objeto con los Datos del USUARIO
                oUsu.IdUsuario = dtgvUsuarios.CurrentRow.Cells["idUsuario"].Value.ToString();
                oUsu.Credencial1 = dtgvUsuarios.CurrentRow.Cells["Credencial"].Value.ToString();
                oUsu.Usuario = dtgvUsuarios.CurrentRow.Cells["usuario"].Value.ToString();
                oUsu.Estado1 = dtgvUsuarios.CurrentRow.Cells["Estado"].Value.ToString();
                oUsu.IdRol = dtgvUsuarios.CurrentRow.Cells["IdRol"].Value.ToString();
                oUsu.IdEmpleado = dtgvUsuarios.CurrentRow.Cells["IdEmpleado"].Value.ToString();
                us.OUsuarioEdit = oUsu;
                us.ShowDialog();
                CargarDatos();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea ELIMINAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                CLS.Usuarios oUsuario = new CLS.Usuarios();
                oUsuario.IdUsuario = dtgvUsuarios.CurrentRow.Cells["IdUsuario"].Value.ToString();
                if (oUsuario.Eliminar())
                {
                    MessageBox.Show("Registro eliminado exitosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDatos();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser eliminado exitosamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            this.FiltrarLocalmente();
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }
    }
}
