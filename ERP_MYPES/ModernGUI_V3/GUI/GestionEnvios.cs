﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.GUI
{
    public partial class GestionEnvios : Form
    {
        BindingSource _EnviosTable = new BindingSource();
        public GestionEnvios()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void GestionEnvios_Load(object sender, EventArgs e)
        {
            CargarDatos();
        
            }

        public void CargarDatos()
        {
            try
            {
                _EnviosTable.DataSource = CacheManager.SystemCache.TODOS_ENVIOS();
                FiltrarLocalmente();
            }
            catch { }
        }
        public void FiltrarLocalmente()
        {
            try
            {
                if (txtFiltro.TextLength > 0)
                {
                    _EnviosTable.Filter = "direccion LIKE '%" + txtFiltro.Text + "%'";
                }
                else
                {
                    _EnviosTable.RemoveFilter();
                }
                dtgvEnvios.AutoGenerateColumns = false;
                dtgvEnvios.DataSource = _EnviosTable;
            }
            catch
            {

            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            EDC.SeleccionarPedidosPendientes ven = new EDC.SeleccionarPedidosPendientes();
            ven.ShowDialog();
        }
    }
}
