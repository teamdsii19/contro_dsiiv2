﻿using CacheManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.GUI
{
    public partial class GestionPedidos : Form
    {
        BindingSource _PedidosTable = new BindingSource();
        public GestionPedidos()
        {
            InitializeComponent();
        }

        public void CargarDatos(String estado)
        {
            try
            {
                _PedidosTable.DataSource = CacheManager.SystemCache.PedidosporEstado(estado);
                FiltrarLocalmente();
            }
            catch { }
        }
        public void FiltrarLocalmente()
        {
            try
            {
                if (txtFiltro.TextLength > 0)
                {
                    _PedidosTable.Filter = "NombreCliente LIKE '%" + txtFiltro.Text + "%'";
                }
                else
                {
                    _PedidosTable.RemoveFilter();
                }
                dtgvPedidos.AutoGenerateColumns = false;
                dtgvPedidos.DataSource = _PedidosTable;
            }
            catch
            {

            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            EDC.PrepararPedido pp = new EDC.PrepararPedido();
            pp.txtNUMPedido.Text = SystemCache.NuevoNumeroPedido();
            pp.ShowDialog();
            CargarDatos("PENDIENTE");
        }

        private void PanelBuscar_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            CargarDatos("NO_ENTREGADO");
        }

        private void GestionPedidos_Load(object sender, EventArgs e)
        {
            CargarDatos("PENDIENTE");
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            this.FiltrarLocalmente();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CargarDatos("ENTREGADO");
        }

        private void btnPerdidos_Click(object sender, EventArgs e)
        {
            CargarDatos("PERDIDO");
        }

        private void btnPreparado_Click(object sender, EventArgs e)
        {
            CargarDatos("PREPARADO");
        }

        private void btnPendiente_Click(object sender, EventArgs e)
        {
            CargarDatos("PENDIENTE");
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            EDC.PrepararPedido pp = new EDC.PrepararPedido();
            CLS.Pedido obj = new CLS.Pedido();
            obj.IDPedido1 = this.dtgvPedidos.CurrentRow.Cells["IDPedido"].Value.ToString();
            obj.IDCliente1 = this.dtgvPedidos.CurrentRow.Cells["IDCliente"].Value.ToString();
            obj.IDSucursal1 = this.dtgvPedidos.CurrentRow.Cells["IDSucursal"].Value.ToString();
            obj.FechaEntrega1 = this.dtgvPedidos.CurrentRow.Cells["FechaEntrega"].Value.ToString();
            obj.FechaPedido1 = this.dtgvPedidos.CurrentRow.Cells["FechaPedido"].Value.ToString();
            obj.EstadoPedido1 = this.dtgvPedidos.CurrentRow.Cells["EstadoPedido"].Value.ToString();
            obj.NumPedido1 = this.dtgvPedidos.CurrentRow.Cells["NumPedido"].Value.ToString();
            pp.NPedido = obj;
            pp.txtNCliente.Text = this.dtgvPedidos.CurrentRow.Cells["NombreCliente"].Value.ToString();
            //MAGIC
            pp.Actualizando = true;
            pp.ShowDialog();
            CargarDatos("PENDIENTE");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea ELIMINAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes &&
                MessageBox.Show("Se eliminaran consigo todos los registros que de el dependan", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes
                )
            {
                CLS.Pedido oPedido = new CLS.Pedido();
                oPedido.IDPedido1 = dtgvPedidos.CurrentRow.Cells["IDPedido"].Value.ToString();
                if (dtgvPedidos.CurrentRow.Cells["EstadoPedido"].Value.ToString()=="PENDIENTE")
                {
                    SystemCache.AlgoritmoEliminacionDetalesPedidos(oPedido.IDPedido1);
                    oPedido.EliminarEnCascada(oPedido.IDPedido1);
                    MessageBox.Show("Registro eliminado exitosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDatos("PENDIENTE");
                }
                else
                {
                    MessageBox.Show("El Tipo de registro no puede ser eliminado", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
    }
}
