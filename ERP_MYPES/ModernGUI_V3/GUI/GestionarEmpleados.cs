﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CacheManager;

namespace ModernGUI_V3.GUI
{
    public partial class GestionarEmpleados : Form
    {
        public GestionarEmpleados()
        {
            InitializeComponent();
        }

        BindingSource _Empleados = new BindingSource();
        public void CargarDatos()
        {
            try
            {
                _Empleados.DataSource = CacheManager.SystemCache.TODOSLOSEMPLEADOS();
                 FiltrarLocalmente();
           }
            catch {
            }
        }

        public void FiltrarLocalmente()
        {
            try
            {
                if (txtFiltro.TextLength > 0)
                {
                    _Empleados.Filter = "Nombres LIKE '%" + txtFiltro.Text + "%' OR Apellidos like '%" + txtFiltro.Text + "%'";
                }
                else
                {
                    _Empleados.RemoveFilter();
                }
                dtgvEmpleados.AutoGenerateColumns = false;
                dtgvEmpleados.DataSource = _Empleados;
                tsRegistro1.Text = dtgvEmpleados.Rows.Count.ToString() + " Registro encontrados";
            }
            catch
            {

            }
        }

        private void GestionarEmpleados_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            EDC.EdicionEmpleados.departamento = "";
            EDC.EdicionEmpleados.mun = "";
            EDC.EdicionEmpleados F = new EDC.EdicionEmpleados();
            F.Text = "Agregar Empleado";
            F.tabControlEEmpleados.TabPages.Remove(F.tpAdministrativos); 
            F.ShowDialog();
            this.CargarDatos();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea EDITAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {

                EDC.EdicionEmpleados f = new EDC.EdicionEmpleados();
                f.Text = "Editar Empleado";
                f.IdEmpleado= this.dtgvEmpleados.CurrentRow.Cells["IDEmpleado"].Value.ToString();
                f.txtID.Text = this.dtgvEmpleados.CurrentRow.Cells["IDEmpleado"].Value.ToString();
                f.txtNombres.Text = this.dtgvEmpleados.CurrentRow.Cells["Nombres"].Value.ToString();
                f.txtApellidos.Text = this.dtgvEmpleados.CurrentRow.Cells["Apellidos"].Value.ToString();
                f.txtDUI.Text = this.dtgvEmpleados.CurrentRow.Cells["DUI"].Value.ToString();
                f.txtNIT.Text = this.dtgvEmpleados.CurrentRow.Cells["NIT"].Value.ToString();
                f.txtTelefono.Text = this.dtgvEmpleados.CurrentRow.Cells["telofono"].Value.ToString();
                f.txtDireccion.Text = this.dtgvEmpleados.CurrentRow.Cells["Direccion"].Value.ToString();
                f.txtNotas.Text = this.dtgvEmpleados["notas",dtgvEmpleados.CurrentRow.Index].Value.ToString();
                f.txtTitulo.Text = this.dtgvEmpleados.CurrentRow.Cells["titulo"].Value.ToString();
               f.dtpFecha.Text= this.dtgvEmpleados["fechaNacimiento", dtgvEmpleados.CurrentRow.Index].Value.ToString();
                EDC.EdicionEmpleados.departamento = this.dtgvEmpleados["departamento", dtgvEmpleados.CurrentRow.Index].Value.ToString();
                EDC.EdicionEmpleados.mun = this.dtgvEmpleados["municipio", dtgvEmpleados.CurrentRow.Index].Value.ToString();
                f.txtCorreo.Text = this.dtgvEmpleados["correo", dtgvEmpleados.CurrentRow.Index].Value.ToString();
                EDC.EdicionEmpleados.Sucursal = this.dtgvEmpleados["nombre", dtgvEmpleados.CurrentRow.Index].Value.ToString();
                EDC.EdicionEmpleados.Cargo = this.dtgvEmpleados["Cargo", dtgvEmpleados.CurrentRow.Index].Value.ToString();
                f.txtSueldo.Text = this.dtgvEmpleados["sueldo", dtgvEmpleados.CurrentRow.Index].Value.ToString();
                f.txtDescripcionCargo.Text = this.dtgvEmpleados["des", dtgvEmpleados.CurrentRow.Index].Value.ToString();
      //          f.SueldoOriginal = Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["des"].ToString();
                f.descripcionOriginal = this.dtgvEmpleados["des", dtgvEmpleados.CurrentRow.Index].Value.ToString();
                f.IdCargoEmpleado = this.dtgvEmpleados["idce", dtgvEmpleados.CurrentRow.Index].Value.ToString();
                f.SueldoOriginal = this.dtgvEmpleados["sueldo", dtgvEmpleados.CurrentRow.Index].Value.ToString();
                try
                {
                    f.fotoEmpleado.Image = Image.FromStream(Operaciones.PrepararImagenParaMostrar((byte[])dtgvEmpleados["foto", dtgvEmpleados.CurrentRow.Index].Value));
                }
                catch { }
                /*             f.txtNotas.Text =Convert.ToString(Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["notas"]);
                               f.txtTitulo.Text = Convert.ToString(Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["titulo"]);
                               f.dtpFecha.Text = this.dtgvEmpleados.CurrentRow.Cells["fechaNacimiento"].Value.ToString();
                               EDC.EdicionEmpleados.departamento = (Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["departamento"]).ToString();
                               EDC.EdicionEmpleados.mun = Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["municipio"].ToString();
                               f.txtCorreo.Text =Convert.ToString(Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["correo"]);
                               EDC.EdicionEmpleados.Sucursal = (Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["nombre"]).ToString();
                               EDC.EdicionEmpleados.Cargo = Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["Cargo"].ToString();
                               f.txtSueldo.Text = Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["sueldo"].ToString();
                               f.txtDescripcionCargo.Text= Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["des"].ToString();
                               f.SueldoOriginal = Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["des"].ToString();
                               f.descripcionOriginal= Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["des"].ToString(); 
                               f.IdCargoEmpleado= Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["idce"].ToString();
                               f.SueldoOriginal = Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["sueldo"].ToString();
                               // IDEmpleado, Nombres, Apellidos, fechaNacimiento, DUI, NIT, Telofono, Direccion, notas, Titulo, idDepartamento, 
                               //idMunicipio, Cargo, idCargo, nombre, nombreCorto, idSucursal, sueldo, idSueldo, foto, idfoto, des
                               try
                               {
                                   f.fotoEmpleado.Image = Image.FromStream(Operaciones.PrepararImagenParaMostrar((byte[])Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["foto"]));
                               }
                               catch { }*/
                f.ShowDialog(); 
                CargarDatos();
            }
        }
        //Boton Detalles
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            EDC.DetalleEmpleado.IdEmpleado = this.dtgvEmpleados.CurrentRow.Cells["IDEmpleado"].Value.ToString();
            EDC.DetalleEmpleado f = new EDC.DetalleEmpleado();
            f.txtID.Text = this.dtgvEmpleados.CurrentRow.Cells["IDEmpleado"].Value.ToString();
             f.txtNombre.Text = this.dtgvEmpleados.CurrentRow.Cells["Nombres"].Value.ToString() + " "+ this.dtgvEmpleados.CurrentRow.Cells["Apellidos"].Value.ToString();
            f.txtDUI.Text = this.dtgvEmpleados.CurrentRow.Cells["DUI"].Value.ToString();
            f.txtNIT.Text = this.dtgvEmpleados.CurrentRow.Cells["NIT"].Value.ToString();
            f.txtTelefono.Text = this.dtgvEmpleados.CurrentRow.Cells["telofono"].Value.ToString();
            f.txtDireccion.Text = this.dtgvEmpleados.CurrentRow.Cells["departamento"].Value.ToString()+" "+ this.dtgvEmpleados.CurrentRow.Cells["municipio"].Value.ToString()+" "+ this.dtgvEmpleados.CurrentRow.Cells["Direccion"].Value.ToString();
//            EDC.EdicionEmpleados.mun = this.dtgvEmpleados.CurrentRow.Cells["municipio"].Value.ToString();
            f.txtCorreo.Text = this.dtgvEmpleados.CurrentRow.Cells["correo"].Value.ToString();
  //          EDC.EdicionEmpleados.Sucursal = this.dtgvEmpleados.CurrentRow.Cells["nombre"].Value.ToString();
    //        EDC.EdicionEmpleados.Cargo = this.dtgvEmpleados.CurrentRow.Cells["Cargo"].Value.ToString();
            f.txtSueldo.Text = this.dtgvEmpleados.CurrentRow.Cells["sueldo"].Value.ToString();
            f.txtTitulo.Text = this.dtgvEmpleados.CurrentRow.Cells["titulo"].Value.ToString();
            f.txtCargo.Text = this.dtgvEmpleados.CurrentRow.Cells["cargo"].Value.ToString();
            f.dtpFecha.Text = this.dtgvEmpleados.CurrentRow.Cells["fechaNacimiento"].Value.ToString();
            try
            {
                f.fotoEmpleado.Image = Image.FromStream(Operaciones.PrepararImagenParaMostrar((byte[])dtgvEmpleados["foto", dtgvEmpleados.CurrentRow.Index].Value));
            }
            catch { }
            /*            f.txtDireccion.Text = Convert.ToString(Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["Departamento"]) + " "+ Convert.ToString(Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["Municipio"]) + " "+this.dtgvEmpleados.CurrentRow.Cells["Direccion"].Value.ToString();
                        f.txtTitulo.Text = Convert.ToString(Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["titulo"]);
                        f.txtCargo.Text = Convert.ToString(Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["Cargo"]);
                        f.dtpFecha.Text = this.dtgvEmpleados.CurrentRow.Cells["fechaNacimiento"].Value.ToString();
                        f.txtCorreo.Text = Convert.ToString(Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["correo"]);
                        f.txtSueldo.Text = Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["sueldo"].ToString();
            try
            {
                f.fotoEmpleado.Image = Image.FromStream(Operaciones.PrepararImagenParaMostrar((byte[])Pro.Rows[this.dtgvEmpleados.CurrentRow.Index]["foto"]));
            }
            catch { }*/
            f.ShowDialog();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

        }

        private void PanelTop_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
