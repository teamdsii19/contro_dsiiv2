﻿using ModernGUI_V3.EDC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.GUI
{
    public partial class GestionCompras : Form
    {
        BindingSource _Compras = new BindingSource();
        public GestionCompras()
        {
            InitializeComponent();
        }

        public void CargarDatos()
        {
            try
            {
                _Compras.DataSource = CacheManager.SystemCache.TODOSLASCOMPRAS();
                FiltrarLocalmente();
            }
            catch { }
        }
        public void FiltrarLocalmente()
        {
            try
            {
                if (txtFiltro.TextLength > 0)
                {
                    _Compras.Filter = "NombreProveedor LIKE '%" + txtFiltro.Text + "%'";
                }
                else
                {
                    _Compras.RemoveFilter();
                }
                dtgvCompras.AutoGenerateColumns = false;
                dtgvCompras.DataSource = _Compras;
                tsRegistro1.Text = dtgvCompras.Rows.Count.ToString() + " Registro encontrados";
            }
            catch
            {

            }
        }

        private void GestionCompras_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            EdicionCompras f = new EdicionCompras();
            f.checkPro.Visible = false;
            f.checkFecha.Visible = false;
            f.checkAnexo.Visible = false;
            f.ShowDialog();
            CargarDatos();
        }

        private void BtnEditar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea EDITAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                EdicionCompras f = new EdicionCompras();
                f.txtIDC.Text = dtgvCompras.CurrentRow.Cells["IDCompra"].Value.ToString();     
                f.dtgvProvedores.Enabled = false;
                f.btnSeleccionar.Enabled = false;
                f.btnVerArchivo.Enabled = false;
                f.ShowDialog();
            }
            CargarDatos();
        }

        private void MunuLateral_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void BtnDetalle_Click(object sender, EventArgs e)
        {
            DetallesCompra f = new DetallesCompra();
            f.IdCom = dtgvCompras.CurrentRow.Cells["IDCompra"].Value.ToString();
            f.ShowDialog();
            CargarDatos();
        }

        private void BtnBuscarDetalle_Click(object sender, EventArgs e)
        {
            ProductosCompra f = new ProductosCompra();
            f.IDcomp = dtgvCompras.CurrentRow.Cells["IDCompra"].Value.ToString();
            f.ShowDialog();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {

        }
    }
}
