﻿namespace ModernGUI_V3.GUI
{
    partial class CrearOrdenPedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtIDCliente = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNCliente = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dtgvProductos = new System.Windows.Forms.DataGridView();
            this.idProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrecioUnitario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Subtotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDDestallesPedidos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelBuscar = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.txtIDPedido = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtEstadoFactura = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboEmpleados = new System.Windows.Forms.ComboBox();
            this.txtNoFactura = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboTipoFactura = new System.Windows.Forms.ComboBox();
            this.btnPreparar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvProductos)).BeginInit();
            this.PanelBuscar.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtIDCliente
            // 
            this.txtIDCliente.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIDCliente.Location = new System.Drawing.Point(24, 113);
            this.txtIDCliente.Name = "txtIDCliente";
            this.txtIDCliente.ReadOnly = true;
            this.txtIDCliente.Size = new System.Drawing.Size(82, 23);
            this.txtIDCliente.TabIndex = 121;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(21, 93);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 17);
            this.label8.TabIndex = 120;
            this.label8.Text = "IDCliente";
            // 
            // txtNCliente
            // 
            this.txtNCliente.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtNCliente.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNCliente.Location = new System.Drawing.Point(23, 171);
            this.txtNCliente.Name = "txtNCliente";
            this.txtNCliente.ReadOnly = true;
            this.txtNCliente.Size = new System.Drawing.Size(422, 23);
            this.txtNCliente.TabIndex = 119;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(20, 151);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 17);
            this.label6.TabIndex = 118;
            this.label6.Text = "Nombre Cliente";
            // 
            // dtgvProductos
            // 
            this.dtgvProductos.AllowUserToAddRows = false;
            this.dtgvProductos.AllowUserToDeleteRows = false;
            this.dtgvProductos.AllowUserToOrderColumns = true;
            this.dtgvProductos.AllowUserToResizeRows = false;
            this.dtgvProductos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgvProductos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvProductos.BackgroundColor = System.Drawing.Color.White;
            this.dtgvProductos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtgvProductos.ColumnHeadersHeight = 28;
            this.dtgvProductos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idProducto,
            this.NombreProducto,
            this.PrecioUnitario,
            this.cantidad,
            this.Subtotal,
            this.IDDestallesPedidos});
            this.dtgvProductos.Location = new System.Drawing.Point(23, 285);
            this.dtgvProductos.Name = "dtgvProductos";
            this.dtgvProductos.RowHeadersVisible = false;
            this.dtgvProductos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgvProductos.Size = new System.Drawing.Size(870, 152);
            this.dtgvProductos.TabIndex = 117;
            // 
            // idProducto
            // 
            this.idProducto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.idProducto.DataPropertyName = "idProducto";
            this.idProducto.HeaderText = "idProducto";
            this.idProducto.Name = "idProducto";
            this.idProducto.ReadOnly = true;
            this.idProducto.Width = 75;
            // 
            // NombreProducto
            // 
            this.NombreProducto.DataPropertyName = "NombreProducto";
            this.NombreProducto.HeaderText = "Producto";
            this.NombreProducto.Name = "NombreProducto";
            this.NombreProducto.ReadOnly = true;
            // 
            // PrecioUnitario
            // 
            this.PrecioUnitario.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.PrecioUnitario.DataPropertyName = "PrecioUnitario";
            this.PrecioUnitario.HeaderText = "PrecioUnitario";
            this.PrecioUnitario.Name = "PrecioUnitario";
            this.PrecioUnitario.ReadOnly = true;
            this.PrecioUnitario.Width = 75;
            // 
            // cantidad
            // 
            this.cantidad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.cantidad.DataPropertyName = "cantidad";
            this.cantidad.HeaderText = "Cant. Pedido";
            this.cantidad.Name = "cantidad";
            this.cantidad.Width = 75;
            // 
            // Subtotal
            // 
            this.Subtotal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Subtotal.DataPropertyName = "Subtotal";
            this.Subtotal.HeaderText = "Sub Total";
            this.Subtotal.Name = "Subtotal";
            this.Subtotal.ReadOnly = true;
            this.Subtotal.Width = 75;
            // 
            // IDDestallesPedidos
            // 
            this.IDDestallesPedidos.DataPropertyName = "IDDestallesPedidos";
            this.IDDestallesPedidos.HeaderText = "IDDestallesPedidos";
            this.IDDestallesPedidos.Name = "IDDestallesPedidos";
            this.IDDestallesPedidos.Visible = false;
            // 
            // PanelBuscar
            // 
            this.PanelBuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(17)))), ((int)(((byte)(38)))));
            this.PanelBuscar.Controls.Add(this.label2);
            this.PanelBuscar.Controls.Add(this.lblUsuario);
            this.PanelBuscar.Controls.Add(this.txtIDPedido);
            this.PanelBuscar.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelBuscar.Location = new System.Drawing.Point(0, 0);
            this.PanelBuscar.Name = "PanelBuscar";
            this.PanelBuscar.Size = new System.Drawing.Size(913, 80);
            this.PanelBuscar.TabIndex = 122;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(12, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(356, 61);
            this.label2.TabIndex = 13;
            this.label2.Text = "FACTURACION ENVIO";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.BackColor = System.Drawing.Color.Transparent;
            this.lblUsuario.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblUsuario.Location = new System.Drawing.Point(414, 32);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(67, 17);
            this.lblUsuario.TabIndex = 98;
            this.lblUsuario.Text = "IDPedido";
            // 
            // txtIDPedido
            // 
            this.txtIDPedido.Enabled = false;
            this.txtIDPedido.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIDPedido.Location = new System.Drawing.Point(487, 29);
            this.txtIDPedido.Name = "txtIDPedido";
            this.txtIDPedido.ReadOnly = true;
            this.txtIDPedido.Size = new System.Drawing.Size(123, 23);
            this.txtIDPedido.TabIndex = 99;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(709, 93);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(109, 17);
            this.label9.TabIndex = 160;
            this.label9.Text = "Estado Factura:";
            // 
            // txtEstadoFactura
            // 
            this.txtEstadoFactura.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEstadoFactura.Location = new System.Drawing.Point(712, 113);
            this.txtEstadoFactura.Name = "txtEstadoFactura";
            this.txtEstadoFactura.ReadOnly = true;
            this.txtEstadoFactura.Size = new System.Drawing.Size(181, 23);
            this.txtEstadoFactura.TabIndex = 161;
            this.txtEstadoFactura.Text = "NO_CANCELADO";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(21, 213);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 17);
            this.label3.TabIndex = 163;
            this.label3.Text = "Empleado:";
            // 
            // comboEmpleados
            // 
            this.comboEmpleados.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboEmpleados.FormattingEnabled = true;
            this.comboEmpleados.Location = new System.Drawing.Point(24, 233);
            this.comboEmpleados.Name = "comboEmpleados";
            this.comboEmpleados.Size = new System.Drawing.Size(421, 28);
            this.comboEmpleados.TabIndex = 162;
            this.comboEmpleados.SelectedIndexChanged += new System.EventHandler(this.comboEmpleados_SelectedIndexChanged);
            // 
            // txtNoFactura
            // 
            this.txtNoFactura.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoFactura.Location = new System.Drawing.Point(480, 113);
            this.txtNoFactura.Name = "txtNoFactura";
            this.txtNoFactura.ReadOnly = true;
            this.txtNoFactura.Size = new System.Drawing.Size(181, 23);
            this.txtNoFactura.TabIndex = 165;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(477, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 17);
            this.label4.TabIndex = 164;
            this.label4.Text = "No Factura:";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(484, 171);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 17);
            this.label5.TabIndex = 167;
            this.label5.Text = "Tipo Factura:";
            // 
            // comboTipoFactura
            // 
            this.comboTipoFactura.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboTipoFactura.FormattingEnabled = true;
            this.comboTipoFactura.Items.AddRange(new object[] {
            "Comercial",
            "Ticket",
            "Credito Fiscal"});
            this.comboTipoFactura.Location = new System.Drawing.Point(487, 191);
            this.comboTipoFactura.Name = "comboTipoFactura";
            this.comboTipoFactura.Size = new System.Drawing.Size(236, 28);
            this.comboTipoFactura.TabIndex = 166;
            // 
            // btnPreparar
            // 
            this.btnPreparar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPreparar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(0)))), ((int)(((byte)(51)))));
            this.btnPreparar.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPreparar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnPreparar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnPreparar.Location = new System.Drawing.Point(723, 457);
            this.btnPreparar.Name = "btnPreparar";
            this.btnPreparar.Size = new System.Drawing.Size(170, 57);
            this.btnPreparar.TabIndex = 168;
            this.btnPreparar.Text = "Guardar/Imprimir Factura";
            this.btnPreparar.UseVisualStyleBackColor = false;
            // 
            // CrearOrdenPedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 526);
            this.Controls.Add(this.btnPreparar);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboTipoFactura);
            this.Controls.Add(this.txtNoFactura);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboEmpleados);
            this.Controls.Add(this.txtEstadoFactura);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.PanelBuscar);
            this.Controls.Add(this.txtIDCliente);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtNCliente);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dtgvProductos);
            this.Name = "CrearOrdenPedido";
            this.Text = "CrearOrdenPedido";
            this.Load += new System.EventHandler(this.CrearOrdenPedido_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvProductos)).EndInit();
            this.PanelBuscar.ResumeLayout(false);
            this.PanelBuscar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtIDCliente;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtNCliente;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dtgvProductos;
        private System.Windows.Forms.DataGridViewTextBoxColumn idProducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreProducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrecioUnitario;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Subtotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDDestallesPedidos;
        private System.Windows.Forms.Panel PanelBuscar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblUsuario;
        public System.Windows.Forms.TextBox txtIDPedido;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtEstadoFactura;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.ComboBox comboEmpleados;
        public System.Windows.Forms.TextBox txtNoFactura;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.ComboBox comboTipoFactura;
        private System.Windows.Forms.Button btnPreparar;
    }
}