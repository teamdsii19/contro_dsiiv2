﻿using ModernGUI_V3.EDC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.GUI
{
    public partial class GestionCliente : Form
    {
        BindingSource _Clientes = new BindingSource();
        public GestionCliente()
        {
            InitializeComponent();
        }

        public BindingSource Clientes { get => _Clientes; set => _Clientes = value; }
        public void CargarDatos()
        {
            try
            {
                _Clientes.DataSource = CacheManager.SystemCache.TODOS_CLIENTES();
                FiltrarLocalmente();
            }
            catch { }
        }
        public void FiltrarLocalmente()
        {
            try
            {
                if (txtFiltro.TextLength > 0)
                {
                    _Clientes.Filter = "NombreContacto LIKE '%" + txtFiltro.Text + "%'";
                }
                else
                {
                    _Clientes.RemoveFilter();
                }
                dtgvClientes.AutoGenerateColumns = false;
                dtgvClientes.DataSource = _Clientes;
                tsRegistro1.Text = dtgvClientes.Rows.Count.ToString() + " Registro encontrados";
            }
            catch
            {

            }
        }

        private void PanelBuscar_Paint(object sender, PaintEventArgs e)
        {

        }

        private void GestionCliente_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            EdicionCliente f = new EdicionCliente();
            f.checkactualizar.Visible = false;
            f.ShowDialog();
            CargarDatos();
        }

        private void BtnEditar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea EDITAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {

                EDC.EdicionCliente f = new EDC.EdicionCliente();
                f.Text = "Editar Empleado";
                f.txtidCliente.Text = dtgvClientes.CurrentRow.Cells["IDCliente"].Value.ToString();
                f.txtUser.Text = this.dtgvClientes.CurrentRow.Cells["NombreContacto"].Value.ToString();
                f.txtDireccion.Text = this.dtgvClientes.CurrentRow.Cells["Direccion"].Value.ToString();
                f.txtTelContacto.Text = this.dtgvClientes.CurrentRow.Cells["TelefoContacto"].Value.ToString();
                f.comboMunicipio.Enabled = false;
                f.comboDep.Enabled = false;
                f.ShowDialog();
                CargarDatos();
            }

                
            
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea ELIMINAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                CLS.Clientes oCliente = new CLS.Clientes();
                oCliente.IDCliente1 = dtgvClientes.CurrentRow.Cells["IDCliente"].Value.ToString();
                if (oCliente.Eliminar())
                {
                    MessageBox.Show("Registro eliminado exitosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDatos();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser eliminado exitosamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
    }
}
