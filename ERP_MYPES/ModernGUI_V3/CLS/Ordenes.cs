﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.CLS
{
    class Ordenes
    {
        public static bool InsertarOrden(String idEmpleado, String idSucursal, String EstadoOrden, String documento, String idCliente, int numero)
        {
            bool f = false;
            String serieFactura = DBManager.CLS.AppConfiguration.RecuperarValor("IDSerie", "1");//(CLS.Ordenes.TraerNumeroActualFactura().ToString());
            // idOrden, IDEmpleado, fechaOrden, IDSucursal, IDPedido, EstadoOrden, idCliente, tipoFactura, idSerie
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into ordenes (idSerie,idEmpleado,IDSucursal,EstadoOrden,tipoFactura,fechaOrden,idCliente,NoFactura,IDPedido) values (");
            Sentencia.Append(serieFactura + "," + idEmpleado + "," + idSucursal + ",'" + EstadoOrden + "','" + documento + "',current_date()," + idCliente + "," + numero + ",null);");
            MessageBox.Show(Sentencia.ToString());
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch (Exception exe)
            {
                MessageBox.Show(exe.StackTrace.ToString());
                f = false;
            }
            return f;
        }

        public static bool ActualizarExistencias(String id, int num)
        {
            bool f = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("update Productos set UnidadesEnStock=" + num + " where idProducto=" + id);
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f;
        }

        public static int TraerExistencias(String idProducto)
        {
            int re = 0;
            DataTable Resultado;
            DBManager.CLS.DBOperacion temp = new DBManager.CLS.DBOperacion();
            StringBuilder sentencia = new StringBuilder();
            sentencia.Append(@" select UnidadesEnStock from productos where idProducto=" + idProducto + ";");
            try
            {
                Resultado = temp.Consultar(sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return re = Convert.ToInt32(Resultado.Rows[0][0]);
        }

        public static int TraerNumeroActualFactura()
        {
            int re = 0;
            DataTable Resultado;
            DBManager.CLS.DBOperacion temp = new DBManager.CLS.DBOperacion();
            StringBuilder sentencia = new StringBuilder();
            String idS = DBManager.CLS.AppConfiguration.RecuperarValor("IDSerie", "1");
            MessageBox.Show("ID-Serie ALMACENADA: " + idS);
            sentencia.Append("select Actual from seriesfacturas where idSerie=" + idS);
            MessageBox.Show(sentencia.ToString());
            try
            {
                Resultado = temp.Consultar(sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return re = Convert.ToInt32(Resultado.Rows[0][0].ToString());
        }

        public static int ActualizarNumeroActualFactura(int num)
        {
            int re = 0;
            DataTable Resultado;
            DBManager.CLS.DBOperacion temp = new DBManager.CLS.DBOperacion();
            StringBuilder sentencia = new StringBuilder();
            String idS = DBManager.CLS.AppConfiguration.RecuperarValor("IDSerie", "1");
            //     MessageBox.Show("ID-Serie: " + idS);
            sentencia.Append(@"Update seriesfacturas set Actual= " + num + " where idSerie=" + idS);
            try
            {
                Resultado = temp.Consultar(sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
                //                MessageBox.Show("");
            }
            return re = Convert.ToInt32(Resultado.Rows.Count);// Resultado.Rows[0][0]);
        }


        public static int ActualizarOrden(int idOrden, String estado, String idCliente, String idEmpleado, String documento, String idSucursal)
        {
            int re = 0;
            DataTable Resultado;
            DBManager.CLS.DBOperacion temp = new DBManager.CLS.DBOperacion();
            StringBuilder sentencia = new StringBuilder();
            //idOrden, IDEmpleado, fechaOrden, IDSucursal, IDPedido, EstadoOrden, idCliente, tipoFactura, idSerie, NoFactura
            sentencia.Append(@"Update ordenes set EstadoOrden='" + estado + "', IDSucursal=" + idSucursal + ",IDEmpleado=" + idEmpleado + ",tipoFactura='" + documento + "',idCliente=" + idCliente);
            sentencia.Append(" where idOrden=" + idOrden + ";");
            try
            {
                Resultado = temp.Consultar(sentencia.ToString());
            }
            catch (Exception exec)
            {
                Resultado = new DataTable();
                MessageBox.Show("Problema" + exec.ToString());
                //                MessageBox.Show("");
            }
            return re = Convert.ToInt32(Resultado.Rows.Count);// Resultado.Rows[0][0]);
        }

        public static int TraerIDOrdenRecienIngresada(String numero)
        {
            int re = 0;
            DataTable Resultado;
            DBManager.CLS.DBOperacion temp = new DBManager.CLS.DBOperacion();
            StringBuilder sentencia = new StringBuilder();
            sentencia.Append(@" select idOrden from ordenes where noFactura=" + numero);
            try
            {
                Resultado = temp.Consultar(sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return re = Convert.ToInt32(Resultado.Rows[0][0]);
        }

        public static int TraerExistenciasSucursales(String idProducto, String sucursal)
        {
            int re = 0;
            DataTable Resultado;
            DBManager.CLS.DBOperacion temp = new DBManager.CLS.DBOperacion();
            StringBuilder sentencia = new StringBuilder();
            sentencia.Append(@" select UnidadesEnStock from productos where idProducto=" + idProducto + ";");
            try
            {
                Resultado = temp.Consultar(sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return re = Convert.ToInt32(Resultado.Rows[0][0]);
        }

        //  MANEJAR LAS SERIES DE FACTURA.......
        // idSerie, prefijo, inicio, fin, fecha, AutorizacionSerie, Actual

        public static bool InsertarSerie(String prefijo, String inicio, String fin, String autorizacion, String actual)
        {
            bool f = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into SeriesFacturas (prefijo,inicio,fin,fecha,AutorizacionSerie,Actual) values('");
            Sentencia.Append(prefijo + "'," + inicio + "," + fin + ",current_date(),'" + autorizacion + "'," + actual + ");");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f;
        }

        public static bool ActualizarSerie(String prefijo, String inicio, String fin, String autorizacion)
        {
            bool f = false;
            StringBuilder Sentencia = new StringBuilder();
            //,inicio,fin,fecha,AutorizacionSerie,Actual
            Sentencia.Append("update SeriesFacturas set prefijo= '" + prefijo + "',");
            Sentencia.Append("inicio= " + inicio + ",");
            Sentencia.Append("fin= " + fin + ",");
            Sentencia.Append("AutorizacionSerie= '" + autorizacion + "';");
            //    Sentencia.Append(prefijo + "," + inicio + "," + fin + "," + fecha + "," + autorizacion + "," + actual + ");");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f;
        }

        public static bool EliminarSerie(String idSerie)
        {
            bool f = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("delete from SeriesFacturas where idSerie=" + idSerie);
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f;
        }

        public static void InsertarDetalle(int idOrden, String idProducto, String descuento, String cantidad, String Precio)
        {
            bool f = false;
            StringBuilder Sentencia = new StringBuilder();
            // idDetalleOrden, idOrden, idProducto, cantidad, precioUnitario, descuento
            Sentencia.Append("insert into ordenesdetalles (idOrden, idProducto, cantidad, precioUnitario, descuento) values(");
            Sentencia.Append(idOrden + "," + idProducto + "," + cantidad + "," + Precio + "," + descuento + ");");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            MessageBox.Show(Sentencia.ToString());
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            //            return f;
        }

        public static bool ActualizarDetalle(int idOrden, String idProducto, int cantidad, Double descuento)
        {
            bool f = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("update ordenesdetalles set descuento= " + descuento + ",");
            Sentencia.Append("cantidad= " + cantidad);
            Sentencia.Append(" where idProducto= " + idProducto + " and idOrden=" + idOrden + " ;");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f;
        }

        public static void EliminarDetalle(String idOrden, String idProducto)
        {
            bool f = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("delete from ordenesdetalles");
            Sentencia.Append(" where idProducto= " + idProducto + " and idOrden=" + idOrden + " ;");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
        }

    }
}
