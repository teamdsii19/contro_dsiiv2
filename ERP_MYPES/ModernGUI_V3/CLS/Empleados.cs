﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CacheManager;
using MySql.Data.MySqlClient;

namespace ModernGUI_V3.CLS
{
    class Empleados
    {

        //IDEmpleado, Nombres, Apellidos, fechaNacimiento, DUI, NIT, Telofono, Direccion, notas, Titulo, 
        //idDepartamento, idMunicipio, Cargo, idCargo, nombre, nombreCorto, idSucursal, sueldo, idSueldo, foto, idfoto
        String _IDEmpleado;
        String _Nombres;
        String _Apellidos;
        String _DUI;
        String _NIT;
        String _titulo;
        String _fechaNacimiento;
        String _correo;
        String _telefono;
        String _Notas;
        String _idMunicipio;
        String _Direccion;
        byte[] _foto;
        String idDepartamento;
        String idMunicipio;
        String cargo;
        String idCargo;
        String nombreSucursal;
        String nombreCorto;
        String idSucursal;
        String sueldo;
        String idSueldo;
        String idFoto; 

        public string IDEmpleado { get => _IDEmpleado; set => _IDEmpleado = value; }
        public string Nombres { get => _Nombres; set => _Nombres = value; }
        public string Apellidos { get => _Apellidos; set => _Apellidos = value; }
        public string DUI { get => _DUI; set => _DUI = value; }
        public string NIT { get => _NIT; set => _NIT = value; }
        public string Titulo { get => _titulo; set => _titulo = value; }
        public string FechaNacimiento { get => _fechaNacimiento; set => _fechaNacimiento = value; }
        public string Correo { get => _correo; set => _correo = value; }
        public string Telefono { get => _telefono; set => _telefono = value; }
        public string Notas { get => _Notas; set => _Notas = value; }
        public string IdMunicipio { get => _idMunicipio; set => _idMunicipio = value; }
        public string Direccion { get => _Direccion; set => _Direccion = value; }
        public byte[] Foto { get => _foto; set => _foto = value; }
        public string IdDepartamento { get => idDepartamento; set => idDepartamento = value; }
        public string IdMunicipio1 { get => idMunicipio; set => idMunicipio = value; }
        public string Cargo { get => cargo; set => cargo = value; }
        public string IdCargo { get => idCargo; set => idCargo = value; }
        public string NombreSucursal { get => nombreSucursal; set => nombreSucursal = value; }
        public string NombreCorto { get => nombreCorto; set => nombreCorto = value; }
        public string IdSucursal { get => idSucursal; set => idSucursal = value; }
        public string Sueldo { get => sueldo; set => sueldo = value; }
        public string IdSueldo { get => idSueldo; set => idSueldo = value; }
        public string IdFoto { get => idFoto; set => idFoto = value; }

        public  bool GuardarEmpleado() {
            bool f = false;
            StringBuilder Sentencia = new StringBuilder();           
            Sentencia.Append("insert into empleados(Nombres, Apellidos, FechaNacimiento, DUI, NIT, Telofono, Direccion,correo,titulo,idMunicipio,notas) values(");
            Sentencia.Append("'" + _Nombres + "',");
            Sentencia.Append("'" + _Apellidos + "',");
            Sentencia.Append("'" + _fechaNacimiento + "',");
            Sentencia.Append("'" + _DUI + "',");
            Sentencia.Append("'" + _NIT + "',");
            Sentencia.Append("'" + _telefono + "',");
            Sentencia.Append("'" + _Direccion + "',");
            Sentencia.Append("'" + _correo + "',");
            Sentencia.Append("'" + _titulo + "',");
            Sentencia.Append("" + _idMunicipio + ",");
            Sentencia.Append("'" + _Notas + "');");
            MessageBox.Show(Sentencia.ToString());
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f; 
        }

        public void guardarFoto(PictureBox myPB,int idE){
            DBManager.CLS.DBOperacion temp = new DBManager.CLS.DBOperacion();
            StringBuilder sentencia = new StringBuilder();
//            sentencia.Append(@"insert into fotos (idEmpleado,fecha,foto) values(" + _IDEmpleado + @",current_date());");
            if (temp.GuardarFoto(Convert.ToString(idE), Operaciones.PrepararImagenParaGuardar(myPB)) > 0)
            {
                MessageBox.Show("Imagen guardada con éxito");
            }
            else
            {
                MessageBox.Show("Falló la inserción de imágen.");
            }
        }
        public void guardarFoto(PictureBox myPB, String idEmpleado)
        {
            DBManager.CLS.DBOperacion temp = new DBManager.CLS.DBOperacion();
            StringBuilder sentencia = new StringBuilder();
            //            sentencia.Append(@"insert into fotos (idEmpleado,fecha,foto) values(" + _IDEmpleado + @",current_date());");
            if (temp.GuardarFoto(idEmpleado, Operaciones.PrepararImagenParaGuardar(myPB)) > 0)
            {
                MessageBox.Show("Imagen guardada con éxito");
            }
            else
            {
                MessageBox.Show("Falló la inserción de imágen.");
            }
        }

        public int TraerIDRecienIngresado() {
            int re = 0;
            DataTable Resultado; 
            DBManager.CLS.DBOperacion temp = new DBManager.CLS.DBOperacion();
            StringBuilder sentencia = new StringBuilder();
            sentencia.Append(@" select idEmpleado from empleados where DUI= '"+_DUI+"';");
            try
            {
                Resultado = temp.Consultar(sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return re=Convert.ToInt32(Resultado.Rows[0][0]); 
        }

        public int TraerIDFotoEmpleado()
        {
            int re = 0;
            DataTable Resultado;
            DBManager.CLS.DBOperacion temp = new DBManager.CLS.DBOperacion();
            StringBuilder sentencia = new StringBuilder();
            sentencia.Append(@" select idEmpleado from empleados where DUI= '" + _DUI + "';");
            try
            {
                Resultado = temp.Consultar(sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return re = Convert.ToInt32(Resultado.Rows[0][0]);
        }

        public bool ActualizarFoto(PictureBox myPB)
        {
            bool re = false;
            DBManager.CLS.DBOperacion temp = new DBManager.CLS.DBOperacion();
            StringBuilder sentencia = new StringBuilder();
            //            sentencia.Append(@"insert into fotos (idEmpleado,fecha,foto) values(" + _IDEmpleado + @",current_date());");
            if (temp.ActualizarFoto(_IDEmpleado, Operaciones.PrepararImagenParaGuardar(myPB)) > 0)
            {
                re = true;
//                MessageBox.Show("Imagen guardada con éxito");
            }
            else
            {
                re = false;
  //              MessageBox.Show("Falló la inserción de imágen.");
            }
            return re; 
        }
        public  bool ActualizarEmpleado()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("UPDATE empleados SET ");
            Sentencia.Append("Nombres='" + _Nombres + "',");
            Sentencia.Append("Apellidos='" + _Apellidos + "',");
            Sentencia.Append("FechaNacimiento='" + _fechaNacimiento + "',");
            Sentencia.Append("DUI='" + _DUI + "',");
            Sentencia.Append("NIT='" + _NIT + "',");
            Sentencia.Append("Telofono='" + _telefono + "',");
            Sentencia.Append("correo='" + _correo + "',");
            Sentencia.Append("notas='" + _Notas + "',");
            Sentencia.Append("titulo='" + _titulo + "',");
            Sentencia.Append("Direccion='" + _Direccion + "' WHERE IDEmpleado='" + _IDEmpleado + "';");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
        public static bool GuardarSueldo(String idEmpleado,String valor)
        {
            bool f = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into AsignacionSueldo(valor, fechaAsignacion, idEmpleado) values(");
            Sentencia.Append("'" + valor + "',");
            Sentencia.Append("current_date(),");
            Sentencia.Append("'" +idEmpleado + "');");
           MessageBox.Show(Sentencia.ToString());
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f;
        }
        public static bool GuardarCargoE(String idEmpleado,String idCargo,String idSucursal,String Nota)
        {
            bool f = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into cargos_empleado(IDCargo, IDSucursal, IDEmpleado, fechaAsignacion, descripcion) values(");
            Sentencia.Append("'" + idCargo + "',");
            Sentencia.Append("'" + idSucursal + "',");
            Sentencia.Append("'" + idEmpleado + "',");
            Sentencia.Append("current_date(),");
            Sentencia.Append("'" + Nota + "');");
            MessageBox.Show(Sentencia.ToString());
            //           MessageBox.Show(Sentencia.ToString());
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f;
        }

        public static bool ActualizarCargoE(String idEmpleado, String idCargo, String idSucursal, String Nota,String idCargoEmpleado)
        {
            // IDCargoEmpleado, IDCargo, IDSucursal, IDEmpleado, fechaAsignacion, descripcion
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("UPDATE cargos_empleado SET ");
            Sentencia.Append("IDCargo='" + idCargo + "',");
            Sentencia.Append("IDSucursal='" + idSucursal + "',");
            Sentencia.Append("fechaAsignacion=current_date(),");
            Sentencia.Append("IDEmpleado='" + idEmpleado + "',");
            Sentencia.Append("descripcion='" + Nota + "' WHERE IDCargoEmpleado=" + idCargoEmpleado + ";");
            MessageBox.Show(Sentencia.ToString());
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
    }
}
