﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.CLS
{   //idUsuario, usuario, Credencial, idRol, idEmpleado, Estado
    class Usuarios
    {
        String idUsuario="";
        String usuario;
        String Credencial;
        String idRol;
        String idEmpleado;
        String Estado;

        public string IdEmpleado { get => idEmpleado; set => idEmpleado = value; }
        public string IdUsuario { get => idUsuario; set => idUsuario = value; }
        public string Usuario { get => usuario; set => usuario = value; }
        public string Credencial1 { get => Credencial; set => Credencial = value; }
        public string IdRol { get => idRol; set => idRol = value; }
        public string Estado1 { get => Estado; set => Estado = value; }

        public bool GuardarUsuario()
        {
            bool f = false;
            String Sentencia = "INSERT INTO usuarios(`usuario`, `Credencial`, `idRol`, `idEmpleado`, `Estado`) " +
                "VALUES('"+Usuario+"', SHA1('"+Credencial1+"'), '"+IdRol+"', '"+idEmpleado+"', '"+Estado1+"');";
            MessageBox.Show(Sentencia.ToString());
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f;
        }

        public bool ActualizarUsuario(int tipo)
        {
            
            bool f = false;
            String Sentencia = "UPDATE usuarios SET usuario='" + usuario + "'," +
            "idRol = '"+IdRol+"', idEmpleado = '" + idEmpleado+"', Estado = '"+Estado1+"' WHERE idUsuario = '"+IdUsuario+"'; ";
            if (tipo==1){
                Sentencia = "UPDATE usuarios SET usuario='" + usuario + "', Credencial=SHA1('" + Credencial1 + "')," +
            "idRol = '" + IdRol + "', idEmpleado = '" + idEmpleado + "', Estado = '" + Estado1 + "' WHERE idUsuario = '" + IdUsuario + "'; ";
            }
            MessageBox.Show(Sentencia.ToString());
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f;
        }

        public Boolean Eliminar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE FROM usuarios ");
            Sentencia.Append("WHERE idUsuario='" + idUsuario + "';");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }

    }
}
