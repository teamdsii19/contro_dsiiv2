﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace ModernGUI_V3.CLS
{
    class Generalidades
    {
        public static bool InsertarGenerales(String nombre, String mision, String vision, byte[] logo, byte[] organigrama, String nrc, String nit, String idEmpleado)
        {
            bool f = false;
            MySqlCommand Comando = new MySqlCommand("insert into generalidades (idGenereladidad,Nombre, mision, vision, logo, organigrama, nrc, NIT, idRepresentanteLegal) values(1,@n,@m,@v,@l,@or,@nrc,@nit,@idr);");
            Comando.Parameters.Add("@n", MySqlDbType.VarChar).Value = nombre;
            Comando.Parameters.Add("@m", MySqlDbType.MediumText).Value = mision;
            Comando.Parameters.Add("@v", MySqlDbType.MediumText).Value = vision;
            Comando.Parameters.Add("@l", MySqlDbType.MediumBlob).Value = logo;
            Comando.Parameters.Add("@or", MySqlDbType.MediumBlob).Value = organigrama;
            Comando.Parameters.Add("@nrc", MySqlDbType.VarChar).Value = nrc;
            Comando.Parameters.Add("@nit", MySqlDbType.VarChar).Value = nit;
            Comando.Parameters.Add("@idr", MySqlDbType.UInt32).Value = idEmpleado;
            /*            StringBuilder Comando = new StringBuilder("insert into generalidades (Nombre, mision, vision, logo, organigrama, nrc, NIT, idRepresentanteLegal) values('");
                        Comando.Append(nombre+"','"+mision+",','"+vision+",',null,null,'"+nrc+"','"+nit+"'," + idEmpleado+";");*/
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Comando.ToString()) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch (Exception exec)
            {
                f = false;
                MessageBox.Show(exec.StackTrace.ToString());
            }
            return f;
        }

        public static bool ActualizarGenerales(String id, String nombre, String mision, String vision, byte[] logo, byte[] organigrama, String nrc, String nit, String idEmpleado)
        {
            bool f = false;
            MySqlCommand Comando = new MySqlCommand("update generalidades set Nombre=@n, mision=@m, vision=@v, logo=@l, organigrama=@or, nrc=@nrc, NIT=@nit, idRepresentanteLegal=@idr where idGenereladidad=" + id);
            Comando.Parameters.Add("@n", MySqlDbType.VarChar).Value = nombre;
            Comando.Parameters.Add("@m", MySqlDbType.MediumText).Value = mision;
            Comando.Parameters.Add("@v", MySqlDbType.MediumText).Value = vision;
            Comando.Parameters.Add("@l", MySqlDbType.MediumBlob).Value = logo;
            Comando.Parameters.Add("@or", MySqlDbType.MediumBlob).Value = organigrama;
            Comando.Parameters.Add("@nrc", MySqlDbType.VarChar).Value = nrc;
            Comando.Parameters.Add("@nit", MySqlDbType.VarChar).Value = nit;
            Comando.Parameters.Add("@idr", MySqlDbType.UInt32).Value = idEmpleado;
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.ActualizarGenerico(Comando) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f;
        }

        public static bool EliminarGenerales(String id)
        {
            bool f = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("delete from generalidades where idGenereladidad=" + id);
            //            Sentencia.Append(" where idProducto= " + idProducto + " and idOrden=" + idOrden + " ;");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f;
        }
    }
}
