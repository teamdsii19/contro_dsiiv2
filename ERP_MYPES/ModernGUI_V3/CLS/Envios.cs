﻿using CacheManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.CLS
{
    //idEnvio, fecha_Requerido, fecha_Entrega, idRepartidor, IdTransporte, IDPedido, idMunicipio, direccion
    class Envios
    {
        String idEnvio;
        String fecha_Requerido;
        String fecha_Entrega;
        String idRepartidor;
        String IdTransporte;
        String IDPedido;
        String idMunicipio;
        String direccion;

        public string IdEnvio { get => idEnvio; set => idEnvio = value; }
        public string Fecha_Requerido { get => fecha_Requerido; set => fecha_Requerido = value; }
        public string Fecha_Entrega { get => fecha_Entrega; set => fecha_Entrega = value; }
        public string IdRepartidor { get => idRepartidor; set => idRepartidor = value; }
        public string IdTransporte1 { get => IdTransporte; set => IdTransporte = value; }
        public string IDPedido1 { get => IDPedido; set => IDPedido = value; }
        public string IdMunicipio { get => idMunicipio; set => idMunicipio = value; }
        public string Direccion { get => direccion; set => direccion = value; }

        public bool Guardar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("INSERT INTO envios(fecha_Requerido,fecha_Entrega, idRepartidor, IdTransporte, IDPedido, idMunicipio, direccion)"+
                                "VALUES('"+fecha_Requerido+"', '"+fecha_Entrega+"', '"+IdRepartidor+"', '"+IdTransporte+"', '"+IDPedido+"', '"+IdMunicipio+"', '"+direccion+"'); ");
            MessageBox.Show(Sentencia.ToString());
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                    SystemCache.ActualizarEstadoPedido(IDPedido,"PREPARADO");

                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;

            }
            return Guardado;
        }
    }
}
