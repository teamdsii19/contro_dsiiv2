﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.CLS
{
    class Proveedor
    {
        private String _idProveedor;
        private String _nombreProveedor;
        private String _nombreContacto;
        private String _tituloContacto;
        private String _telefonoContacto;
        private String _telefonoProveedor;
        private String _direccion;
        private String _idMunicipio;

        public string IdProveedor { get => _idProveedor; set => _idProveedor = value; }
        public string NombreProveedor { get => _nombreProveedor; set => _nombreProveedor = value; }
        public string NombreContacto { get => _nombreContacto; set => _nombreContacto = value; }
        public string TituloContacto { get => _tituloContacto; set => _tituloContacto = value; }
        public string TelefonoContacto { get => _telefonoContacto; set => _telefonoContacto = value; }
        public string TelefonoProveedor { get => _telefonoProveedor; set => _telefonoProveedor = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public string IdMunicipio { get => _idMunicipio; set => _idMunicipio = value; }

        public bool Guardar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into Proveedores(NombreProveedor, NombreContacto, TituloContacto, TelefoProveedor, TelefoContacto, Direccion, idMunicipio) values(");
            Sentencia.Append("'" + _nombreProveedor + "',");
            Sentencia.Append("'" + _nombreContacto + "',");
            Sentencia.Append("'" + _tituloContacto + "',");
            Sentencia.Append("'" + _telefonoProveedor + "',");
            Sentencia.Append("'" + _telefonoContacto + "',");
            Sentencia.Append("'" + _direccion + "',");
            Sentencia.Append("" + _idMunicipio + ");");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;

            }
            return Guardado;
        }
        public bool Actualizar()
        {
            // IDProveedor, NombreProveedor, NombreContacto, TituloContacto, TelefoProveedor, 
            // TelefoContacto, Direccion, idMunicipio
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("update proveedores set ");
            Sentencia.Append("NombreProveedor='" + _nombreProveedor + "',");
            Sentencia.Append("NombreContacto='" + _nombreContacto + "',");
            Sentencia.Append("TituloContacto='" + _tituloContacto + "',");
            Sentencia.Append("TelefoProveedor='" + _telefonoProveedor + "',");
            Sentencia.Append("TelefoContacto='" + _telefonoContacto + "',");
            Sentencia.Append("Direccion='" + _direccion + "',");
            Sentencia.Append("idMunicipio=" + _idMunicipio + " where IDProveedor=" + _idProveedor + ";");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            MessageBox.Show(Sentencia.ToString());
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;

            }
            return Guardado;
        }

        public bool Eliminar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE FROM Proveedores ");
            Sentencia.Append("WHERE IDProveedor=" + _idProveedor + ";");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;

            }
            return Guardado;
        }
    }
}
