﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.CLS
{
    class Clientes
    {
        String IDCliente;
        String NombreContacto;
        String TelefoContacto;
        String Direccion;
        String idMunicipio;

        public string IDCliente1 { get => IDCliente; set => IDCliente = value; }
        public string NombreContacto1 { get => NombreContacto; set => NombreContacto = value; }
        public string TelefoContacto1 { get => TelefoContacto; set => TelefoContacto = value; }
        public string Direccion1 { get => Direccion; set => Direccion = value; }
        public string IdMunicipio { get => idMunicipio; set => idMunicipio = value; }

        public bool GuardarCliente()
        {
            bool f = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("INSERT INTO clientes(NombreContacto,TelefoContacto,Direccion,idMunicipio) VALUES (");
            Sentencia.Append("'" + NombreContacto + "',");
            Sentencia.Append("'" + TelefoContacto + "',");
            Sentencia.Append("'" + Direccion + "',");
            Sentencia.Append("'" + idMunicipio + "');");

            MessageBox.Show(Sentencia.ToString());
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f;
        }

        public bool ActualizarCliente(int tipo)
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            if (tipo==1) {
            Sentencia.Append("UPDATE clientes SET ");
            Sentencia.Append("NombreContacto='"+NombreContacto+"', TelefoContacto='"+TelefoContacto+"',");
            Sentencia.Append(" Direccion='"+Direccion+"', idMunicipio='"+idMunicipio+"' WHERE IDCliente='"+IDCliente+"';");
            }
            else
            {
                Sentencia.Append("UPDATE clientes SET ");
                Sentencia.Append("NombreContacto='" + NombreContacto + "', TelefoContacto='" + TelefoContacto + "',");
                Sentencia.Append(" Direccion='" + Direccion + "' WHERE IDCliente='" + IDCliente + "';");
            }

            MessageBox.Show(Sentencia+"");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }

        public Boolean Eliminar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE FROM clientes ");
            Sentencia.Append("WHERE IDCliente='" + IDCliente + "';");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }

    }
}
