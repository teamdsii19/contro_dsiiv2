﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModernGUI_V3.CLS
{
    class CreandoTicket
    {
        private StringBuilder linea = new StringBuilder();

        int maxCar = 40, cortar;// Pensado para una impresora de 40 columnas y cortar es para respetar el limite

        public StringBuilder Linea { get => linea; set => linea = value; }

        public string lineasGuia()
        {

            String lineasGuias = "";
            for (int i = 0; i < maxCar; i++)
            {
                lineasGuias += "-";
            }
            lineasGuias += "\n";
            return Linea.Append(lineasGuias).ToString();
        }

        public string lineasAsteriscos()
        {
            String lineasAstericos = "";
            for (int i = 0; i < maxCar; i++)
            {
                lineasAstericos += "*";
            }
            lineasAstericos += "\n";
            return Linea.Append(lineasAstericos).ToString();
        }
        public string lineasIgual()
        {
            String lineasIgual = "";
            for (int i = 0; i < maxCar; i++)
            {
                lineasIgual += "=";
            }
            lineasIgual += "\n";
            return Linea.Append(lineasIgual).ToString();
        }

        public void EncabezadoVenta()
        {
            string p = "ARTICULO           |CANT|PRECIO|IMPORTE";
            linea.Append(p);
        }

        public void TextoIzquierdo(String texto)
        {
            if (texto.Length > maxCar)
            { // Si se pasa de la longitud de carro maxima
                int caracterActual = 0;
                for (int longitud = texto.Length; longitud > maxCar; longitud -= maxCar)
                {
                    Linea.AppendLine(texto.Substring(caracterActual, maxCar));
                    caracterActual += maxCar;
                }
                Linea.AppendLine(texto.Substring(caracterActual, texto.Length - caracterActual));
            }
            else
            {
                Linea.AppendLine(texto);
            }
        }

        public void TextoDerecha(String texto)
        {
            if (texto.Length > maxCar)
            { // Si se pasa de la longitud de carro maxima
                int caracterActual = 0;
                for (int longitud = texto.Length; longitud > maxCar; longitud -= maxCar)
                {
                    Linea.AppendLine(texto.Substring(caracterActual, maxCar));
                    caracterActual += maxCar;
                }
                String espacios = " ";
                for (int i = 0; i < (maxCar - texto.Substring(caracterActual, texto.Length - caracterActual).Length); i++)
                {
                    espacios += " ";
                }
                Linea.AppendLine(espacios + texto.Substring(caracterActual, texto.Length - caracterActual));

            }
            else
            {
                String espacios = " ";
                for (int i = 0; i < (maxCar - texto.Length); i++)
                {
                    espacios += " ";
                }
                Linea.AppendLine(espacios + texto);
            }
        }

        public void TextoCentro(String texto)
        {
            if (texto.Length > maxCar)
            { // Si se pasa de la longitud de carro maxima
                int caracterActual = 0;
                for (int longitud = texto.Length; longitud > maxCar; longitud -= maxCar)
                {
                    Linea.AppendLine(texto.Substring(caracterActual, maxCar));
                    caracterActual += maxCar;
                }
                String espacios = " ";
                int center = (maxCar - texto.Substring(caracterActual, texto.Length - caracterActual).Length) / 2;
                for (int i = 0; i < (maxCar - texto.Substring(caracterActual, texto.Length - caracterActual).Length); i++)
                {
                    espacios += " ";
                }
                Linea.AppendLine(espacios + texto.Substring(caracterActual, texto.Length - caracterActual));

            }
            else
            {
                String espacios = " ";
                int center = (maxCar - texto.Length) / 2;
                for (int i = 0; i < center; i++)
                {
                    espacios += " ";
                }
                Linea.AppendLine(espacios + texto);
            }
        }

        public void TextoEstremos(String textoIzquierdo, String textoDerecho)
        {
            String textoIzq, textoDer, textoCompleto = "", espacios = "";
            if (textoIzquierdo.Length > 18)
            {
                cortar = textoIzquierdo.Length - 18;
                textoIzq = textoIzquierdo.Remove(18, cortar);

            }
            else
            {
                textoIzq = textoIzquierdo;
            }

            if (textoDerecho.Length > 20)
            {
                cortar = textoDerecho.Length - 20;
                textoDer = textoDerecho.Remove(20, cortar);
            }
            else
            {
                textoDer = textoDerecho;
            }

            int numEspacios = maxCar - (textoIzq.Length + textoDer.Length);
            for (int j = 0; j < numEspacios; j++)
            {
                espacios += " ";
            }
            textoCompleto += espacios + textoDerecho;
            Linea.AppendLine(textoCompleto);
            //            linea.AppendLine();
        }

        public void AgregarTotales(String texto, decimal total)
        {
            string resumen, valor, textoCompleto, espacios = " ";
            if (texto.Length > 25)
            {
                cortar = texto.Length - 25;
                resumen = texto.Remove(25, cortar);
            }
            else
            {
                resumen = texto;
            }
            textoCompleto = resumen;
            valor = total.ToString("#,#.00");

            int numEspacions = maxCar - (resumen.Length + valor.Length);
            for (int i = 0; i < numEspacions; i++)
            {
                espacios += " ";
            }
            textoCompleto += espacios + valor;
            Linea.Append(textoCompleto);

        }

        public void AgregarArticulo(String articulo, int cant, decimal precio, decimal importe)
        {
            // 
            if (cant.ToString().Length <= 5 && precio.ToString().Length <= 7 && importe.ToString().Length <= 8)
            {
                String elemento = "", espacios = "";
                bool bandera = false;
                int numEspacios = 0;

                // Si el nombre del articulo es mayor a 20, bajar a otra linea
                if (articulo.Length > 20)
                {
                    numEspacios = (5 - cant.ToString().Length);
                    espacios = "";
                    for (int i = 0; i < numEspacios; i++)
                    {
                        espacios += " ";
                    }
                    elemento += espacios + cant.ToString();

                    //colocando el precio a la derecha
                    numEspacios = (7 - precio.ToString().Length);
                    espacios = "";
                    for (int i = 0; i < numEspacios; i++)
                    {
                        espacios += " ";
                    }
                    elemento += espacios + precio.ToString();

                    numEspacios = (8 - importe.ToString().Length);
                    espacios = "";
                    for (int i = 0; i < numEspacios; i++)
                    {
                        espacios += " ";
                    }
                    elemento += espacios + importe.ToString();

                    int caracterActual = 0;
                    for (int longitudtexto = articulo.Length; longitudtexto > 20; longitudtexto -= 20)
                    {
                        if (bandera == false)
                        {
                            Linea.AppendLine(articulo.Substring(caracterActual, 20) + elemento);
                            bandera = true;
                        }
                        else
                        {
                            Linea.AppendLine(articulo.Substring(caracterActual, 20));
                        }
                        caracterActual += 20;
                    }
                    Linea.AppendLine(articulo.Substring(caracterActual, articulo.Length - caracterActual));
                }
                else
                {
                    for (int i = 0; i < (20 - articulo.Length); i++)
                    {
                        espacios += " ";
                    }
                    elemento = articulo + espacios;
                    numEspacios = (5 - cant.ToString().Length);
                    espacios = "";
                    for (int i = 0; i < numEspacios; i++)
                    {
                        espacios += " ";
                    }
                    elemento += espacios + cant.ToString();

                    numEspacios = (7 - precio.ToString().Length);
                    espacios = "";
                    for (int i = 0; i < numEspacios; i++)
                    {
                        espacios += " ";
                    }
                    elemento += espacios + precio.ToString();

                    numEspacios = (8 - importe.ToString().Length);
                    espacios = "";
                    for (int i = 0; i < numEspacios; i++)
                    {
                        espacios += " ";
                    }
                    elemento += espacios + importe.ToString();
                    Linea.AppendLine(elemento);
                }
            }
            else
            {
                Linea.AppendLine("Los valores ingresados para esta fila");
                Linea.AppendLine("superan las columnas soportados por este");
                //                linea.AppendLine("Los valores ingresados para esta fila");
                throw new Exception("Los valores ingresados para algunas filas del ticket\nsuperan las columnas de este.");
            }
        }

    }
}
