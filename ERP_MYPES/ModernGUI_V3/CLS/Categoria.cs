﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.CLS
{
    class Categoria
    {
        private static String idCategoria;
        private static String categorias;
        private static String descripcion;

        public static string IdCategoria { get => idCategoria; set => idCategoria = value; }
        public static string Categorias { get => categorias; set => categorias = value; }
        public static string Descripcion { get => descripcion; set => descripcion = value; }

        public static bool Guardar(String nombre, String des)
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into Categorias(NombreCategroria,Descripcion) values('");
            Sentencia.Append(nombre + "','" + des + "');");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;

            }
            return Guardado;
        }
        public static bool Actualizar(String Nombre, String idc, String descripcion)
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("Update categorias set NombreCategroria='" + Nombre);
            Sentencia.Append("' , descripcion='" + descripcion);
            Sentencia.Append("' WHERE IDCategoria=" + idc + ";");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            MessageBox.Show(Sentencia.ToString());
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;

            }
            return Guardado;
        }
        public static bool Eliminar(String idc)
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE FROM Categorias ");
            Sentencia.Append("WHERE IdCategoria=" + idc + ";");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;

            }
            return Guardado;
        }
    }
}
