﻿using CacheManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.CLS
{
    class Pedido
    {
        //IDPedido, IDCliente, IDSucursal, FechaPedido, FechaEntrega, EstadoPedido
        String IDPedido;
        String IDCliente;
        String IDSucursal;
        String FechaPedido;
        String FechaEntrega;
        String EstadoPedido;
        String NumPedido;

        public string IDPedido1 { get => IDPedido; set => IDPedido = value; }
        public string IDSucursal1 { get => IDSucursal; set => IDSucursal = value; }
        public string FechaPedido1 { get => FechaPedido; set => FechaPedido = value; }
        public string FechaEntrega1 { get => FechaEntrega; set => FechaEntrega = value; }
        public string EstadoPedido1 { get => EstadoPedido; set => EstadoPedido = value; }
        public string NumPedido1 { get => NumPedido; set => NumPedido = value; }
        public string IDCliente1 { get => IDCliente; set => IDCliente = value; }

        public bool Guardar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("INSERT INTO pedidos(`IDCliente`, `IDSucursal`, `FechaPedido`, `FechaEntrega`, `EstadoPedido`, `NumPedido`)"+
            " VALUES('"+IDCliente+"','"+IDSucursal+"', '"+FechaPedido+"', '"+FechaPedido+"', 'PENDIENTE', '"+NumPedido+"');");
            MessageBox.Show(Sentencia.ToString());
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;

            }
            return Guardado;
        }

        public bool DetallePedido(String idPed, String idPro, String cant)
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("INSERT INTO "+
            "detallespedidos(`IDPedido`, `IDProducto`, `Cantidad`) "+
            "VALUES('"+ idPed + "', '"+ idPro + "', '"+ cant + "');");
            MessageBox.Show(Sentencia.ToString());
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                    SystemCache.DescontarStock(idPro,cant);
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;

            }
            return Guardado;
        }

        public bool ActualizarPedido()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("UPDATE pedidos SET FechaEntrega='"+FechaEntrega+"', "+
                "IDCliente = '"+IDCliente+"' "+
                "WHERE IDPedido = '"+IDPedido+"'; ");


            MessageBox.Show(Sentencia + "");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
        //DELETE FROM detallespedidos WHERE IDDestallesPedidos='4';
        public Boolean EliminarDetalle(String iddp, String idp, String cant)
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE FROM detallespedidos WHERE IDDestallesPedidos='"+iddp+"';");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                    SystemCache.AgregarAStock(idp,cant);
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }

        public Boolean EliminarEnCascada(String idp)
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE dp, pd FROM detallespedidos dp "+
                            "LEFT JOIN pedidos pd "+
                            "ON dp.IDPedido = pd.IDPedido "+
                            "WHERE dp.IDPedido = '"+ idp + "'; ");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }

        public Boolean EliminarDetalle(String iddp)
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE FROM detallespedidos WHERE IDDestallesPedidos='" + iddp + "';");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                    //SystemCache.AgregarAStock(idp, cant);
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }

    }
}
