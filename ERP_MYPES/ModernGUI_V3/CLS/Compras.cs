﻿using CacheManager;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.CLS
{
    class Compras
    {
        //IDCompra, IDProveedor, FechaFactura, FechaIngreso, descripcion, AnexoDocumento
        int IDCompra;
        int IDProveedor;
        String FechaFactura;
        String FechaIngreso;
        String descripcion;
        byte[] AnexoDocumento;

        public int IDCompra1 { get => IDCompra; set => IDCompra = value; }
        public int IDProveedor1 { get => IDProveedor; set => IDProveedor = value; }
        public String FechaFactura1 { get => FechaFactura; set => FechaFactura = value; }
        public String FechaIngreso1 { get => FechaIngreso; set => FechaIngreso = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public byte[] AnexoDocumento1 { get => AnexoDocumento; set => AnexoDocumento = value; }

        public bool Guardar()
        {
            bool f = false;
            MySqlCommand Comando = new MySqlCommand("INSERT INTO compras (IDProveedor,FechaFactura,FechaIngreso,descripcion,AnexoDocumento) " +
                "VALUES (@idProveedor,@fechaFAC,@fechaING,@descri,@AnexoDocumento);");
            Comando.Parameters.Add("@descri", MySqlDbType.VarChar).Value = descripcion;
            Comando.Parameters.Add("@idProveedor", MySqlDbType.Int32).Value = IDProveedor;
            Comando.Parameters.Add("@fechaFAC", MySqlDbType.VarChar).Value = FechaFactura;
            Comando.Parameters.Add("@fechaING", MySqlDbType.VarChar).Value = FechaIngreso1;
            Comando.Parameters.Add("@AnexoDocumento", MySqlDbType.MediumBlob).Value = AnexoDocumento;
            MessageBox.Show(descripcion);
            MessageBox.Show(IDProveedor+"");
            MessageBox.Show(FechaFactura + "");
            MessageBox.Show(FechaIngreso + "");
            MessageBox.Show(System.Text.Encoding.UTF8.GetString(AnexoDocumento));

            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.GuardarGenerico(Comando) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f;
        }

        public bool ActualizarFecha()
        {
            bool f = false;
            String Comando = "UPDATE compras SET FechaIngreso='"+FechaIngreso+"' WHERE IDCompra='"+IDCompra+"';";
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Actualizar( Comando) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f;
        }

        public bool ActualizarIDProveedor()
        {
            bool f = false;
            String Comando = "UPDATE compras SET IDProveedor='" + IDProveedor + "' WHERE IDCompra='" + IDCompra + "';";
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Actualizar(Comando) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f;
        }

        public bool ActualizarAnexo()
        {
            bool f = false;
            MySqlCommand Comando = new MySqlCommand("UPDATE compras SET AnexoDocumento=@fot WHERE IDCompra='"+IDCompra+"';");
            Comando.Parameters.Add("@fot", MySqlDbType.MediumBlob).Value = AnexoDocumento;
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.ActualizarGenerico(Comando) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f;
        }

        public bool DetalleCompra(String producto,String cant,String precio)
        {
            bool f = false;
            String Comando = "INSERT INTO comprasdetalles (idCompra, idProducto, cantidad, precioCosto) "+
            "VALUES('"+IDCompra+"', '"+producto+"', '"+cant+"', '"+precio+"'); ";
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Comando) > 0)
                {
                    f = true;
                    SystemCache.AgregarAStock(producto,cant);

                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f;
        }

        public bool ElimiarDetalleCompra(String idDetalle,String idP, String cant)
        {
            bool f = false;
            String Comando = "DELETE FROM comprasdetalles WHERE idComprasDetalles='"+idDetalle+"'; ";
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Comando) > 0)
                {
                    f = true;
                    SystemCache.DescontarStock(idP,cant);
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f;
        }


    }
}
