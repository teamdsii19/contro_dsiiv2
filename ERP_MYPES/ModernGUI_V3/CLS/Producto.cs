﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace ModernGUI_V3.CLS
{
    class Producto
    {
        String _IDProducto;
        String _NombreProducto;
        String _Alias;
        String _codigoBarras;
        String _IDProveedor, _IDCategoria, _MedidaPorUnidad, _PrecioUnitario, _UnidadesEnStock, _UnidadesOrdenadas, _minimo;
        String _idSucursal;
        String _precioSucursal;
        String _idProductoSucursal;
        String _Costo;
        byte[] imagen;
        public string IDProducto { get => _IDProducto; set => _IDProducto = value; }
        public string NombreProducto { get => _NombreProducto; set => _NombreProducto = value; }
        public string Alias { get => _Alias; set => _Alias = value; }
        public string CodigoBarras { get => _codigoBarras; set => _codigoBarras = value; }
        public string IDProveedor { get => _IDProveedor; set => _IDProveedor = value; }
        public string IDCategoria { get => _IDCategoria; set => _IDCategoria = value; }
        public string MedidaPorUnidad { get => _MedidaPorUnidad; set => _MedidaPorUnidad = value; }
        public string PrecioUnitario { get => _PrecioUnitario; set => _PrecioUnitario = value; }
        public string UnidadesEnStock { get => _UnidadesEnStock; set => _UnidadesEnStock = value; }
        public string UnidadesOrdenadas { get => _UnidadesOrdenadas; set => _UnidadesOrdenadas = value; }
        public string Minimo { get => _minimo; set => _minimo = value; }
        public string IdSucursal { get => _idSucursal; set => _idSucursal = value; }
        public string PrecioSucursal { get => _precioSucursal; set => _precioSucursal = value; }
        public string IdProductoSucursal { get => _idProductoSucursal; set => _idProductoSucursal = value; }
        public string Costo { get => _Costo; set => _Costo = value; }
        public byte[] Imagen { get => imagen; set => imagen = value; }

        public bool GuardarProducto()
        {
            bool f = false;
            MySqlCommand Comando = new MySqlCommand("insert into Productos(NombreProducto,Alias,MedidaPorUnidad,precioUnitario,idCategoria,codigoBarras,idProveedor,minimo,costo,UnidadesEnStock,fecha,imagen)" +
                "values(@nombre,@alias,@medida,@precio,@idc,@codigo,@idp,@minimo,@costo,@cantidad,current_date(),@foto);");
            Comando.Parameters.Add("@nombre", MySqlDbType.VarChar).Value = _NombreProducto;
            Comando.Parameters.Add("@alias", MySqlDbType.VarChar).Value = _Alias;
            Comando.Parameters.Add("@medida", MySqlDbType.VarChar).Value = _MedidaPorUnidad;
            Comando.Parameters.Add("@precio", MySqlDbType.Double).Value = Convert.ToDouble(_PrecioUnitario);
            Comando.Parameters.Add("@idc", MySqlDbType.Int32).Value = Convert.ToInt32(_IDCategoria);
            Comando.Parameters.Add("@idp", MySqlDbType.Int32).Value = Convert.ToInt32(_IDProveedor);
            Comando.Parameters.Add("@codigo", MySqlDbType.VarChar).Value = _codigoBarras;
            Comando.Parameters.Add("@cantidad", MySqlDbType.UInt32).Value = Convert.ToInt32(_UnidadesEnStock);
            Comando.Parameters.Add("@minimo", MySqlDbType.UInt32).Value = Convert.ToInt32(_minimo);
            Comando.Parameters.Add("@costo", MySqlDbType.Double).Value = Convert.ToDouble(_Costo);
            Comando.Parameters.Add("@foto", MySqlDbType.MediumBlob).Value = imagen;
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.GuardarGenerico(Comando) > 0)
                {
                    f = true;
                }
                else
                {
                    f = false;
                }
            }
            catch
            {
                f = false;
            }
            return f;
        }

        public bool ActualizarProducto(String idProducto)
        {
            bool Guardado = false;
            MySqlCommand Comando = new MySqlCommand("Update  Productos set NombreProducto=@nombre,Alias=@alias,MedidaPorUnidad=@medida,precioUnitario=@precio,idCategoria=@idc,codigoBarras=@codigo,idProveedor=@idp,minimo=@minimo,costo=@costo,UnidadesEnStock=@cantidad,imagen=@foto where IDProducto=" + idProducto + ";");
            Comando.Parameters.Add("@nombre", MySqlDbType.VarChar).Value = _NombreProducto;
            Comando.Parameters.Add("@alias", MySqlDbType.VarChar).Value = _Alias;
            Comando.Parameters.Add("@medida", MySqlDbType.VarChar).Value = _MedidaPorUnidad;
            Comando.Parameters.Add("@precio", MySqlDbType.Double).Value = Convert.ToDouble(_PrecioUnitario);
            Comando.Parameters.Add("@idc", MySqlDbType.Int32).Value = Convert.ToInt32(_IDCategoria);
            Comando.Parameters.Add("@idp", MySqlDbType.Int32).Value = Convert.ToInt32(_IDProveedor);
            Comando.Parameters.Add("@codigo", MySqlDbType.VarChar).Value = _codigoBarras;
            Comando.Parameters.Add("@cantidad", MySqlDbType.UInt32).Value = Convert.ToInt32(_UnidadesEnStock);
            Comando.Parameters.Add("@minimo", MySqlDbType.UInt32).Value = Convert.ToInt32(_minimo);
            Comando.Parameters.Add("@costo", MySqlDbType.Double).Value = Convert.ToDouble(_Costo);
            Comando.Parameters.Add("@foto", MySqlDbType.MediumBlob).Value = imagen;
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.ActualizarGenerico(Comando) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }

        public static Boolean Eliminar(String idProducto)
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE FROM Productos ");
            Sentencia.Append("WHERE IDProducto=" + idProducto + ";");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;

            }
            return Guardado;
        }
    }
}
