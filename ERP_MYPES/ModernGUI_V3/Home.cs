﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3
{
    public partial class Home : Form
    {
        SessionManager.Sesion _SESION = SessionManager.Sesion.Instancia;
        BindingSource _Productos = new BindingSource();

        public Home()
        {
            InitializeComponent();
        }

        private void Home_Load(object sender, EventArgs e)
        {
            if (DBManager.CLS.AppConfiguration.RecuperarValor("ModalidadVariacionProducto", false))
            {
                MessageBox.Show("Bienvenido");
            }
            else {
                this.panelVariante.Dispose();
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbHora.Text = DateTime.Now.ToLongTimeString();
            lbFecha.Text = DateTime.Now.ToLongDateString();

        }

        public void CargarDatos() {
            dtgvProductosHome.AutoGenerateColumns = false;
            _Productos.DataSource = CacheManager.SystemCache.TODOSLOSPRODUCTOS();
            this.FiltrarLocalmente();
        }
  
        public void FiltrarLocalmente()
        {
            //            FiltrarLocalmente();
            _Productos.DataSource = CacheManager.SystemCache.TODOSLOSPRODUCTOS();
            try
            {
                if (txtFiltro.TextLength >0 )
                {
                    _Productos.Filter = "NombreProducto LIKE '%" + txtFiltro.Text + "%' OR Alias LIKE '% " + txtFiltro.Text + "%' OR codigoBarras LIKE '%"+txtFiltro.Text+"%'";
                    //   tsRegistro1.Text = dtgvProductosHome.Rows.Count.ToString() + " Registro encontrados";
                    //   dtgvProductosHome.DataSource = _Productos;
                               dtgvProductosHome.AutoGenerateColumns = false;
                               dtgvProductosHome.DataSource = _Productos;
                }
                else
                {
                    _Productos.RemoveFilter();
                    _Productos.DataSource=null;
                    dtgvProductosHome.AutoGenerateColumns = false;
                    dtgvProductosHome.DataSource = _Productos;

                    /*  _Productos.Clear();
                      dtgvProductosHome.Rows.Clear();*/
//                    dtgvProductosHome.Rows.Clear();
                                     dtgvProductosHome.Refresh();              
                }
///                dtgvProductosHome.AutoGenerateColumns = false;
   //             dtgvProductosHome.DataSource = _Productos;
                tsRegistro1.Text = dtgvProductosHome.Rows.Count.ToString() + " Registro encontrados";
            }
            catch
            {
            }
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            this.FiltrarLocalmente();
            if (this.panelVariante.IsAccessible) {
                MessageBox.Show("Esta accesible");
            }
            else {
                MessageBox.Show("No es accesible");
            }
//            this.txtFiltro.Clear();
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
             this.FiltrarLocalmente();            
        }

        private void txtFiltro_KeyPress(object sender, KeyPressEventArgs e)
        {
           /* if (txtFiltro.Text.Length==1 && (int)e.KeyChar==(int)Keys.Delete){
                try
                {
                        for (int i = dtgvProductosHome.Rows.Count; i <= 0; i--)
                        {
                            dtgvProductosHome.Rows.RemoveAt(i - 1);
                        }
                   // this.dtgvProductosHome.DataSource = null;
                    this.dtgvProductosHome.Rows.Clear();
                    this.dtgvProductosHome.Refresh();
                }
                catch { }
            }*/
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (_SESION.OUsuario.VerificarPermiso(1)){
                MessageBox.Show("Si puede vender");
            }
        }

        private void txtFiltro_TextChanged_1(object sender, EventArgs e)
        {
            this.FiltrarLocalmente();
        }
    }
}
