﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.EDC
{
    public partial class SeleccionarPedidosPendientes : Form
    {
        BindingSource _PedidosTable = new BindingSource();
        public SeleccionarPedidosPendientes()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void SeleccionarPedidosPendientes_Load(object sender, EventArgs e)
        {
            CargarDatos("PENDIENTE");
        }

        public void CargarDatos(String estado)
        {
            try
            {
                _PedidosTable.DataSource = CacheManager.SystemCache.PedidosporEstado(estado);
                FiltrarLocalmente();
            }
            catch { }
        }
        public void FiltrarLocalmente()
        {
            try
            {
                if (txtFiltro.TextLength > 0)
                {
                    _PedidosTable.Filter = "NombreCliente LIKE '%" + txtFiltro.Text + "%'";
                }
                else
                {
                    _PedidosTable.RemoveFilter();
                }
                dtgvPedidos.AutoGenerateColumns = false;
                dtgvPedidos.DataSource = _PedidosTable;
            }
            catch
            {

            }
        }

        private void btnCategorias_Click(object sender, EventArgs e)
        {
            EdicionEnvios en = new EdicionEnvios();
            en.txtIDPedido.Text = dtgvPedidos.CurrentRow.Cells["IDPedido"].Value.ToString();
            en.txtIDCliente.Text = dtgvPedidos.CurrentRow.Cells["IDCliente"].Value.ToString();
            en.lblCliente.Text = dtgvPedidos.CurrentRow.Cells["NombreCliente"].Value.ToString();
            en.dtpFecRequerida.Text = dtgvPedidos.CurrentRow.Cells["FechaEntrega"].Value.ToString();
            en.ShowDialog();

        }
    }
}
