﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.EDC
{
    public partial class PanelProveedores : Form
    {
        ArrayList AMunicipios;
        BindingSource _sourceDate = new BindingSource();

        public ArrayList AMunicipios1 { get => AMunicipios; set => AMunicipios = value; }

        public PanelProveedores()
        {
            InitializeComponent();
        }

        public void CargarDatos()
        {
            try
            {
                _sourceDate.DataSource = CacheManager.SystemCache.TODOSLOSPROVEEDORES();
                dtgvCategorias.AutoGenerateColumns = false;
                dtgvCategorias.DataSource = _sourceDate;
            }
            catch
            {
            }
        }

        private void MostrarEnTexts()
        {
            // IDProveedor, NombreProveedor, NombreContacto, TituloContacto, 
            //TelefoProveedor, TelefoContacto, Direccion, municipio, idMunicipio, dm
            txtID.Text = this.dtgvCategorias["IDProveedor", dtgvCategorias.CurrentRow.Index].Value.ToString();
            txtNombrePro.Text = this.dtgvCategorias["NombreProveedor", dtgvCategorias.CurrentRow.Index].Value.ToString();
            txtContacto.Text = this.dtgvCategorias["NombreContacto", dtgvCategorias.CurrentRow.Index].Value.ToString();
            txtTitulo.Text = this.dtgvCategorias["TituloContacto", dtgvCategorias.CurrentRow.Index].Value.ToString();
            txtTelPro.Text = this.dtgvCategorias["TelefoProveedor", dtgvCategorias.CurrentRow.Index].Value.ToString();
            txtTelContacto.Text = this.dtgvCategorias["TelefoContacto", dtgvCategorias.CurrentRow.Index].Value.ToString();
            txtDireccion.Text = this.dtgvCategorias["Direccion", dtgvCategorias.CurrentRow.Index].Value.ToString();
            comboMunicipios.SelectedItem = this.dtgvCategorias["municipio", dtgvCategorias.CurrentRow.Index].Value.ToString();
            //   comboMunicipios.SelectedValue= this.dtgvCategorias["municipio", dtgvCategorias.CurrentRow.Index].Value.ToString();
            txtDepMunicipio.Text = this.dtgvCategorias["dm", dtgvCategorias.CurrentRow.Index].Value.ToString();
        }

        private void LimpiarTextos()
        {
            this.txtID.Text = "";
            this.txtNombrePro.Text = "";
            this.txtTelPro.Text = "";
            this.txtContacto.Text = "";
            this.txtTelContacto.Text = "";
            this.txtTitulo.Text = "";
            this.txtDireccion.Text = "";
            this.txtDepMunicipio.Text = "";
            this.comboMunicipios.SelectedText = "";
        }

        private bool Valido()
        {
            Boolean Verificado = true;
            Notificador.Clear();
            if (txtNombrePro.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtNombrePro, "Este campo no puede quedar vacío");
            }

            if (txtTelContacto.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtTelContacto, "Este campo no puede quedar vacío");
            }
            if (txtContacto.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtContacto, "Este campo no puede quedar vacío");
            }

            if (txtDireccion.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtDireccion, "Este campo no puede quedar vacío");
            }
            if (txtTelPro.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtTelPro, "Este campo no puede quedar vacío");
            }
            if (txtTitulo.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtTitulo, "Este campo no puede quedar vacío");
            }
            if (comboMunicipios.SelectedIndex < 0)
            {
                Verificado = false;
                Notificador.SetError(comboMunicipios, "Este campo no puede quedar vacío");
            }

            return Verificado;
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            this.LimpiarTextos();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            CLS.Proveedor proveedor = new CLS.Proveedor();
            proveedor.IdProveedor = txtID.Text;
            if (this.dtgvCategorias.CurrentRow.Index != -1)
            {
                if (this.txtID.Text.Equals(""))
                {
                    MessageBox.Show("Debe elegir un registro para la operación");
                }
                else
                {
                    if (proveedor.Eliminar())
                    {
                        MessageBox.Show("Operación realizada con éxito");
                        this.CargarDatos();
                        this.LimpiarTextos();
                    }
                    else
                    {
                        MessageBox.Show("No se realizó la operación");
                    }
                }
            }

        }

        private void btnGuardarE_Click(object sender, EventArgs e)
        {
            if (this.dtgvCategorias.CurrentRow.Index != -1)
            {
                if (txtID.Text.Equals(""))
                {
                    MessageBox.Show("Seleccione el elemento a actualizar,e intentelo de nuevo.");
                }
                else
                {
                    if (Valido())
                    {
                        Procesar();
                        this.LimpiarTextos();
                    }
                }
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {          
            if (this.dtgvCategorias.Rows.Count > 0)
            {
                if (this.dtgvCategorias.CurrentRow.Index != -1)
                {
                    if (txtID.Text.Equals(""))
                    {
                        if (Valido())
                        {
                            Procesar();
                            LimpiarTextos();
                        }
                    }
                    else
                    {
                        MessageBox.Show("El campo ID debe estar limpio.\nDé click en el botón limpiar, ingrese los nuevos datos e intentelo de nuevo.");
                    }
                }
            }
            else
            {
                if (txtID.Text.Equals(""))
                {
                    if (Valido())
                    {
                        Procesar();
                        LimpiarTextos();
                    }
                }
            }

        }


        private void Procesar()
        {
            CLS.Proveedor oProveedor = new CLS.Proveedor();
            oProveedor.IdProveedor = txtID.Text;
            oProveedor.NombreProveedor = txtNombrePro.Text;
            oProveedor.TelefonoProveedor = txtTelPro.Text;
            oProveedor.NombreContacto = txtContacto.Text;
            oProveedor.TelefonoContacto = txtTelContacto.Text;
            oProveedor.TituloContacto = txtTitulo.Text;
            oProveedor.IdMunicipio = AMunicipios[comboMunicipios.SelectedIndex].ToString();
            oProveedor.Direccion = txtDireccion.Text;
            if (txtID.Text.Length == 0)
            {
                //Insertar 
                if (oProveedor.Guardar())
                {
                    MessageBox.Show("Operación Realizada con éxito");
                }
                else
                {
                    MessageBox.Show("No se realizó la operación");
                }
            }
            else
            {
                // Actualizar
                if (oProveedor.Actualizar())
                {
                    MessageBox.Show("Operación Realizada con éxito");
                }
                else
                {
                    MessageBox.Show("No se realizó la operación");
                }
            }
            this.CargarDatos();
        }


        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PanelProveedores_Load(object sender, EventArgs e)
        {
            this.CargarDatos();
            this.AMunicipios = CacheManager.Operaciones.LlenarComboBox(this.comboMunicipios, "idMunicipio", "Municipio", "Municipios");
        }

        private void dtgvCategorias_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.dtgvCategorias.CurrentRow.Index != -1)
                {
                    this.MostrarEnTexts();
                }
            }
            catch (Exception)
            {
            }

        }

        private void dtgvCategorias_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if (this.dtgvCategorias.CurrentRow.Index != -1)
                {
                    this.MostrarEnTexts();
                }
            }
            catch (Exception)
            {
            }

        }

        private void dtgvCategorias_MouseClick(object sender, MouseEventArgs e)
        {

            try
            {
                if (this.dtgvCategorias.CurrentRow.Index != -1)
                {
                    this.MostrarEnTexts();
                }
            }
            catch (Exception)
            {
            }

        }

        private void txtTelPro_KeyPress(object sender, KeyPressEventArgs e)
        {
            CacheManager.Operaciones.SoloNumeros(e);
        }

        private void txtTelContacto_KeyPress(object sender, KeyPressEventArgs e)
        {
            CacheManager.Operaciones.SoloNumeros(e);
        }
    }
}
