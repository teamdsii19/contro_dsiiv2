﻿using CacheManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.EDC
{
    public partial class EdicionCompras : Form
    {
        BindingSource _sourceDate = new BindingSource();
        public EdicionCompras()
        {
            InitializeComponent();

            this.StartPosition = FormStartPosition.CenterParent;
            dtpFecIngreso.Value = DateTime.Now;
            dtpFecFactura.Value = DateTime.Now;
        }
        public void CargarDatos()
        {
            try
            {
                _sourceDate.DataSource = CacheManager.SystemCache.TODOSLOSPROVEEDORES();
                dtgvProvedores.AutoGenerateColumns = false;
                dtgvProvedores.DataSource = _sourceDate;
            }
            catch
            {
            }
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void DtpFecha_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void EdicionCompras_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void PDucumento_Click(object sender, EventArgs e)
        {
           
        }

        private void BtnSeleccionar_Click(object sender, EventArgs e)
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "C:\\";
            openFileDialog1.Filter = "PDF(*.pdf)|*.pdf|IMG(*.jpg)|*.jpg";
            openFileDialog1.FilterIndex = 0;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            txtFile.Text = openFileDialog1.FileName;

                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al seleccionar el archivo " + ex.Message);
                }
            }
        }

        private void BtnVerArchivo_Click(object sender, EventArgs e)
        {
           if( Verificacion()){
                try{
                     Operaciones.VerArchivo(txtFile.Text);
                }
                catch
                {
                    MessageBox.Show("Error al Abrir el Archivo");
                }
            }
        }

        private void TxtFile_TextChanged(object sender, EventArgs e)
        {

        }

        private Boolean Verificacion()
        {
            Boolean Verificado = true;
            Notificador.Clear();
            if (txtFile.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtFile, "Este campo no puede quedar vacío");
            }
            return Verificado;
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            
                Procesar();
            
        }

        public void Procesar()
        {
            CLS.Compras oCompra = new CLS.Compras();
            oCompra.IDProveedor1 = Convert.ToInt32(dtgvProvedores.CurrentRow.Cells["IDProveedor"].Value.ToString());
            oCompra.FechaFactura1 = dtpFecFactura.Text;
            oCompra.FechaIngreso1 = dtpFecIngreso.Text;
            oCompra.Descripcion = txtDescripcion.Text;
            oCompra.AnexoDocumento1 = Operaciones.Convertirbyte(txtFile.Text);

            if (Verificacion() && txtIDC.TextLength == 0)
            {
                //Estoy insertando un nuevo regitro
                if (oCompra.Guardar())
                {
                    MessageBox.Show("Registro guardado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser guardado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            if( txtIDC.TextLength != 0)
            {
                oCompra.IDCompra1 = Convert.ToInt32( txtIDC.Text);
                if (checkFecha.Checked)
                {
                    if(oCompra.ActualizarFecha())
                    MessageBox.Show("Fecha Actalizada");
                    
                }
                if (checkPro.Checked)
                {
                    if (oCompra.ActualizarIDProveedor())
                        MessageBox.Show("Proveedor Actalizado");
                   
                }
                if (checkAnexo.Checked && Verificacion())
                {
                    if (oCompra.ActualizarAnexo())
                        MessageBox.Show("Anexo Actalizado");
                    
                }
                Close();

            }
        }

        private void CheckFecha_CheckedChanged(object sender, EventArgs e)
        {
            if (checkFecha.Checked)
            {
                dtpFecFactura.Enabled = true;
            }
            else
            {
                dtpFecFactura.Enabled = false;
            }
        }

        private void CheckAnexo_CheckedChanged(object sender, EventArgs e)
        {
            if (checkAnexo.Checked)
            {
                btnVerArchivo.Enabled = true;
                btnSeleccionar.Enabled = true;
            }
            else
            {
                btnVerArchivo.Enabled = false;
                btnSeleccionar.Enabled = false;
            }
        }

        private void CheckPro_CheckedChanged(object sender, EventArgs e)
        {
            if (checkPro.Checked)
                dtgvProvedores.Enabled = true;
            else
                dtgvProvedores.Enabled = false;
        }
    }
}
