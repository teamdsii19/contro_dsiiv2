﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.EDC
{
    public partial class EdicionEnvios : Form
    {

        public EdicionEnvios()
        {
            InitializeComponent();
            CargarDepartamentos();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void btnPreparar_Click(object sender, EventArgs e)
        {
            if (txtIDEnvio.TextLength == 0)
            {
                //Estoy insertando un nuevo regitro
                if (GuardarEnvio())
                {
                    MessageBox.Show("Registro guardado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser guardado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
        private void CargarDepartamentos()
        {
            try
            {
                this.comboDep.DataSource = CacheManager.SystemCache.TODOSDEPARTAMENTOS();
                this.comboDep.ValueMember = "idDepartamento";
                this.comboDep.DisplayMember = "Departamento";
                this.comboDep.SelectedIndex = 0;
                CargarMunicipios();
            }
            catch { }
        }

        private void CargarMunicipios()
        {
            try
            {
                this.comboMunicipio.DataSource = CacheManager.SystemCache.TODOSMUNICIPIOS(this.comboDep.SelectedValue.ToString());
                this.comboMunicipio.ValueMember = "idMunicipio";
                this.comboMunicipio.DisplayMember = "Municipio";
                this.comboMunicipio.SelectedIndex = 0;
            }
            catch { }
        }
        private Boolean GuardarEnvio()
        {
            CLS.Envios NEnvio = new CLS.Envios();
            NEnvio.Fecha_Requerido = dtpFecRequerida.Text;
            NEnvio.Fecha_Entrega = dtpFechaEntrega.Text;
            NEnvio.IdRepartidor = txtIDRepartidor.Text;
            NEnvio.IdTransporte1 = txtidTransportista.Text;
            NEnvio.IDPedido1 = txtIDPedido.Text;
            NEnvio.IdMunicipio = comboMunicipio.SelectedValue.ToString();
            NEnvio.Direccion = txtDireccion.Text;
            return NEnvio.Guardar();

        }

        private void btnBuscarProducto_Click(object sender, EventArgs e)
        {
            BuscarTranportista bt = new BuscarTranportista();
            bt.ShowDialog();
            txtidTransportista.Text = bt.IdTransporte;
            txtTransportista.Text = bt.NombreTrasporte;
        }

        private void btnBuscarCliente_Click(object sender, EventArgs e)
        {
            BuscarEmpleado bt = new BuscarEmpleado();
            bt.ShowDialog();
            txtIDRepartidor.Text = bt.Id;
            txtEmpleado.Text = bt.Nombre;
        }

        private void EdicionEnvios_Load(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void comboDep_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void BtnGuardaFac_Click(object sender, EventArgs e)
        {
            GUI.CrearOrdenPedido nu = new GUI.CrearOrdenPedido();
            nu.txtIDPedido.Text = this.txtIDPedido.Text;
            nu.txtNCliente.Text = this.lblCliente.Text;
            nu.txtIDCliente.Text = this.txtIDCliente.Text;
            nu.IdPedido1 = this.txtIDPedido.Text;
            nu.ShowDialog();
        }
    }
}
