﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CacheManager;

namespace ModernGUI_V3.EDC
{
    public partial class EdicionEmpleados : Form
    {
        ArrayList dep=new ArrayList();
        ArrayList muni = new ArrayList();
        ArrayList cargos = new ArrayList();
        ArrayList sucursales = new ArrayList();
        private String idSucursal;
        private String sueldoOriginal; 
        private String idCargoEmpleado;
        private String idEmpleado;
        public String descripcionOriginal; 
        private static String cargo;
        private static String sucursal;
        public static String departamento;
        public static String mun;
        public string Departamento { get => departamento; set => departamento = value; }
        public string Mun { get => mun; set => mun = value; }
        public ArrayList Cargos { get => cargos; set => cargos = value; }
        public ArrayList Sucursales { get => sucursales; set => sucursales = value; }
        public static string Cargo { get => cargo; set => cargo = value; }
        public static string Sucursal { get => sucursal; set => sucursal = value; }
        public string IdSucursal { get => idSucursal; set => idSucursal = value; }
        public string IdCargoEmpleado { get => idCargoEmpleado; set => idCargoEmpleado = value; }
    //    public string IdSueldo { get => idSueldo; set => idSueldo = value; }
        public string SueldoOriginal { get => sueldoOriginal; set => sueldoOriginal = value; }
        public string IdEmpleado { get => idEmpleado; set => idEmpleado = value; }

        public EdicionEmpleados()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Operaciones.CargarImagenSO(this.fotoEmpleado); 
        }

        private void EdicionEmpleados_Load(object sender, EventArgs e)
        {
            this.dep=Operaciones.LlenarComboBox(this.comboDepartamentos,"idDepartamento","departamento","Departamentos");
            this.muni=Operaciones.LlenarComboBox(this.ComboMunicipios, "idMunicipio", "municipio", "municipios");
            this.sucursales=Operaciones.LlenarComboBox(this.comboSucursal,"IDSucursal","Nombre","sucursales");
            this.cargos = Operaciones.LlenarComboBox(this.comboCargo, "IDCargo", "NombreCargo", "cargos");
            try
            {
                this.ComboMunicipios.Text = mun;
                this.comboDepartamentos.Text = departamento;
                this.comboSucursal.Text = sucursal;
                this.comboCargo.Text = cargo;
            }
            catch
            {
                this.ComboMunicipios.SelectedIndex = this.ComboMunicipios.Items.Count - 1;
                this.comboDepartamentos.SelectedIndex = this.comboDepartamentos.Items.Count - 1;
                this.comboCargo.SelectedIndex = this.comboCargo.Items.Count - 1;
                this.comboSucursal.SelectedIndex = this.comboSucursal.Items.Count - 1;
            }
        }


        private Boolean Verificacion()
        {
            Boolean Verificado = true;
            Notificador.Clear();
            if (txtNombres.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtNombres, "Este campo no puede quedar vacío");
            }

            if (txtApellidos.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtApellidos, "Este campo no puede quedar vacío");
            }

            if (dtpFecha.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(dtpFecha, "Este campo no puede quedar vacío");
            }


            if (txtNIT.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtNIT, "Este campo no puede quedar vacío");
            }

            if (txtDUI.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtDUI, "Este campo no puede quedar vacío");
            }

            if (txtTelefono.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtTelefono, "Este campo no puede quedar vacío");
            }

            if (txtDireccion.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtDireccion, "Este campo no puede quedar vacío");
            }

            return Verificado;
        }

        public void Procesar()
        {
            CLS.Empleados oEmpleado = new CLS.Empleados();
            oEmpleado.IDEmpleado = txtID.Text;
            oEmpleado.Nombres = txtNombres.Text;
            oEmpleado.Apellidos = txtApellidos.Text;
            oEmpleado.FechaNacimiento = dtpFecha.Text;
            oEmpleado.DUI = txtDUI.Text;
            oEmpleado.NIT = txtNIT.Text;
            oEmpleado.Telefono = txtTelefono.Text;
            oEmpleado.Direccion = txtDireccion.Text;
            oEmpleado.IdDepartamento = dep[(comboDepartamentos.SelectedIndex)].ToString();
            oEmpleado.IdMunicipio = muni[(ComboMunicipios.SelectedIndex)].ToString();
            oEmpleado.Telefono = txtTelefono.Text;
            oEmpleado.Correo = txtCorreo.Text;
            oEmpleado.Notas = txtNotas.Text;
            oEmpleado.Titulo = txtTitulo.Text;
            oEmpleado.Foto = Operaciones.PrepararImagenParaGuardar(fotoEmpleado);

            if (txtID.TextLength == 0)
            {
                //Estoy insertando un nuevo regitro
                if (oEmpleado.GuardarEmpleado())
                {
                    MessageBox.Show("Registro guardado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    oEmpleado.guardarFoto(this.fotoEmpleado,oEmpleado.TraerIDRecienIngresado());
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser guardado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                if (oEmpleado.ActualizarEmpleado())
                {
                    MessageBox.Show("Registro actualizado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    if (oEmpleado.ActualizarFoto(this.fotoEmpleado)){
                        MessageBox.Show("Se Actualizó la foto con exito");
                    }
                    else{
                        oEmpleado.guardarFoto(this.fotoEmpleado, oEmpleado.IDEmpleado);
                    }
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser actualizado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }



        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void fotoEmpleado_Click(object sender, EventArgs e)
        {
            Operaciones.CargarImagenSO(this.fotoEmpleado);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (Verificacion()){
                Procesar();
                Close();
            }
        }


        public bool ValidoGuardar() {
            bool re = true;
            if (txtSueldo.Text.Equals("") || comboSucursal.SelectedItem.Equals("") || comboCargo.SelectedItem.Equals("")) {
                re = false;
            }
            if (Convert.ToDouble(txtSueldo.Text)<200){
                re = false;
            }
            return re;
        }

        private void btnGuardarAdmin_Click(object sender, EventArgs e)
        {
            if (ValidoGuardar())
            {
                if (ExisteRegistroCargo() && HayCambios())
                {
                    CLS.Empleados.ActualizarCargoE(idEmpleado, this.Cargos[(comboCargo.SelectedIndex)].ToString(), sucursales[(comboSucursal.SelectedIndex)].ToString(), txtDescripcionCargo.Text, IdCargoEmpleado);
                }
                else
                {
                    if (!ExisteRegistroCargo()) {
                        CLS.Empleados.GuardarCargoE(idEmpleado, Cargos[(comboCargo.SelectedIndex)].ToString(), sucursales[(comboSucursal.SelectedIndex)].ToString(), txtDescripcionCargo.Text);
                    }
                }
                if (HayCambiosSueldo())
                {
                    CLS.Empleados.GuardarSueldo(idEmpleado,txtSueldo.Text);
                }
                Close();
            }
            else {
                MessageBox.Show("Existen campos con valores no vàlidos");
            }
        }

        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            Operaciones.SoloNumerosSinEspacio(e);
        }

        private void txtNombres_KeyPress(object sender, KeyPressEventArgs e)
        {
            Operaciones.SoloLetras(e);
        }

        private void txtApellidos_KeyPress(object sender, KeyPressEventArgs e)
        {
            Operaciones.SoloLetras(e);
        }

        private bool ExisteRegistroSueldo() {
            bool re = true;
            if (sueldoOriginal.Equals(""))
            {
                re = false;
            }
            return re; 
        }
        private bool ExisteRegistroCargo()
        {
            bool re = true;
            if (IdCargoEmpleado.Equals("")){
                re = false;
            }
            return re;
        }

        private bool HayCambios(){
            bool re = false;
            if (!cargo.Equals(comboCargo.SelectedItem.ToString())) {
                re = true;
            }
            if (!(this.descripcionOriginal.Equals(this.txtDescripcionCargo.Text))){
                re = true;
            }
            if (!sucursal.Equals(comboSucursal.SelectedItem))
            {
                re = true;
            }
            return re;
        }

        private bool HayCambiosSueldo()
        {
            bool re = false;
            if (!sueldoOriginal.Equals(txtSueldo.Text))
            {
                re = true;
            }
            return re;
        }


        private void tabControlEEmpleados_MouseClick(object sender, MouseEventArgs e)
        {
            if (tabControlEEmpleados.Enabled==false) {
                MessageBox.Show("Debe Ingresar los datos generales antes de configurar estos datos");
            }
        }

        private void txtSueldo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Operaciones.NumerosDecimales(e);
        }

        private void TpGeneralidades_Click(object sender, EventArgs e)
        {

        }
    }
}
