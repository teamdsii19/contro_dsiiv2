﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.EDC
{
    public partial class DetalleProducto : Form
    {
        private static String categoria;
        private static String proveedor;
        private static String costo;
        private static String precio;
        public static string Categoria { get => categoria; set => categoria = value; }
        public static string Proveedor { get => proveedor; set => proveedor = value; }
        public static string Costo { get => costo; set => costo = value; }
        public static string Precio { get => precio; set => precio = value; }

        public DetalleProducto()
        {
            InitializeComponent();
        }


    /*    private void DetalleProducto_Load(object sender, EventArgs e)
        {
            try
            {
                this.txtCategoria.Text = Categoria;
                this.txtProveedor.Text = proveedor;
                Double cos, pre;
                cos = Convert.ToDouble(Costo);
                pre = Convert.ToDouble(precio);
                this.txtBeneficio.Text = (pre - cos).ToString();
           //     this.txtBeneficio.ForeColor = Color.Blue;
            }
            catch {
                MessageBox.Show("Ocurre un error");
            }        
        }*/

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DetalleProducto_Load(object sender, EventArgs e)
        {
            try
            {
                this.txtCategoria.Text = Categoria;
                this.txtProveedor.Text = proveedor;
                Double cos, pre;
                cos = Convert.ToDouble(Costo);
                pre = Convert.ToDouble(precio);
                this.txtBeneficio.Text = (pre - cos).ToString();
                this.txtBeneficio.ForeColor = Color.Blue;
            }
            catch
            {
                MessageBox.Show("Ocurre un error");
            }
        }
    }
}
