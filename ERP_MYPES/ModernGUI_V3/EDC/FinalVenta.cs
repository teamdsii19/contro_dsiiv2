﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModernGUI_V3.CLS;

namespace ModernGUI_V3.EDC
{
    public partial class FinalVenta : Form
    {
        Double efectivo, total, devolucion;
        CreandoTicket ticket = new CreandoTicket();
        DataTable datos;

        public FinalVenta()
        {
            InitializeComponent();
        }

        private void FinalVenta_Load(object sender, EventArgs e)
        {
            datos = CacheManager.SystemCache.ParaUnaFactura(GUI.Ventas.idVenta.ToString());
            //   tiempo.Start();
            this.txtEfectivo.Focus();

        }

        public FinalVenta(String documento, String efectivo)
        {
            InitializeComponent();
            txtDocumento.Text = documento;
            txtTotal.Text = efectivo;
        }


        public void CrearTicketDeVenta()
        {
            ticket.TextoCentro(datos.Rows[0][0].ToString());
            ticket.TextoCentro("TEL: 23434567");
            ticket.TextoCentro("NRC" + datos.Rows[0][1]);
            ticket.TextoCentro("NIT" + datos.Rows[0][2]);
            ticket.lineasIgual();
            ticket.TextoIzquierdo("Cajero: " + datos.Rows[0][25]);
            ticket.lineasIgual();
            //         ticket.lineasGuia();

            ticket.EncabezadoVenta();
            ///            ticket.lineasGuia();
            ticket.Linea.AppendLine();
            for (int i = 0; i < datos.Rows.Count; i++)
            {
                ticket.AgregarArticulo(datos.Rows[i][15].ToString(),
                    Convert.ToInt32(datos.Rows[i][17]),
                    Convert.ToDecimal(datos.Rows[i][16]),
                    Convert.ToDecimal(datos.Rows[i][23]));
            }
            ticket.Linea.AppendLine();
            ticket.Linea.AppendLine();
            ticket.AgregarTotales("Total.....", Convert.ToDecimal(datos.Rows[0][24]));
            ticket.Linea.AppendLine();
            ticket.AgregarTotales("Efectivo.....", Convert.ToDecimal(txtEfectivo.Text.ToString()));
            ticket.Linea.AppendLine();
            ticket.AgregarTotales("Devolucion.....", Convert.ToDecimal(txtCambio.Text.ToString()));
            ticket.lineasIgual();
            ParaUnaFactura tick = new ParaUnaFactura(ticket.Linea.ToString());
            tick.ShowDialog();
        }

    }
}
