﻿namespace ModernGUI_V3.EDC
{
    partial class EdicionEmpleados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EdicionEmpleados));
            this.Notificador = new System.Windows.Forms.ErrorProvider(this.components);
            this.tabControlEEmpleados = new System.Windows.Forms.TabControl();
            this.tpGeneralidades = new System.Windows.Forms.TabPage();
            this.txtTitulo = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.fotoEmpleado = new System.Windows.Forms.PictureBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.txtNotas = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.ComboMunicipios = new System.Windows.Forms.ComboBox();
            this.comboDepartamentos = new System.Windows.Forms.ComboBox();
            this.txtNIT = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDUI = new System.Windows.Forms.TextBox();
            this.txtApellidos = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNombres = new System.Windows.Forms.TextBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tpAdministrativos = new System.Windows.Forms.TabPage();
            this.btnGuardarAdmin = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtDescripcionCargo = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtSueldo = new System.Windows.Forms.TextBox();
            this.comboCargo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.comboSucursal = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Notificador)).BeginInit();
            this.tabControlEEmpleados.SuspendLayout();
            this.tpGeneralidades.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fotoEmpleado)).BeginInit();
            this.tpAdministrativos.SuspendLayout();
            this.SuspendLayout();
            // 
            // Notificador
            // 
            this.Notificador.ContainerControl = this;
            // 
            // tabControlEEmpleados
            // 
            this.tabControlEEmpleados.Controls.Add(this.tpGeneralidades);
            this.tabControlEEmpleados.Controls.Add(this.tpAdministrativos);
            this.tabControlEEmpleados.Location = new System.Drawing.Point(-5, 0);
            this.tabControlEEmpleados.Multiline = true;
            this.tabControlEEmpleados.Name = "tabControlEEmpleados";
            this.tabControlEEmpleados.SelectedIndex = 0;
            this.tabControlEEmpleados.Size = new System.Drawing.Size(667, 562);
            this.tabControlEEmpleados.TabIndex = 34;
            this.tabControlEEmpleados.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tabControlEEmpleados_MouseClick);
            // 
            // tpGeneralidades
            // 
            this.tpGeneralidades.Controls.Add(this.txtTitulo);
            this.tpGeneralidades.Controls.Add(this.label16);
            this.tpGeneralidades.Controls.Add(this.label13);
            this.tpGeneralidades.Controls.Add(this.label10);
            this.tpGeneralidades.Controls.Add(this.label11);
            this.tpGeneralidades.Controls.Add(this.label2);
            this.tpGeneralidades.Controls.Add(this.label5);
            this.tpGeneralidades.Controls.Add(this.label7);
            this.tpGeneralidades.Controls.Add(this.label9);
            this.tpGeneralidades.Controls.Add(this.fotoEmpleado);
            this.tpGeneralidades.Controls.Add(this.btnCancelar);
            this.tpGeneralidades.Controls.Add(this.btnGuardar);
            this.tpGeneralidades.Controls.Add(this.txtNotas);
            this.tpGeneralidades.Controls.Add(this.label12);
            this.tpGeneralidades.Controls.Add(this.dtpFecha);
            this.tpGeneralidades.Controls.Add(this.txtDireccion);
            this.tpGeneralidades.Controls.Add(this.ComboMunicipios);
            this.tpGeneralidades.Controls.Add(this.comboDepartamentos);
            this.tpGeneralidades.Controls.Add(this.txtNIT);
            this.tpGeneralidades.Controls.Add(this.label8);
            this.tpGeneralidades.Controls.Add(this.txtCorreo);
            this.tpGeneralidades.Controls.Add(this.label6);
            this.tpGeneralidades.Controls.Add(this.txtTelefono);
            this.tpGeneralidades.Controls.Add(this.label4);
            this.tpGeneralidades.Controls.Add(this.txtDUI);
            this.tpGeneralidades.Controls.Add(this.txtApellidos);
            this.tpGeneralidades.Controls.Add(this.label3);
            this.tpGeneralidades.Controls.Add(this.txtNombres);
            this.tpGeneralidades.Controls.Add(this.txtID);
            this.tpGeneralidades.Controls.Add(this.label1);
            this.tpGeneralidades.Location = new System.Drawing.Point(4, 22);
            this.tpGeneralidades.Name = "tpGeneralidades";
            this.tpGeneralidades.Padding = new System.Windows.Forms.Padding(3);
            this.tpGeneralidades.Size = new System.Drawing.Size(659, 536);
            this.tpGeneralidades.TabIndex = 0;
            this.tpGeneralidades.Text = "Generalidades";
            this.tpGeneralidades.UseVisualStyleBackColor = true;
            this.tpGeneralidades.Click += new System.EventHandler(this.TpGeneralidades_Click);
            // 
            // txtTitulo
            // 
            this.txtTitulo.Location = new System.Drawing.Point(99, 412);
            this.txtTitulo.Name = "txtTitulo";
            this.txtTitulo.Size = new System.Drawing.Size(187, 20);
            this.txtTitulo.TabIndex = 86;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label16.Location = new System.Drawing.Point(422, 435);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 16);
            this.label16.TabIndex = 85;
            this.label16.Text = "( Año / Mes / dia )";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(21, 451);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 16);
            this.label13.TabIndex = 84;
            this.label13.Text = "Notas:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(18, 412);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 16);
            this.label10.TabIndex = 83;
            this.label10.Text = "Titulo:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(18, 371);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 16);
            this.label11.TabIndex = 82;
            this.label11.Text = "Dirección:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1, 328);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 16);
            this.label2.TabIndex = 81;
            this.label2.Text = "Departamento:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(21, 283);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 16);
            this.label5.TabIndex = 80;
            this.label5.Text = "Telefono:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(24, 239);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 16);
            this.label7.TabIndex = 79;
            this.label7.Text = "DUI:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(24, 195);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 16);
            this.label9.TabIndex = 78;
            this.label9.Text = "Nombres:";
            // 
            // fotoEmpleado
            // 
            this.fotoEmpleado.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.fotoEmpleado.ErrorImage = ((System.Drawing.Image)(resources.GetObject("fotoEmpleado.ErrorImage")));
            this.fotoEmpleado.Location = new System.Drawing.Point(142, 20);
            this.fotoEmpleado.Name = "fotoEmpleado";
            this.fotoEmpleado.Size = new System.Drawing.Size(134, 150);
            this.fotoEmpleado.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fotoEmpleado.TabIndex = 77;
            this.fotoEmpleado.TabStop = false;
            this.fotoEmpleado.Click += new System.EventHandler(this.fotoEmpleado_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.LightGray;
            this.btnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.Red;
            this.btnCancelar.Location = new System.Drawing.Point(397, 482);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(92, 34);
            this.btnCancelar.TabIndex = 76;
            this.btnCancelar.Text = "CANCELAR";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click_1);
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.Color.LightGray;
            this.btnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.ForeColor = System.Drawing.Color.Navy;
            this.btnGuardar.Location = new System.Drawing.Point(530, 482);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(102, 34);
            this.btnGuardar.TabIndex = 75;
            this.btnGuardar.Text = "GUARDAR";
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // txtNotas
            // 
            this.txtNotas.Location = new System.Drawing.Point(93, 450);
            this.txtNotas.Multiline = true;
            this.txtNotas.Name = "txtNotas";
            this.txtNotas.Size = new System.Drawing.Size(252, 66);
            this.txtNotas.TabIndex = 74;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(304, 412);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(112, 16);
            this.label12.TabIndex = 73;
            this.label12.Text = "Fech.Nacimiento:";
            // 
            // dtpFecha
            // 
            this.dtpFecha.CustomFormat = "yyyy-MM-dd";
            this.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFecha.Location = new System.Drawing.Point(422, 412);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(200, 20);
            this.dtpFecha.TabIndex = 72;
            // 
            // txtDireccion
            // 
            this.txtDireccion.Location = new System.Drawing.Point(93, 370);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.Size = new System.Drawing.Size(519, 20);
            this.txtDireccion.TabIndex = 70;
            // 
            // ComboMunicipios
            // 
            this.ComboMunicipios.FormattingEnabled = true;
            this.ComboMunicipios.Location = new System.Drawing.Point(422, 325);
            this.ComboMunicipios.Name = "ComboMunicipios";
            this.ComboMunicipios.Size = new System.Drawing.Size(190, 21);
            this.ComboMunicipios.TabIndex = 69;
            // 
            // comboDepartamentos
            // 
            this.comboDepartamentos.FormattingEnabled = true;
            this.comboDepartamentos.Location = new System.Drawing.Point(99, 327);
            this.comboDepartamentos.Name = "comboDepartamentos";
            this.comboDepartamentos.Size = new System.Drawing.Size(190, 21);
            this.comboDepartamentos.TabIndex = 68;
            // 
            // txtNIT
            // 
            this.txtNIT.Location = new System.Drawing.Point(425, 236);
            this.txtNIT.Name = "txtNIT";
            this.txtNIT.Size = new System.Drawing.Size(187, 20);
            this.txtNIT.TabIndex = 67;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(325, 328);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 16);
            this.label8.TabIndex = 66;
            this.label8.Text = "Municipio:";
            // 
            // txtCorreo
            // 
            this.txtCorreo.Location = new System.Drawing.Point(422, 283);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(190, 20);
            this.txtCorreo.TabIndex = 65;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(328, 283);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 16);
            this.label6.TabIndex = 64;
            this.label6.Text = "Correo:";
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(99, 282);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(187, 20);
            this.txtTelefono.TabIndex = 63;
            this.txtTelefono.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTelefono_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(331, 239);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 16);
            this.label4.TabIndex = 62;
            this.label4.Text = "NIT:";
            // 
            // txtDUI
            // 
            this.txtDUI.Location = new System.Drawing.Point(99, 238);
            this.txtDUI.Name = "txtDUI";
            this.txtDUI.Size = new System.Drawing.Size(187, 20);
            this.txtDUI.TabIndex = 61;
            // 
            // txtApellidos
            // 
            this.txtApellidos.Location = new System.Drawing.Point(425, 192);
            this.txtApellidos.Name = "txtApellidos";
            this.txtApellidos.Size = new System.Drawing.Size(207, 20);
            this.txtApellidos.TabIndex = 60;
            this.txtApellidos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtApellidos_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(331, 195);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 16);
            this.label3.TabIndex = 59;
            this.label3.Text = "Apellidos:";
            // 
            // txtNombres
            // 
            this.txtNombres.Location = new System.Drawing.Point(99, 194);
            this.txtNombres.Name = "txtNombres";
            this.txtNombres.Size = new System.Drawing.Size(207, 20);
            this.txtNombres.TabIndex = 58;
            this.txtNombres.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombres_KeyPress);
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(397, 72);
            this.txtID.Name = "txtID";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(100, 20);
            this.txtID.TabIndex = 57;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(303, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 16);
            this.label1.TabIndex = 56;
            this.label1.Text = "ID empleado:";
            // 
            // tpAdministrativos
            // 
            this.tpAdministrativos.Controls.Add(this.btnGuardarAdmin);
            this.tpAdministrativos.Controls.Add(this.label19);
            this.tpAdministrativos.Controls.Add(this.label18);
            this.tpAdministrativos.Controls.Add(this.txtDescripcionCargo);
            this.tpAdministrativos.Controls.Add(this.label17);
            this.tpAdministrativos.Controls.Add(this.txtSueldo);
            this.tpAdministrativos.Controls.Add(this.comboCargo);
            this.tpAdministrativos.Controls.Add(this.label14);
            this.tpAdministrativos.Controls.Add(this.comboSucursal);
            this.tpAdministrativos.Controls.Add(this.label15);
            this.tpAdministrativos.Location = new System.Drawing.Point(4, 22);
            this.tpAdministrativos.Name = "tpAdministrativos";
            this.tpAdministrativos.Padding = new System.Windows.Forms.Padding(3);
            this.tpAdministrativos.Size = new System.Drawing.Size(659, 536);
            this.tpAdministrativos.TabIndex = 1;
            this.tpAdministrativos.Text = "Administrativos";
            this.tpAdministrativos.UseVisualStyleBackColor = true;
            // 
            // btnGuardarAdmin
            // 
            this.btnGuardarAdmin.BackColor = System.Drawing.Color.SeaShell;
            this.btnGuardarAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarAdmin.ForeColor = System.Drawing.Color.DarkBlue;
            this.btnGuardarAdmin.Location = new System.Drawing.Point(472, 465);
            this.btnGuardarAdmin.Name = "btnGuardarAdmin";
            this.btnGuardarAdmin.Size = new System.Drawing.Size(156, 43);
            this.btnGuardarAdmin.TabIndex = 93;
            this.btnGuardarAdmin.Text = "Guardar cambios";
            this.btnGuardarAdmin.UseVisualStyleBackColor = false;
            this.btnGuardarAdmin.Click += new System.EventHandler(this.btnGuardarAdmin_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.ForestGreen;
            this.label19.Location = new System.Drawing.Point(18, 12);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(112, 18);
            this.label19.TabIndex = 91;
            this.label19.Text = "Datos Actuales:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(12, 295);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(140, 16);
            this.label18.TabIndex = 90;
            this.label18.Text = "Descripción de cargo:";
            // 
            // txtDescripcionCargo
            // 
            this.txtDescripcionCargo.Location = new System.Drawing.Point(15, 314);
            this.txtDescripcionCargo.Multiline = true;
            this.txtDescripcionCargo.Name = "txtDescripcionCargo";
            this.txtDescripcionCargo.Size = new System.Drawing.Size(388, 96);
            this.txtDescripcionCargo.TabIndex = 89;
            this.txtDescripcionCargo.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(12, 215);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 16);
            this.label17.TabIndex = 88;
            this.label17.Text = "Sueldo:  $";
            // 
            // txtSueldo
            // 
            this.txtSueldo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSueldo.Location = new System.Drawing.Point(81, 209);
            this.txtSueldo.Name = "txtSueldo";
            this.txtSueldo.Size = new System.Drawing.Size(262, 26);
            this.txtSueldo.TabIndex = 87;
            this.txtSueldo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSueldo_KeyPress);
            // 
            // comboCargo
            // 
            this.comboCargo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboCargo.FormattingEnabled = true;
            this.comboCargo.Location = new System.Drawing.Point(81, 43);
            this.comboCargo.Name = "comboCargo";
            this.comboCargo.Size = new System.Drawing.Size(262, 28);
            this.comboCargo.TabIndex = 86;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label14.Location = new System.Drawing.Point(12, 48);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 16);
            this.label14.TabIndex = 85;
            this.label14.Text = "Cargo:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // comboSucursal
            // 
            this.comboSucursal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboSucursal.FormattingEnabled = true;
            this.comboSucursal.Location = new System.Drawing.Point(81, 117);
            this.comboSucursal.Name = "comboSucursal";
            this.comboSucursal.Size = new System.Drawing.Size(262, 28);
            this.comboSucursal.TabIndex = 84;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(12, 123);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 16);
            this.label15.TabIndex = 82;
            this.label15.Text = "Sucursal:";
            // 
            // EdicionEmpleados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(662, 558);
            this.Controls.Add(this.tabControlEEmpleados);
            this.MaximizeBox = false;
            this.Name = "EdicionEmpleados";
            this.Text = "Edicion de empleados";
            this.Load += new System.EventHandler(this.EdicionEmpleados_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Notificador)).EndInit();
            this.tabControlEEmpleados.ResumeLayout(false);
            this.tpGeneralidades.ResumeLayout(false);
            this.tpGeneralidades.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fotoEmpleado)).EndInit();
            this.tpAdministrativos.ResumeLayout(false);
            this.tpAdministrativos.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ErrorProvider Notificador;
        private System.Windows.Forms.TabPage tpGeneralidades;
        public System.Windows.Forms.TabControl tabControlEEmpleados;
        public System.Windows.Forms.PictureBox fotoEmpleado;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        public System.Windows.Forms.TextBox txtNotas;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.DateTimePicker dtpFecha;
        public System.Windows.Forms.TextBox txtDireccion;
        public System.Windows.Forms.ComboBox ComboMunicipios;
        public System.Windows.Forms.ComboBox comboDepartamentos;
        public System.Windows.Forms.TextBox txtNIT;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtCorreo;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtDUI;
        public System.Windows.Forms.TextBox txtApellidos;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtNombres;
        public System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.ComboBox comboSucursal;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.ComboBox comboCargo;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.TabPage tpAdministrativos;
        public System.Windows.Forms.TextBox txtDescripcionCargo;
        public System.Windows.Forms.Button btnGuardarAdmin;
        private System.Windows.Forms.Label label19;
        public System.Windows.Forms.TextBox txtTitulo;
        public System.Windows.Forms.TextBox txtSueldo;
    }
}