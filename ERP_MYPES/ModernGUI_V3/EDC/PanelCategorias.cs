﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.EDC
{
    public partial class PanelCategorias : Form
    {
        BindingSource _sourceDate = new BindingSource();

        public PanelCategorias()
        {
            InitializeComponent();
        }

        public void CargarDatos()
        {
            try
            {
                _sourceDate.DataSource = CacheManager.SystemCache.TODASLASCATEGORIAS();
                dtgvCategorias.AutoGenerateColumns = false;
                dtgvCategorias.DataSource = _sourceDate;
            }
            catch
            {
            }
        }

        private void MostrarEnTexts()
        {
            txtID.Text = this.dtgvCategorias["IDCategoria", dtgvCategorias.CurrentRow.Index].Value.ToString();
            txtCategoria.Text = this.dtgvCategorias["NombreCategroria", dtgvCategorias.CurrentRow.Index].Value.ToString();
            txtDescripcion.Text = this.dtgvCategorias["Descripcion", dtgvCategorias.CurrentRow.Index].Value.ToString();
        }

        private void LimpiarTextos()
        {
            this.txtCategoria.Text = "";
            this.txtDescripcion.Text = "";
            this.txtID.Text = "";
        }

        private bool Valido()
        {
            bool re = true;
            if (this.txtCategoria.Text.Equals(""))
            {
                re = false;
            }
            return re;
        }
        
        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dtgvCategorias_KeyDown(object sender, KeyEventArgs e)
        {

            try
            {
                if (this.dtgvCategorias.CurrentRow.Index != -1)
                {
                    this.MostrarEnTexts();
                }
            }
            catch (Exception)
            {
            }

        }

        private void dtgvCategorias_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if (this.dtgvCategorias.CurrentRow.Index != -1)
                {
                    this.MostrarEnTexts();
                }
            }
            catch (Exception)
            {
            }

        }
        private void Procesar()
        {
            if (txtID.Text.Length == 0)
            {
                //Insertar 
                if (CLS.Categoria.Guardar(this.txtCategoria.Text, this.txtDescripcion.Text))
                {
                    MessageBox.Show("Operación Realizada con éxito");
                }
                else
                {
                    MessageBox.Show("No se realizó la operación");
                }
            }
            else
            {
                // Actualizar
                if (CLS.Categoria.Actualizar(this.txtCategoria.Text, this.txtID.Text, this.txtDescripcion.Text))
                {
                    MessageBox.Show("Operación Realizada con éxito");
                }
                else
                {
                    MessageBox.Show("No se realizó la operación");
                }
            }
            this.CargarDatos();
        }

        private void dtgvCategorias_MouseClick(object sender, MouseEventArgs e)
        {

            try
            {
                if (this.dtgvCategorias.CurrentRow.Index != -1)
                {
                    this.MostrarEnTexts();
                }
            }
            catch (Exception)
            {
            }

        }

        private void PanelCategorias_Load(object sender, EventArgs e)
        {
            this.CargarDatos();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
                try { 
                    if (this.dtgvCategorias.CurrentRow.Index != -1)
                    {
                        if (this.txtID.Text.Equals(""))
                        {
                            MessageBox.Show("Debe elegir un registro para la operación");
                        }
                        else
                        {
                            if (CLS.Categoria.Eliminar(this.txtID.Text))
                            {
                                MessageBox.Show("Operación realizada con éxito");
                                this.CargarDatos();
                            }
                            else
                            {
                                MessageBox.Show("No se realizó la operación");
                            }

                        }
                        this.LimpiarTextos();
                    }
            }catch{}
        }

        private void btnGuardarE_Click(object sender, EventArgs e)
        {
            if (this.dtgvCategorias.CurrentRow.Index != -1)
            {
                if (txtID.Text.Equals(""))
                {
                    MessageBox.Show("Seleccione el elemento a actualizar,e intentelo de nuevo.","FALTA SELECCION");
                }
                else
                {
                    if (Valido())
                    {
                        Procesar();
                        this.LimpiarTextos();
                    }
                }
            }

        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            this.LimpiarTextos();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {

                if (this.dtgvCategorias.CurrentRow.Index != -1)
                {
                    if (txtID.Text.Equals(""))
                    {
                        if (Valido())
                        {
                            Procesar();
                            LimpiarTextos();
                        }
                    }
                    else
                    {
                        MessageBox.Show("El campo ID debe estar limpio.\nDé click en el botón limpiar, ingrese los nuevos datos e intentelo de nuevo.");
                    }
                }
            }
            catch { }
        }
        /*  */

    }
}
