﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.EDC
{
    public partial class EdicionCliente : Form
    {
        public EdicionCliente()
        {
            InitializeComponent();
            CargarDepartamentos();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void CargarDepartamentos()
        {
            try
            {
                this.comboDep.DataSource = CacheManager.SystemCache.TODOSDEPARTAMENTOS();
                this.comboDep.ValueMember = "idDepartamento";
                this.comboDep.DisplayMember = "Departamento";
                this.comboDep.SelectedIndex = 0;
                CargarMunicipios();
            }
            catch { }
        }

        private void CargarMunicipios()
        {
            try
            {
                this.comboMunicipio.DataSource = CacheManager.SystemCache.TODOSMUNICIPIOS(this.comboDep.SelectedValue.ToString());
                this.comboMunicipio.ValueMember = "idMunicipio";
                this.comboMunicipio.DisplayMember = "Municipio";
                this.comboMunicipio.SelectedIndex = 0;
            }
            catch { }
        }

        private void TxtIDUsuario_TextChanged(object sender, EventArgs e)
        {

        }

        private void LblUsuario_Click(object sender, EventArgs e)
        {

        }

        private void EdicionCliente_Load(object sender, EventArgs e)
        {
            //CargarDepartamentos();
        }

        private void ComboDep_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarMunicipios();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            if (Verificacion())
            {
                Procesar();
                Close();
            }
        }

        public void Procesar()
        {
            CLS.Clientes oCliente = new CLS.Clientes();
            oCliente.IDCliente1 = txtidCliente.Text;
            oCliente.NombreContacto1 = txtUser.Text;
            oCliente.TelefoContacto1 = txtTelContacto.Text;
            oCliente.Direccion1 = txtDireccion.Text;
            oCliente.IdMunicipio = comboMunicipio.SelectedValue.ToString();
                       

            if (txtidCliente.TextLength == 0)
            {
                //Estoy insertando un nuevo regitro
                if (oCliente.GuardarCliente())
                {
                    MessageBox.Show("Registro guardado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser guardado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                int tipo = 0;
                if (checkactualizar.Checked)
                {
                    tipo = 1;
                }
                if (oCliente.ActualizarCliente(tipo))
                {
                    MessageBox.Show("Registro actualizado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser actualizado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private Boolean Verificacion()
        {
            Boolean Verificado = true;
            Notificador.Clear();
            if (txtUser.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtUser, "Este campo no puede quedar vacío");
            }

            if (txtTelContacto.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtTelContacto, "Este campo no puede quedar vacío");
            }

            if (txtDireccion.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtDireccion, "Este campo no puede quedar vacío");
            }


            return Verificado;
        }

        private void Checkactualizar_CheckedChanged(object sender, EventArgs e)
        {
            if (checkactualizar.Checked)
            {
                comboDep.Enabled = true;
                comboMunicipio.Enabled = true;
            }
            else
            {
                comboDep.Enabled = false;
                comboMunicipio.Enabled = false;
            }
        }
    }
}
