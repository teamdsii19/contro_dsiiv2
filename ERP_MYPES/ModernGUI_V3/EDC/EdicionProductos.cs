﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.EDC
{
    public partial class EdicionProductos : Form
    {
        ArrayList ACategorias;
        ArrayList AProveedores;
        private static String categoria;
        private static String proveedor;
        //ArrayList ASucursal;
        private static String idSucursal;

        public static string IdSucursal { get => idSucursal; set => idSucursal = value; }
        public static string Categoria { get => categoria; set => categoria = value; }
        public static string Proveedor { get => proveedor; set => proveedor = value; }

        public EdicionProductos()
        {
            InitializeComponent();
        }

        public void Procesar()
        {
            CLS.Producto oProducto = new CLS.Producto();
            oProducto.IDProducto = txtID.Text;
            oProducto.NombreProducto = txtNombre.Text;
            oProducto.Alias = txtAlias.Text;
            oProducto.IDCategoria = ACategorias[comboCategoria.SelectedIndex].ToString();
            oProducto.IDProveedor = AProveedores[comboProveedor.SelectedIndex].ToString();
            oProducto.IdSucursal = idSucursal;
            oProducto.Costo = txtCosto.Text;
            oProducto.PrecioUnitario = txtPrecio.Text;
            oProducto.PrecioSucursal = txtPrecio.Text;
            oProducto.Minimo = txtMinimo.Text;
            oProducto.MedidaPorUnidad = txtPresentacion.Text;
            try
            {
                oProducto.Imagen = CacheManager.Operaciones.PrepararImagenParaGuardar(this.fotoProductos);
            }
            catch
            {

            }
            oProducto.UnidadesEnStock = txtExistencias.Text;
            oProducto.CodigoBarras = txtCodigoBarras.Text;

            if (txtID.TextLength == 0)
            {
                //Estoy insertando un nuevo regitro
                if (oProducto.GuardarProducto())
                {
                    MessageBox.Show("Registro guardado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //                    oProducto.guardarFoto(this.fotoProductos, oEmpleado.TraerIDRecienIngresado());
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser guardado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                if (oProducto.ActualizarProducto(oProducto.IDProducto))
                {
                    MessageBox.Show("Registro actualizado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser actualizado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private Boolean Verificacion()
        {
            Boolean Verificado = true;
            Notificador.Clear();
            if (txtNombre.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtNombre, "Este campo no puede quedar vacío");
            }

            if (txtExistencias.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtExistencias, "Este campo no puede quedar vacío");
            }

            if (comboCategoria.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(comboCategoria, "Debe Seleccionar una categorìa");
            }

            if (comboProveedor.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(comboProveedor, "Este campo no puede quedar vacío");
            }

            if (txtPrecio.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtPrecio, "Este campo no puede quedar vacío");
            }

            if (txtCosto.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtCosto, "Este campo no puede quedar vacío");
            }

            if (txtCosto.Text.Equals("") || txtPrecio.Text.Equals(""))
            {
                Verificado = false;
                MessageBox.Show("Verifique el precio de venta y costo del producto", "INCONGRUENCIA DE PRECIOS");
            }
            else
            {
                if (Convert.ToDouble(txtCosto.Text) <= 0 || Convert.ToDouble(txtCosto.Text) >= Convert.ToDouble(txtPrecio.Text) || Convert.ToDouble(txtPrecio.Text) <= 0)
                {
                    Verificado = false;
                    MessageBox.Show("Verifique el precio de venta y costo del producto", "INCONGRUENCIA DE PRECIOS");
                }
            }
            return Verificado;
        }

        private void fotoProductos_Click(object sender, EventArgs e)
        {
            CacheManager.Operaciones.CargarImagenSO(this.fotoProductos);
        }

        private void txtExistencias_KeyPress(object sender, KeyPressEventArgs e)
        {
            CacheManager.Operaciones.NumerosDecimales(e);
        }

        private void EdicionProductos_Load(object sender, EventArgs e)
        {
            this.ACategorias = CacheManager.Operaciones.LlenarComboBox(this.comboCategoria, "idCategoria", "NombreCategroria", "Categorias");
            this.AProveedores = CacheManager.Operaciones.LlenarComboBox(this.comboProveedor, "idProveedor", "NombreProveedor", "proveedores");
            try
            {
                this.comboCategoria.Text = Categoria;
                this.comboProveedor.Text = Proveedor;
            }
            catch
            {
                this.comboCategoria.SelectedIndex = this.comboCategoria.Items.Count - 1;
                this.comboProveedor.SelectedIndex = this.comboProveedor.Items.Count - 1;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (Verificacion())
            {
                Procesar();
                Close();
            }
        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void LblPro_Click(object sender, EventArgs e)
        {

        }
    }
}
