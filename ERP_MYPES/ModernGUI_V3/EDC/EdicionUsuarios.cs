﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModernGUI_V3.CLS;

namespace ModernGUI_V3.EDC
{
    public partial class EdicionUsuarios : Form
    {
        public EdicionUsuarios()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
        }
        BindingSource _Empleados = new BindingSource();
        ArrayList arrayList = new System.Collections.ArrayList();
        CLS.Usuarios oUsuarioEdit = new CLS.Usuarios();

        internal Usuarios OUsuarioEdit { get => oUsuarioEdit; set => oUsuarioEdit = value; }

        public void CargarDatos()
        {
            try
            {
                _Empleados.DataSource = CacheManager.SystemCache.TODOSLOSEMPLEADOS();
                FiltrarLocalmente();
            }
            catch
            {
            }
        }

        public void FiltrarLocalmente()
        {
            try
            {
                if (txtFiltro.TextLength > 0)
                {
                    _Empleados.Filter = "Nombres LIKE '%" + txtFiltro.Text + "%' OR Apellidos like '%" + txtFiltro.Text + "%'";
                }
                else
                {
                    _Empleados.RemoveFilter();
                }
                dtgvEmpleados.AutoGenerateColumns = false;
                dtgvEmpleados.DataSource = _Empleados;
                if (this.Text.Equals("Editar Usuario"))
                {
                    this.dtgvEmpleados.Visible = false;
                    this.txtFiltro.Visible = false;
                    this.lblLupa.Visible = false;
                    this.label2.Visible = false;
                    try
                    {
                        pbUser.Image = Image.FromStream(CacheManager.Operaciones.PrepararImagenParaMostrar((byte[])this.dtgvEmpleados["foto", dtgvEmpleados.CurrentRow.Index].Value));
                    }
                    catch { }
                }
                else {
                    pbUser.Visible = false;
                }
            }
            catch
            {

            }
        }
        private void txtIDUsuario_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtUser_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        private void EdicionUsuarios_Load(object sender, EventArgs e)
        {
            CargarDatos();
            llenarCombo();

        }

        private void llenarCombo()
        {
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            String query = "SELECT idRol, Rol from roles";
            DataTable dt = oOperacion.Consultar(query);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                cmbRoles.Items.Add(dt.Rows[i]["Rol"]);
                arrayList.Add(dt.Rows[i]["idRol"]);
            }

        }

        private void lblEstado_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEstado.Checked)
            {
                cmbEstado.Enabled = true;
            }
            else {
                cmbEstado.Enabled = false;
            }

        }

        private void checkCambiarRol_CheckedChanged(object sender, EventArgs e)
        {
            if (checkCambiarRol.Checked)
            {
                cmbRoles.Enabled = true;
            }
            else
            {
                cmbRoles.Enabled = false;
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            
            if (txtIDUsuario.TextLength == 0)
            {
                CLS.Usuarios oUser = new CLS.Usuarios();
                //Estoy insertando un nuevo regitro
                if (VerificacionInsertar())
                {
                    oUser.Usuario = txtUser.Text;
                    oUser.Credencial1 = txtCredencial.Text;
                    oUser.IdRol = arrayList[this.cmbRoles.SelectedIndex].ToString();
                    oUser.IdEmpleado = dtgvEmpleados.CurrentRow.Cells["IDEmpleado"].Value.ToString();
                    oUser.Estado1 = cmbEstado.SelectedItem.ToString();
                    if (oUser.GuardarUsuario())
                    {
                        MessageBox.Show("Registro guardado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("El registro no pudo ser guardado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else {
                    MessageBox.Show("Verifique los Datos", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                }
            }
            else
            {
                if (VerificacionEdit()){
                    //verificamoos si los cambios seran modificados
                    //si el tipo es 1 se actualizara la contraseña
                    int tipo = 0;
                    if (checkCambiarRol.Checked)
                    {
                        oUsuarioEdit.IdRol = arrayList[this.cmbRoles.SelectedIndex].ToString();
                    }
                    if (checkEstado.Checked)
                    {
                        oUsuarioEdit.Estado1 = cmbEstado.SelectedItem.ToString();
                    }
                    oUsuarioEdit.Usuario = txtUser.Text;
                    if (checkpassword.Checked)
                    {
                        tipo = 1;
                        oUsuarioEdit.Credencial1 = txtCredencial.Text;
                    }
                    if (oUsuarioEdit.ActualizarUsuario(tipo))
                        {
                            MessageBox.Show("Registro actualizado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            Close();
                        }
                        else
                        {
                            MessageBox.Show("El registro no pudo ser actualizado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
            }
        }

        private Boolean VerificacionInsertar()
        {
            Boolean Verificado = true;
            Notificador.Clear();
            if (txtUser.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtUser, "Este campo no puede quedar vacío");
            }

            if (txtCredencial.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtCredencial, "Este campo no puede quedar vacío");
            }

            if (checkCambiarRol.Checked == false || checkEstado.Checked == false)
            {
                Verificado = false;
            }

            return Verificado;
        }

        private Boolean VerificacionEdit()
        {
            Boolean Verificado = true;
            Notificador.Clear();
            if (txtUser.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtUser, "Este campo no puede quedar vacío");
            }

            if (txtCredencial.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtCredencial, "Este campo no puede quedar vacío");
            }


            return Verificado;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
