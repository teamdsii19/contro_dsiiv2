﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModernGUI_V3.CLS;

namespace ModernGUI_V3.EDC
{
    public partial class BuscarProducto : Form
    {
        BindingSource _Productos = new BindingSource();
        CLS.Producto oProduct = new CLS.Producto();
        Boolean Seleccionado = false;

        internal Producto OProduct { get => oProduct; set => oProduct = value; }
        public bool Seleccionado1 { get => Seleccionado; set => Seleccionado = value; }

        public BuscarProducto()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void DtgvProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        public void CargarDatos()
        {
            try
            {

                    _Productos.DataSource = CacheManager.SystemCache.TODOSLOSPRODUCTOS();
                    FiltrarLocalmente();

            }
            catch
            {
            }
        }

        public void FiltrarLocalmente()
        {
            try
            {
                if (txtFiltro.TextLength > 0)
                {
                    _Productos.Filter = "NombreProducto LIKE '%" + txtFiltro.Text + "%' OR Alias LIKE '% " + txtFiltro.Text + "%'";
                }
                else
                {
                    _Productos.RemoveFilter();
                }
                dtgvProductos.AutoGenerateColumns = false;
                dtgvProductos.DataSource = _Productos;
            }
            catch
            {
            }
        }

        private void BuscarProducto_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void BtnCategorias_Click(object sender, EventArgs e)
        {
            Seleccionado = true;
            OProduct.IDProducto= this.dtgvProductos.CurrentRow.Cells["idProducto"].Value.ToString();
            OProduct.NombreProducto  = this.dtgvProductos.CurrentRow.Cells["NombreProducto"].Value.ToString();
            OProduct.UnidadesEnStock = this.dtgvProductos.CurrentRow.Cells["UnidadesEnStock"].Value.ToString();
            OProduct.PrecioUnitario= this.dtgvProductos.CurrentRow.Cells["PrecioUnitario"].Value.ToString();
            Close();
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }
    }
}
