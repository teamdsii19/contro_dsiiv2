﻿using CacheManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModernGUI_V3.CLS;

namespace ModernGUI_V3.EDC
{
    public partial class PrepararPedido : Form
    {
        Boolean _Actualizando = false;

        BindingSource _Productos = new BindingSource();

        CLS.Pedido _NPedido = new CLS.Pedido();

        public bool Actualizando { get => _Actualizando; set => _Actualizando = value; }
        internal Pedido NPedido { get => _NPedido; set => _NPedido = value; }

        public PrepararPedido()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
            dtpFecPedido.Value = DateTime.Now;
        }

        private void PrepararPedido_Load(object sender, EventArgs e)
        {
            if (Actualizando)
            {
                CargarDatos();
                txtIDPedido.Text = NPedido.IDPedido1;
                txtNUMPedido.Text = NPedido.NumPedido1;
                txtIDCliente.Text = NPedido.IDCliente1;
                dtpFechaEntrega.Text = NPedido.FechaEntrega1;
                dtpFecPedido.Text = NPedido.FechaPedido1;


            }
        }


        public void CargarDatos()
        {
            try
            {

                _Productos.DataSource = CacheManager.SystemCache.ProductosPorPedido(_NPedido.IDPedido1);
                dtgvProductos.AutoGenerateColumns = false;
                dtgvProductos.DataSource = _Productos;

            }
            catch
            {
            }
        }
        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void btnBuscarProducto_Click(object sender, EventArgs e)
        {
            BuscarProducto ven = new BuscarProducto();
            ven.ShowDialog();
            if (ven.Seleccionado1 == true)
            {
                txtidProducto.Text = ven.OProduct.IDProducto;
                txtNombreProducto.Text = ven.OProduct.NombreProducto;
                lblPrecio.Text = ven.OProduct.PrecioUnitario;
                lblUnidadesStk.Text = ven.OProduct.UnidadesEnStock;
                CalcularSubTotal();
            }
        }

        private void btnBuscarCliente_Click(object sender, EventArgs e)
        {
            BuscarCliente ven = new BuscarCliente();
            ven.ShowDialog();
            txtIDCliente.Text = ven.OCliente.IDCliente1;
            txtNCliente.Text = ven.OCliente.NombreContacto1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            EdicionCliente VR = new EdicionCliente();
            VR.ShowDialog();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            if (VerificacionFinal() && dtgvProductos.RowCount > 0)
            {

                CLS.Pedido nOPedido = new CLS.Pedido();
                nOPedido.IDCliente1 = txtIDCliente.Text;
                nOPedido.IDSucursal1 = "1";
                nOPedido.FechaEntrega1 = dtpFechaEntrega.Text;
                nOPedido.FechaPedido1 = dtpFecPedido.Text;
                nOPedido.EstadoPedido1 = "PENDIENTE";
                nOPedido.NumPedido1 = txtNUMPedido.Text;
                if (Actualizando == true)
                {
                    nOPedido.IDPedido1 = txtIDPedido.Text;
                    nOPedido.ActualizarPedido();
                    Close();
                }
                else {
                    if (nOPedido.Guardar())
                    {
                        MessageBox.Show("Registro guardado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        if (VerificarStock()) {
                            MessageBox.Show("No se agregagaron los Detalles, un producto no es valido", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else if (dtgvProductos.RowCount > 0) {
                            for (int i = 0; i < dtgvProductos.RowCount; i++) {
                                nOPedido.DetallePedido(
                                    SystemCache.IDPedidoNumero(txtNUMPedido.Text),
                                    dtgvProductos.Rows[i].Cells["idProducto"].Value.ToString(),
                                    dtgvProductos.Rows[i].Cells["cantidad"].Value.ToString()
                                    );
                            }
                            Close();
                        }


                        Close();
                    }
                    else
                    {
                        MessageBox.Show("error");
                    }
                }
            }else  {
                MessageBox.Show("NO SE PUEDE GUARDAR UN PEDIDO SIN PRODUCTOS");


            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (VerificacionAddProduct())
            {
                if (Convert.ToInt32(lblUnidadesStk.Text) < Convert.ToInt32(numericant.Value.ToString()))
                {
                    MessageBox.Show("Stock insuficiente");
                }
                else
                {
                    if (productoExist())
                    {
                        MessageBox.Show("Producto ya Agregado");
                    }
                    else
                    {
                        if (Actualizando==true)
                        {
                            NPedido.DetallePedido(
                                    NPedido.IDPedido1,
                                    txtidProducto.Text,
                                    numericant.Value.ToString()
                                    );
                            CargarDatos();

                        } else {
                            dtgvProductos.Rows.Add(
                                txtidProducto.Text,
                                txtNombreProducto.Text,
                                lblPrecio.Text,
                                numericant.Value.ToString(),
                                lblSubTotal.Text,
                                "NULL"
                                );
                        }
                    }
                }
                txtidProducto.Text = "";
                txtNombreProducto.Text = "";
                lblUnidadesStk.Text = "0";
                lblPrecio.Text = "0";
                lblSubTotal.Text = "0";
            }

        }

        private void CalcularSubTotal()
        {
            Double VA = Convert.ToDouble(numericant.Value.ToString()) * Convert.ToDouble(lblPrecio.Text);
            lblSubTotal.Text = VA + "";
        }

        private void numericant_ValueChanged(object sender, EventArgs e)
        {
            CalcularSubTotal();
        }

        private Boolean productoExist()
        {
            Boolean v = false;
            if (dtgvProductos.Rows.Count > 0)
            {
                for (int i = 0; i < dtgvProductos.RowCount; i++)
                {
                    if (this.dtgvProductos.Rows[i].Cells["idProducto"].Value.ToString() == txtidProducto.Text)
                    {
                        v = true;
                    }
                }
            }
            return v;
        }

        private void dtgvProductos_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void dtgvProductos_CellEnter(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dtgvProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private Boolean VerificacionFinal()
        {
            Boolean Verificado = true;
            Notificador.Clear();
            if (txtIDCliente.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtIDCliente, "Este campo no puede quedar vacío");
            }
            return Verificado;
        }

        private Boolean VerificacionAddProduct()
        {
            Boolean Verificado = true;
            Notificador.Clear();
            if (txtidProducto.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtidProducto, "Este campo no puede quedar vacío");
            }
            return Verificado;
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("Realmente desea ELIMINAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    if (Actualizando == true) {
                        if (dtgvProductos.RowCount ==1) {
                            MessageBox.Show("Error");

                        } else {
                            NPedido.EliminarDetalle(
                                dtgvProductos.CurrentRow.Cells["IDDestallesPedidos"].Value.ToString(),
                                dtgvProductos.CurrentRow.Cells["idProducto"].Value.ToString(),
                                dtgvProductos.CurrentRow.Cells["cantidad"].Value.ToString()
                                );
                            CargarDatos();
                        }
                    }
                    else {
                        dtgvProductos.Rows.Remove(dtgvProductos.CurrentRow);
                    }
                }
                catch
                {
                    MessageBox.Show("Error de eliminacion");
                }
            }
            txtidProducto.Text = "";
            txtNombreProducto.Text = "";
            lblUnidadesStk.Text = "0";
            lblPrecio.Text = "0";
            lblSubTotal.Text = "0";
        }

        private Boolean VerificarStock()
        {
            Boolean be = false;
            if (dtgvProductos.RowCount > 0)
            {
                for (int i = 0; i < dtgvProductos.RowCount; i++)
                {
                    MessageBox.Show(SystemCache.UnidadesStock(this.dtgvProductos.Rows[i].Cells["idProducto"].Value.ToString())+" "+ Convert.ToInt32(this.dtgvProductos.Rows[i].Cells["cantidad"].Value.ToString()));
                    if (Convert.ToInt32(SystemCache.UnidadesStock(this.dtgvProductos.Rows[i].Cells["idProducto"].Value.ToString()))
                         < Convert.ToInt32(this.dtgvProductos.Rows[i].Cells["cantidad"].Value.ToString())
                         )
                    {
                        be = true;
                    }
                }
            }
            return be;
        }
    }
}
