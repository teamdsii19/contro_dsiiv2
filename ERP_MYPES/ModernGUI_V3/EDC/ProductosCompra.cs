﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.EDC
{
    public partial class ProductosCompra : Form
    {
        BindingSource _Productos = new BindingSource();
        String iDcomp;

        public string IDcomp { get => iDcomp; set => iDcomp = value; }

        public ProductosCompra()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        public void CargarDatos()
        {
            try
            {

                _Productos.DataSource = CacheManager.SystemCache.Productos_Compra(iDcomp);
                dtgvProductos.AutoGenerateColumns = false;
                dtgvProductos.DataSource = _Productos;
                txtIDC.Text = iDcomp;

            }
            catch
            {
            }
        }


        private void Label2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void LblUsuario_Click(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            BuscarProducto ven = new BuscarProducto();
            ven.ShowDialog();
            txtidProducto.Text = ven.OProduct.IDProducto;
            txtNombreProducto.Text = ven.OProduct.NombreProducto;
        }

        private void ProductosCompra_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("SE AGREGARA LOS PRODUCTOS AL STOCK", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                CLS.Compras oCompra = new CLS.Compras();
                oCompra.IDCompra1 = Convert.ToInt32(txtIDC.Text);
                if (oCompra.DetalleCompra(txtidProducto.Text, numericant.Value.ToString(), txbSubtotal.Text)) {
                    MessageBox.Show("SE AGREGO CON EXITO");
                }
            }
            txbSubtotal.Text = "";
            txtidProducto.Text = "";
            txtNombreProducto.Text = "";
            numericant.Value = 1;
            CargarDatos();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea ELIMINAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                CLS.Compras oCompras1 = new CLS.Compras();
                if (oCompras1.ElimiarDetalleCompra(dtgvProductos.CurrentRow.Cells["idComprasDetalles"].Value.ToString(), dtgvProductos.CurrentRow.Cells["idProducto"].Value.ToString(), dtgvProductos.CurrentRow.Cells["cantidad"].Value.ToString()))
                {
                    MessageBox.Show("Registro eliminado exitosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDatos();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser eliminado exitosamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
    }
}
