﻿namespace ModernGUI_V3.EDC
{
    partial class BuscarTranportista
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dtgvClientes = new System.Windows.Forms.DataGridView();
            this.panelBusqueda = new System.Windows.Forms.Panel();
            this.btnCategorias = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtFiltro = new System.Windows.Forms.TextBox();
            this.IDTransporte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreTransporte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TituloContacto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TelefonoContacto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoTransporte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvClientes)).BeginInit();
            this.panelBusqueda.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtgvClientes
            // 
            this.dtgvClientes.AllowUserToAddRows = false;
            this.dtgvClientes.AllowUserToDeleteRows = false;
            this.dtgvClientes.AllowUserToResizeRows = false;
            this.dtgvClientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvClientes.BackgroundColor = System.Drawing.Color.Silver;
            this.dtgvClientes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtgvClientes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.MenuText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgvClientes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dtgvClientes.ColumnHeadersHeight = 30;
            this.dtgvClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dtgvClientes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDTransporte,
            this.NombreTransporte,
            this.TituloContacto,
            this.TelefonoContacto,
            this.TipoTransporte});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgvClientes.DefaultCellStyle = dataGridViewCellStyle5;
            this.dtgvClientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgvClientes.EnableHeadersVisualStyles = false;
            this.dtgvClientes.GridColor = System.Drawing.Color.Silver;
            this.dtgvClientes.Location = new System.Drawing.Point(0, 89);
            this.dtgvClientes.MultiSelect = false;
            this.dtgvClientes.Name = "dtgvClientes";
            this.dtgvClientes.ReadOnly = true;
            this.dtgvClientes.RowHeadersVisible = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(17)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dtgvClientes.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dtgvClientes.RowTemplate.Height = 28;
            this.dtgvClientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgvClientes.Size = new System.Drawing.Size(828, 278);
            this.dtgvClientes.TabIndex = 31;
            // 
            // panelBusqueda
            // 
            this.panelBusqueda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(17)))), ((int)(((byte)(38)))));
            this.panelBusqueda.Controls.Add(this.btnCategorias);
            this.panelBusqueda.Controls.Add(this.button1);
            this.panelBusqueda.Controls.Add(this.txtFiltro);
            this.panelBusqueda.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelBusqueda.Location = new System.Drawing.Point(0, 0);
            this.panelBusqueda.Name = "panelBusqueda";
            this.panelBusqueda.Size = new System.Drawing.Size(828, 89);
            this.panelBusqueda.TabIndex = 30;
            // 
            // btnCategorias
            // 
            this.btnCategorias.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnCategorias.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCategorias.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCategorias.ForeColor = System.Drawing.SystemColors.Control;
            this.btnCategorias.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCategorias.Location = new System.Drawing.Point(633, 32);
            this.btnCategorias.Name = "btnCategorias";
            this.btnCategorias.Size = new System.Drawing.Size(155, 38);
            this.btnCategorias.TabIndex = 11;
            this.btnCategorias.Text = "Seleccionar";
            this.btnCategorias.UseVisualStyleBackColor = false;
            this.btnCategorias.Click += new System.EventHandler(this.btnCategorias_Click);
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(61)))), ((int)(((byte)(92)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Gainsboro;
            this.button1.Image = global::ModernGUI_V3.Properties.Resources.loupe__1_;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(11, 32);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(102, 29);
            this.button1.TabIndex = 10;
            this.button1.Text = "FILTRAR:";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // txtFiltro
            // 
            this.txtFiltro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFiltro.Location = new System.Drawing.Point(118, 39);
            this.txtFiltro.Name = "txtFiltro";
            this.txtFiltro.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtFiltro.Size = new System.Drawing.Size(370, 20);
            this.txtFiltro.TabIndex = 9;
            this.txtFiltro.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDTransporte
            // 
            this.IDTransporte.DataPropertyName = "IDTransporte";
            this.IDTransporte.FillWeight = 55F;
            this.IDTransporte.HeaderText = "ID";
            this.IDTransporte.MinimumWidth = 50;
            this.IDTransporte.Name = "IDTransporte";
            this.IDTransporte.ReadOnly = true;
            // 
            // NombreTransporte
            // 
            this.NombreTransporte.DataPropertyName = "NombreTransporte";
            this.NombreTransporte.HeaderText = "NombreTransporte";
            this.NombreTransporte.Name = "NombreTransporte";
            this.NombreTransporte.ReadOnly = true;
            // 
            // TituloContacto
            // 
            this.TituloContacto.DataPropertyName = "TituloContacto";
            this.TituloContacto.HeaderText = "Contacto";
            this.TituloContacto.Name = "TituloContacto";
            this.TituloContacto.ReadOnly = true;
            // 
            // TelefonoContacto
            // 
            this.TelefonoContacto.DataPropertyName = "TelefonoContacto";
            this.TelefonoContacto.HeaderText = "TelefonoContacto";
            this.TelefonoContacto.Name = "TelefonoContacto";
            this.TelefonoContacto.ReadOnly = true;
            // 
            // TipoTransporte
            // 
            this.TipoTransporte.DataPropertyName = "TipoTransporte";
            this.TipoTransporte.HeaderText = "TipoTransporte";
            this.TipoTransporte.Name = "TipoTransporte";
            this.TipoTransporte.ReadOnly = true;
            // 
            // BuscarTranportista
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 367);
            this.Controls.Add(this.dtgvClientes);
            this.Controls.Add(this.panelBusqueda);
            this.Name = "BuscarTranportista";
            this.Text = "BuscarTranportista";
            this.Load += new System.EventHandler(this.BuscarTranportista_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvClientes)).EndInit();
            this.panelBusqueda.ResumeLayout(false);
            this.panelBusqueda.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgvClientes;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDTransporte;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreTransporte;
        private System.Windows.Forms.DataGridViewTextBoxColumn TituloContacto;
        private System.Windows.Forms.DataGridViewTextBoxColumn TelefonoContacto;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoTransporte;
        private System.Windows.Forms.Panel panelBusqueda;
        private System.Windows.Forms.Button btnCategorias;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtFiltro;
    }
}