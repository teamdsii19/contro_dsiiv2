﻿using CacheManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.EDC
{
    public partial class DetallesCompra : Form
    {

        CLS.Compras oCompras = new CLS.Compras();
        DataTable _sourceDate = new DataTable();
        String idCom;

        public string IdCom { get => idCom; set => idCom = value; }

        public DetallesCompra()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
            comboBox1.SelectedIndex = 0;
        }

        private void DetallesCompra_Load(object sender, EventArgs e)
        {
            CargarDatos();
            Cargarlbltxt();
        }
        public void CargarDatos()
        {
            this.txtIDC.Text = idCom;
            try
            {
                _sourceDate = CacheManager.SystemCache.ComprasPorID(IdCom);
                oCompras.IDCompra1 = Convert.ToInt32(_sourceDate.Rows[0][0]);
                oCompras.IDProveedor1 = Convert.ToInt32(_sourceDate.Rows[0][1]);
                oCompras.FechaFactura1 = _sourceDate.Rows[0][2].ToString();
                oCompras.FechaIngreso1 = _sourceDate.Rows[0][3].ToString();
                oCompras.Descripcion = _sourceDate.Rows[0][4].ToString();
                oCompras.AnexoDocumento1 = (byte[])_sourceDate.Rows[0][5];

            }
            catch
            {
                MessageBox.Show("error1");
            }
        }

        public void Cargarlbltxt()
        {
            lblFechaFcatura.Text = oCompras.FechaFactura1;
            lblFechaIngreso.Text = oCompras.FechaIngreso1;
            lblProveedor.Text = _sourceDate.Rows[0][6].ToString();
            txtDescripcion.Text = oCompras.Descripcion;


        }

        private void BtnVerArchivo_Click(object sender, EventArgs e)
        {
            ProcessArchivos();
        }

        private void ProcessArchivos()
        {
            if (!System.IO.File.Exists("C:\\CopiasDocComprasERP\\" + "AnexoCompra_" + idCom + comboBox1.SelectedItem.ToString()))
            {
                try
                {
                    Operaciones.GuardarCopia("C:\\CopiasDocComprasERP", "AnexoCompra_" + idCom + comboBox1.SelectedItem.ToString(), oCompras.AnexoDocumento1);
                    Operaciones.VerArchivo("C:\\CopiasDocComprasERP\\" + "AnexoCompra_" + idCom + comboBox1.SelectedItem.ToString());

                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
            else
            {
                try
                {
                    Operaciones.EliminarArchivo("C:\\CopiasDocComprasERP\\" + "AnexoCompra_" + idCom + comboBox1.SelectedItem.ToString());
                    ProcessArchivos();
                }
                catch
                {
                    MessageBox.Show("Error Al Eliminar");
                }
            }
        }

        private void BtnSeleccionar_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtFile.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void Copiar_Click(object sender, EventArgs e)
        {
           MessageBox.Show("GUARDADO: "+ Operaciones.GuardarCopia(txtFile.Text, "AnexoCompra_" + idCom + comboBox1.SelectedItem.ToString(), oCompras.AnexoDocumento1));
        }

        private void DetallesCompra_ControlRemoved(object sender, ControlEventArgs e)
        {

        }

        private void DetallesCompra_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Operaciones.EliminarArchivo("C:\\CopiasDocComprasERP\\" + "AnexoCompra_" + idCom + comboBox1.SelectedItem.ToString());
                
            }
            catch
            {
                MessageBox.Show("Error Al Eliminar");
            }
        }
    }
}