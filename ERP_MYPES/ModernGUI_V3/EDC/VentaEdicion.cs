﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.EDC
{
    public partial class VentaEdicion : Form
    {
        BindingSource _Productos = new BindingSource();
        public static bool edicion = false;
        private int existenciasParaEdicion = 0;
        private static int PosTablaEdicionVenta = 0;
        private static DataTable TablaProductos = new DataTable();

        public VentaEdicion()
        {
            InitializeComponent();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PanelTop_Paint(object sender, PaintEventArgs e)
        {

        }

        private void VentaEdicion_Load(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            this.FiltrarLocalmente();
        }

        public void FiltrarLocalmente()
        {
            _Productos.DataSource = CacheManager.SystemCache.TODOSLOSPRODUCTOS();
            try
            {
                if (txtFiltro.TextLength > 0)
                {
                    _Productos.Filter = "NombreProducto LIKE '%" + txtFiltro.Text + "%' OR Alias LIKE '% " + txtFiltro.Text + "%' OR codigoBarras LIKE '%" + txtFiltro.Text + "%'";  
                    dtgvProductos.AutoGenerateColumns = false;
                    dtgvProductos.DataSource = _Productos;
                }
                else
                {
                    _Productos.RemoveFilter();
                    _Productos.DataSource = null;
                    dtgvProductos.AutoGenerateColumns = false;
                    dtgvProductos.DataSource = _Productos;
                    dtgvProductos.Refresh();
                }
                tsRegistro1.Text = dtgvProductos.Rows.Count.ToString() + " Registro encontrados";
            }
            catch
            {
            }
        }

        private void dtgvProductos_KeyDown(object sender, KeyEventArgs e)
        {
            this.Previsualizacion.Visible = true;
            try
            {
                this.Previsualizacion.Image = Image.FromStream(CacheManager.Operaciones.PrepararImagenParaMostrar((byte[])dtgvProductos["imagen", dtgvProductos.CurrentRow.Index].Value));
            }
            catch
            {
                this.Previsualizacion.Visible = false;
            }
        }

        private void dtgvProductos_KeyUp(object sender, KeyEventArgs e)
        {
            this.Previsualizacion.Visible = true;
            try
            {
                this.Previsualizacion.Image = Image.FromStream(CacheManager.Operaciones.PrepararImagenParaMostrar((byte[])dtgvProductos["imagen", dtgvProductos.CurrentRow.Index].Value));
            }
            catch
            {
                this.Previsualizacion.Visible = false;

            }
        }

        private void dtgvProductos_MouseMove(object sender, MouseEventArgs e)
        {
            this.Previsualizacion.Visible = true;
            try
            {
                this.Previsualizacion.Image = Image.FromStream(CacheManager.Operaciones.PrepararImagenParaMostrar((byte[])dtgvProductos["imagen", dtgvProductos.CurrentRow.Index].Value));
            }
            catch
            {
                this.Previsualizacion.Visible = false;
            }
        }

        public void CargarAtablaLocalEdicion()
        {
            dtgvProductos.Columns.Clear();
            BindingSource tempo = new BindingSource();
            tempo.DataSource = TablaProductos;
            this.dtgvProductos.DataSource = tempo;
            this.txtFiltro.Enabled = false;
            this.button1.Enabled = false;
            this.button1.Visible = false;
            this.lblTitulo.Text = "Edicion de venta: ";
            dtgvProductos.Columns[3].Width = 60;
            dtgvProductos.Columns[0].Width = 45;
            this.dtgvProductos.Columns[6].ReadOnly = true;
            this.dtgvProductos.Columns[6].Visible = false;
            this.textBox1.Text = dtgvProductos.CurrentRow.Cells[3].Value.ToString();
            this.textBox2.Text = dtgvProductos.CurrentRow.Cells[5].Value.ToString();
        }

        public static void CargarATablaLocalPorEdicion(int posicion, Object[] fila)
        {
            TablaProductos = new DataTable();
            DataColumn id = new DataColumn("ID", Type.GetType("System.String"));
            DataColumn Nombre = new DataColumn("Nombre", Type.GetType("System.String"));
            DataColumn Barras = new DataColumn("Co.Barras", Type.GetType("System.String"));
            DataColumn Cantidad = new DataColumn("Cantidad", Type.GetType("System.Int32"));
            DataColumn Descuento = new DataColumn("Descuento", Type.GetType("System.Double"));
            DataColumn precio = new DataColumn("Precio", Type.GetType("System.Double"));
            DataColumn img = new DataColumn("imagen", Type.GetType("System.Byte[]"));
            PosTablaEdicionVenta = posicion;

            TablaProductos.Columns.Add(id);
            TablaProductos.Columns.Add(Nombre);
            TablaProductos.Columns.Add(Barras);
            TablaProductos.Columns.Add(Cantidad);
            TablaProductos.Columns.Add(precio);
            TablaProductos.Columns.Add(Descuento);
            TablaProductos.Columns.Add(img);
            DataRow fil = TablaProductos.NewRow();
            for (int i = 0; i < 7; i++)
            {
                fil[i] = fila[i];
            }
            TablaProductos.Rows.Add(fil);
            edicion = true;
        }


        private void btnListo_Click(object sender, EventArgs e)
        {
            if (this.textBox1.TextLength > 0)
            {
                if (Existencias())
                {
                    if (this.conversion(textBox1.Text) > 0 && this.conversion(textBox2.Text) < this.conversion(textBox1.Text))
                    {
                        /*        DataRow datos = GUI.Ventas.TablaProductos.NewRow();
                                datos[0] = this.dtgvProductos.CurrentRow.Cells[0].Value.ToString();
                                datos[1] = this.dtgvProductos.CurrentRow.Cells[1].Value.ToString();
                                datos[2] = this.dtgvProductos.CurrentRow.Cells[2].Value.ToString();
                                datos[4] = Convert.ToDouble(this.dtgvProductos.CurrentRow.Cells[4].Value);
                                datos[3] = Convert.ToInt32(textBox1.Text);
                                if (textBox2.TextLength > 0)
                                {
                                    datos[5] = this.textBox2.Text;
                                }
                                else
                                {
                                    datos[5] = 0;
                                }
                                datos[6] = CacheManager.Operaciones.PrepararImagenParaGuardar(this.Previsualizacion);
                                GUI.Ventas.TablaProductos.Rows.InsertAt(datos,PosTablaEdicionVenta);*/
                        GUI.Ventas.TablaProductos.Rows[PosTablaEdicionVenta][3] = Convert.ToInt32(textBox1.Text);
                        if (textBox2.TextLength > 0)
                        {
                            GUI.Ventas.TablaProductos.Rows[PosTablaEdicionVenta][5] = this.textBox2.Text;
                        }
                        else
                        {
                            GUI.Ventas.TablaProductos.Rows[PosTablaEdicionVenta][5] = 0;
                        }
                        int actualDB = CLS.Ordenes.TraerExistencias(this.dtgvProductos.CurrentRow.Cells[0].Value.ToString());
                        actualDB += Convert.ToInt32(this.dtgvProductos.CurrentRow.Cells["Cantidad"].Value);
                        actualDB -= Convert.ToInt32(textBox1.Text);
                        CLS.Ordenes.ActualizarExistencias(this.dtgvProductos.CurrentRow.Cells[0].Value.ToString(), actualDB);
                        double desco = (Convert.ToUInt32(this.dtgvProductos.CurrentRow.Cells["Cantidad"].Value)) * (Convert.ToDouble(this.dtgvProductos.CurrentRow.Cells["Descuento"].Value));
                        CLS.Ordenes.ActualizarDetalle(GUI.Ventas.idVenta, this.dtgvProductos.CurrentRow.Cells[0].Value.ToString(), Convert.ToInt32(textBox1.Text), desco);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Incongruencia en ciertos parametros de la venta", "INCONGRUENCIAS");
                    }
                }
                else
                {
                    MessageBox.Show("La cantidad requerida es mayor que la DISPONIBLE EN STOCK o NO ES VALIDA", "NO HAY SUFICIENTES EXISTENCIAS / CANTIDAD NO VALIDA");
                }
            }
            else
            {
                MessageBox.Show("Ingrese la cantidad a vender");
            }

        }

        private bool Existencias()
        {
            bool e = false;
            int exis = 0;
            try
            {
                if (edicion)
                {
                    //   exis = Convert.ToInt32(this.dtgvProductos.CurrentRow.Cells[3].Value);
                    exis = Convert.ToInt32(textBox1.Text);
                    int existe = CLS.Ordenes.TraerExistencias(this.dtgvProductos.CurrentRow.Cells[0].Value.ToString());
                    MessageBox.Show("Existencias:" + existe);
                    if (existe >= exis)
                    {
                        e = true;
                    }
                }
                else
                {
                    exis = Convert.ToInt32(this.dtgvProductos.CurrentRow.Cells["UnidadesEnStock"].Value);
                    if (exis >= Convert.ToInt32(textBox1.Text))
                    {
                        e = true;
                    }
                }
            }
            catch
            {
                MessageBox.Show("ERROE EN FUNCION EXISTENCIAS");
            }
            return e;
        }

        private double conversion(String valor)
        {
            double re = 0;
            try
            {
                re = Convert.ToDouble(valor);
            }
            catch
            {
                re = 0;
            }
            return re;
        }


    }
}
