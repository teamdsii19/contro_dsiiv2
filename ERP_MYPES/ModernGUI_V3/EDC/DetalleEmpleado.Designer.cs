﻿namespace ModernGUI_V3.EDC
{
    partial class DetalleEmpleado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DetalleEmpleado));
            this.btnAceptar = new System.Windows.Forms.Button();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.fotoEmpleado = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDUI = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNIT = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSueldo = new System.Windows.Forms.TextBox();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTitulo = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tabControlHistorico = new System.Windows.Forms.TabControl();
            this.tpSucursales = new System.Windows.Forms.TabPage();
            this.dtgSucursales = new System.Windows.Forms.DataGridView();
            this.NombreCargo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpSueldos = new System.Windows.Forms.TabPage();
            this.dtgSueldos = new System.Windows.Forms.DataGridView();
            this.valor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaAsignacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtCargo = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.fotoEmpleado)).BeginInit();
            this.tabControlHistorico.SuspendLayout();
            this.tpSucursales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgSucursales)).BeginInit();
            this.tpSueldos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgSueldos)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAceptar
            // 
            this.btnAceptar.BackColor = System.Drawing.Color.CadetBlue;
            this.btnAceptar.Font = new System.Drawing.Font("Source Sans Pro Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAceptar.ForeColor = System.Drawing.Color.White;
            this.btnAceptar.Location = new System.Drawing.Point(728, 585);
            this.btnAceptar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(108, 39);
            this.btnAceptar.TabIndex = 53;
            this.btnAceptar.Text = "ACEPTAR";
            this.btnAceptar.UseVisualStyleBackColor = false;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // txtDireccion
            // 
            this.txtDireccion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDireccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDireccion.Location = new System.Drawing.Point(313, 257);
            this.txtDireccion.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.ReadOnly = true;
            this.txtDireccion.Size = new System.Drawing.Size(508, 14);
            this.txtDireccion.TabIndex = 45;
            this.txtDireccion.TextChanged += new System.EventHandler(this.txtDireccion_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(593, 304);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 16);
            this.label8.TabIndex = 44;
            this.label8.Text = "Fecha Nac:";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // txtCorreo
            // 
            this.txtCorreo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCorreo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorreo.Location = new System.Drawing.Point(98, 304);
            this.txtCorreo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.ReadOnly = true;
            this.txtCorreo.Size = new System.Drawing.Size(221, 14);
            this.txtCorreo.TabIndex = 43;
            this.txtCorreo.TextChanged += new System.EventHandler(this.txtCorreo_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(31, 304);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 16);
            this.label6.TabIndex = 42;
            this.label6.Text = "Correo:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(230, 255);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 16);
            this.label4.TabIndex = 40;
            this.label4.Text = "Dirección:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtNombre
            // 
            this.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNombre.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.Location = new System.Drawing.Point(383, 194);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.ReadOnly = true;
            this.txtNombre.Size = new System.Drawing.Size(438, 24);
            this.txtNombre.TabIndex = 36;
            this.txtNombre.TextChanged += new System.EventHandler(this.txtNombre_TextChanged);
            // 
            // txtID
            // 
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.Location = new System.Drawing.Point(289, 151);
            this.txtID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtID.Name = "txtID";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(116, 14);
            this.txtID.TabIndex = 35;
            this.txtID.TextChanged += new System.EventHandler(this.txtID_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(234, 149);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 16);
            this.label1.TabIndex = 34;
            this.label1.Text = "ID :";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // fotoEmpleado
            // 
            this.fotoEmpleado.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.fotoEmpleado.ErrorImage = ((System.Drawing.Image)(resources.GetObject("fotoEmpleado.ErrorImage")));
            this.fotoEmpleado.Location = new System.Drawing.Point(24, 23);
            this.fotoEmpleado.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.fotoEmpleado.Name = "fotoEmpleado";
            this.fotoEmpleado.Size = new System.Drawing.Size(186, 215);
            this.fotoEmpleado.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fotoEmpleado.TabIndex = 55;
            this.fotoEmpleado.TabStop = false;
            this.fotoEmpleado.Click += new System.EventHandler(this.fotoEmpleado_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(234, 198);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 16);
            this.label2.TabIndex = 56;
            this.label2.Text = "Nombre Completo:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtDUI
            // 
            this.txtDUI.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDUI.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDUI.Location = new System.Drawing.Point(483, 149);
            this.txtDUI.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDUI.Name = "txtDUI";
            this.txtDUI.ReadOnly = true;
            this.txtDUI.Size = new System.Drawing.Size(116, 14);
            this.txtDUI.TabIndex = 58;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(441, 152);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 16);
            this.label3.TabIndex = 57;
            this.label3.Text = "DUI:";
            this.label3.Click += new System.EventHandler(this.label3_Click_1);
            // 
            // txtNIT
            // 
            this.txtNIT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNIT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNIT.Location = new System.Drawing.Point(692, 151);
            this.txtNIT.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNIT.Name = "txtNIT";
            this.txtNIT.ReadOnly = true;
            this.txtNIT.Size = new System.Drawing.Size(125, 14);
            this.txtNIT.TabIndex = 60;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(651, 151);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 16);
            this.label5.TabIndex = 59;
            this.label5.Text = "NIT:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(230, 30);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 16);
            this.label7.TabIndex = 61;
            this.label7.Text = "Sueldo Actual:  $";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // txtSueldo
            // 
            this.txtSueldo.BackColor = System.Drawing.SystemColors.Control;
            this.txtSueldo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSueldo.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSueldo.ForeColor = System.Drawing.Color.Green;
            this.txtSueldo.Location = new System.Drawing.Point(345, 24);
            this.txtSueldo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSueldo.Name = "txtSueldo";
            this.txtSueldo.ReadOnly = true;
            this.txtSueldo.Size = new System.Drawing.Size(162, 24);
            this.txtSueldo.TabIndex = 62;
            this.txtSueldo.TextChanged += new System.EventHandler(this.txtSueldo_TextChanged);
            // 
            // txtTelefono
            // 
            this.txtTelefono.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTelefono.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefono.Location = new System.Drawing.Point(423, 306);
            this.txtTelefono.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.ReadOnly = true;
            this.txtTelefono.Size = new System.Drawing.Size(148, 15);
            this.txtTelefono.TabIndex = 64;
            this.txtTelefono.TextChanged += new System.EventHandler(this.txtTelefono_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(339, 304);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 16);
            this.label9.TabIndex = 63;
            this.label9.Text = "Telefono:";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label16.Location = new System.Drawing.Point(689, 331);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 16);
            this.label16.TabIndex = 87;
            this.label16.Text = "( Año / Mes / dia )";
            this.label16.Click += new System.EventHandler(this.label16_Click);
            // 
            // dtpFecha
            // 
            this.dtpFecha.CustomFormat = "yyyy-MM-dd";
            this.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFecha.Location = new System.Drawing.Point(689, 304);
            this.dtpFecha.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(137, 21);
            this.dtpFecha.TabIndex = 86;
            this.dtpFecha.ValueChanged += new System.EventHandler(this.dtpFecha_ValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(21, 251);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 16);
            this.label10.TabIndex = 88;
            this.label10.Text = "Titulo:";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // txtTitulo
            // 
            this.txtTitulo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitulo.Location = new System.Drawing.Point(81, 251);
            this.txtTitulo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTitulo.Name = "txtTitulo";
            this.txtTitulo.ReadOnly = true;
            this.txtTitulo.Size = new System.Drawing.Size(129, 14);
            this.txtTitulo.TabIndex = 89;
            this.txtTitulo.TextChanged += new System.EventHandler(this.txtTitulo_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Montserrat", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(13, 354);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(156, 19);
            this.label20.TabIndex = 96;
            this.label20.Text = "Datos Historicos:";
            this.label20.Click += new System.EventHandler(this.label20_Click);
            // 
            // tabControlHistorico
            // 
            this.tabControlHistorico.Controls.Add(this.tpSucursales);
            this.tabControlHistorico.Controls.Add(this.tpSueldos);
            this.tabControlHistorico.Location = new System.Drawing.Point(9, 390);
            this.tabControlHistorico.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabControlHistorico.Name = "tabControlHistorico";
            this.tabControlHistorico.SelectedIndex = 0;
            this.tabControlHistorico.Size = new System.Drawing.Size(715, 256);
            this.tabControlHistorico.TabIndex = 95;
            this.tabControlHistorico.SelectedIndexChanged += new System.EventHandler(this.tabControlHistorico_SelectedIndexChanged);
            // 
            // tpSucursales
            // 
            this.tpSucursales.BackColor = System.Drawing.Color.Silver;
            this.tpSucursales.Controls.Add(this.dtgSucursales);
            this.tpSucursales.Location = new System.Drawing.Point(4, 25);
            this.tpSucursales.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpSucursales.Name = "tpSucursales";
            this.tpSucursales.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpSucursales.Size = new System.Drawing.Size(707, 227);
            this.tpSucursales.TabIndex = 0;
            this.tpSucursales.Text = "Sucursales-Cargos";
            // 
            // dtgSucursales
            // 
            this.dtgSucursales.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgSucursales.BackgroundColor = System.Drawing.SystemColors.ScrollBar;
            this.dtgSucursales.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtgSucursales.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.dtgSucursales.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NombreCargo,
            this.fecha,
            this.nombre});
            this.dtgSucursales.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgSucursales.GridColor = System.Drawing.SystemColors.ActiveCaption;
            this.dtgSucursales.Location = new System.Drawing.Point(4, 4);
            this.dtgSucursales.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtgSucursales.Name = "dtgSucursales";
            this.dtgSucursales.ReadOnly = true;
            this.dtgSucursales.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dtgSucursales.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgSucursales.Size = new System.Drawing.Size(699, 219);
            this.dtgSucursales.TabIndex = 0;
            // 
            // NombreCargo
            // 
            this.NombreCargo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.NombreCargo.DataPropertyName = "NombreCargo";
            this.NombreCargo.HeaderText = "Cargo";
            this.NombreCargo.Name = "NombreCargo";
            this.NombreCargo.ReadOnly = true;
            this.NombreCargo.Width = 139;
            // 
            // fecha
            // 
            this.fecha.DataPropertyName = "fecha";
            this.fecha.HeaderText = "Fecha Asignacion";
            this.fecha.Name = "fecha";
            this.fecha.ReadOnly = true;
            // 
            // nombre
            // 
            this.nombre.DataPropertyName = "nombre";
            this.nombre.HeaderText = "Sucursal";
            this.nombre.Name = "nombre";
            this.nombre.ReadOnly = true;
            // 
            // tpSueldos
            // 
            this.tpSueldos.BackColor = System.Drawing.Color.Silver;
            this.tpSueldos.Controls.Add(this.dtgSueldos);
            this.tpSueldos.Location = new System.Drawing.Point(4, 24);
            this.tpSueldos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpSueldos.Name = "tpSueldos";
            this.tpSueldos.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpSueldos.Size = new System.Drawing.Size(707, 228);
            this.tpSueldos.TabIndex = 1;
            this.tpSueldos.Text = "Sueldos";
            // 
            // dtgSueldos
            // 
            this.dtgSueldos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgSueldos.BackgroundColor = System.Drawing.SystemColors.ScrollBar;
            this.dtgSueldos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtgSueldos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.dtgSueldos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.valor,
            this.fechaAsignacion});
            this.dtgSueldos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgSueldos.Location = new System.Drawing.Point(4, 4);
            this.dtgSueldos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtgSueldos.MultiSelect = false;
            this.dtgSueldos.Name = "dtgSueldos";
            this.dtgSueldos.ReadOnly = true;
            this.dtgSueldos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgSueldos.Size = new System.Drawing.Size(699, 220);
            this.dtgSueldos.TabIndex = 0;
            // 
            // valor
            // 
            this.valor.DataPropertyName = "valor";
            this.valor.HeaderText = "Sueldo";
            this.valor.Name = "valor";
            this.valor.ReadOnly = true;
            // 
            // fechaAsignacion
            // 
            this.fechaAsignacion.DataPropertyName = "fechaAsignacion";
            this.fechaAsignacion.HeaderText = "Fecha-Asignacion";
            this.fechaAsignacion.Name = "fechaAsignacion";
            this.fechaAsignacion.ReadOnly = true;
            // 
            // txtCargo
            // 
            this.txtCargo.BackColor = System.Drawing.SystemColors.Control;
            this.txtCargo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCargo.Font = new System.Drawing.Font("Source Sans Pro Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCargo.ForeColor = System.Drawing.Color.Green;
            this.txtCargo.Location = new System.Drawing.Point(289, 74);
            this.txtCargo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCargo.Name = "txtCargo";
            this.txtCargo.ReadOnly = true;
            this.txtCargo.Size = new System.Drawing.Size(533, 24);
            this.txtCargo.TabIndex = 98;
            this.txtCargo.TextChanged += new System.EventHandler(this.txtCargo_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(230, 80);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 16);
            this.label11.TabIndex = 97;
            this.label11.Text = "Cargo:";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // DetalleEmpleado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HighlightText;
            this.ClientSize = new System.Drawing.Size(844, 651);
            this.Controls.Add(this.txtCargo);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.tabControlHistorico);
            this.Controls.Add(this.txtTitulo);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.dtpFecha);
            this.Controls.Add(this.txtTelefono);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtSueldo);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtNIT);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtDUI);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fotoEmpleado);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.txtDireccion);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtCorreo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "DetalleEmpleado";
            this.Text = "DetalleEmpleado";
            this.Load += new System.EventHandler(this.DetalleEmpleado_Load);
            ((System.ComponentModel.ISupportInitialize)(this.fotoEmpleado)).EndInit();
            this.tabControlHistorico.ResumeLayout(false);
            this.tpSucursales.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgSucursales)).EndInit();
            this.tpSueldos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgSueldos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.PictureBox fotoEmpleado;
        private System.Windows.Forms.Button btnAceptar;
        public System.Windows.Forms.TextBox txtDireccion;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtCorreo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtNombre;
        public System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtDUI;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtNIT;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtSueldo;
        public System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txtTitulo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TabControl tabControlHistorico;
        public System.Windows.Forms.TabPage tpSucursales;
        public System.Windows.Forms.DataGridView dtgSucursales;
        private System.Windows.Forms.TabPage tpSueldos;
        public System.Windows.Forms.DataGridView dtgSueldos;
        public System.Windows.Forms.TextBox txtCargo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridViewTextBoxColumn valor;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaAsignacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreCargo;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre;
    }
}