﻿namespace ModernGUI_V3.EDC
{
    partial class EdicionCompras
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtIDC = new System.Windows.Forms.TextBox();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.dtpFecIngreso = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFecFactura = new System.Windows.Forms.DateTimePicker();
            this.dtgvProvedores = new System.Windows.Forms.DataGridView();
            this.IDProveedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreProveedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreContacto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TituloContacto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TelefoProveedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TelefoContacto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.municipio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idMunicipio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.btnSeleccionar = new System.Windows.Forms.Button();
            this.btnVerArchivo = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.Notificador = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblProveedor = new System.Windows.Forms.Label();
            this.checkAnexo = new System.Windows.Forms.CheckBox();
            this.checkFecha = new System.Windows.Forms.CheckBox();
            this.checkPro = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvProvedores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Notificador)).BeginInit();
            this.SuspendLayout();
            // 
            // txtIDC
            // 
            this.txtIDC.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIDC.Location = new System.Drawing.Point(26, 32);
            this.txtIDC.Name = "txtIDC";
            this.txtIDC.ReadOnly = true;
            this.txtIDC.Size = new System.Drawing.Size(123, 23);
            this.txtIDC.TabIndex = 28;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.Location = new System.Drawing.Point(23, 9);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(76, 17);
            this.lblUsuario.TabIndex = 27;
            this.lblUsuario.Text = "IDCompra";
            // 
            // dtpFecIngreso
            // 
            this.dtpFecIngreso.CustomFormat = "yyyy-MM-dd";
            this.dtpFecIngreso.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFecIngreso.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFecIngreso.Location = new System.Drawing.Point(29, 99);
            this.dtpFecIngreso.Name = "dtpFecIngreso";
            this.dtpFecIngreso.Size = new System.Drawing.Size(172, 23);
            this.dtpFecIngreso.TabIndex = 73;
            this.dtpFecIngreso.Value = new System.DateTime(2019, 5, 29, 0, 0, 0, 0);
            this.dtpFecIngreso.ValueChanged += new System.EventHandler(this.DtpFecha_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 17);
            this.label1.TabIndex = 74;
            this.label1.Text = "Fecha Ingreso (Hoy):";
            this.label1.Click += new System.EventHandler(this.Label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(228, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 17);
            this.label2.TabIndex = 76;
            this.label2.Text = "Fecha Factura:";
            // 
            // dtpFecFactura
            // 
            this.dtpFecFactura.CustomFormat = "yyyy-MM-dd";
            this.dtpFecFactura.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFecFactura.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFecFactura.Location = new System.Drawing.Point(234, 99);
            this.dtpFecFactura.Name = "dtpFecFactura";
            this.dtpFecFactura.Size = new System.Drawing.Size(172, 23);
            this.dtpFecFactura.TabIndex = 75;
            this.dtpFecFactura.Value = new System.DateTime(2019, 5, 27, 14, 21, 2, 0);
            // 
            // dtgvProvedores
            // 
            this.dtgvProvedores.AllowUserToAddRows = false;
            this.dtgvProvedores.AllowUserToDeleteRows = false;
            this.dtgvProvedores.AllowUserToResizeRows = false;
            this.dtgvProvedores.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgvProvedores.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvProvedores.BackgroundColor = System.Drawing.Color.Silver;
            this.dtgvProvedores.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dtgvProvedores.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.dtgvProvedores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvProvedores.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDProveedor,
            this.NombreProveedor,
            this.NombreContacto,
            this.TituloContacto,
            this.TelefoProveedor,
            this.TelefoContacto,
            this.municipio,
            this.idMunicipio,
            this.dm});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgvProvedores.DefaultCellStyle = dataGridViewCellStyle1;
            this.dtgvProvedores.GridColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.dtgvProvedores.Location = new System.Drawing.Point(26, 297);
            this.dtgvProvedores.MultiSelect = false;
            this.dtgvProvedores.Name = "dtgvProvedores";
            this.dtgvProvedores.ReadOnly = true;
            this.dtgvProvedores.RowHeadersVisible = false;
            this.dtgvProvedores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgvProvedores.Size = new System.Drawing.Size(702, 114);
            this.dtgvProvedores.TabIndex = 77;
            // 
            // IDProveedor
            // 
            this.IDProveedor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IDProveedor.DataPropertyName = "IDProveedor";
            this.IDProveedor.FillWeight = 12.57918F;
            this.IDProveedor.HeaderText = "ID";
            this.IDProveedor.MinimumWidth = 50;
            this.IDProveedor.Name = "IDProveedor";
            this.IDProveedor.ReadOnly = true;
            this.IDProveedor.Width = 179;
            // 
            // NombreProveedor
            // 
            this.NombreProveedor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NombreProveedor.DataPropertyName = "NombreProveedor";
            this.NombreProveedor.FillWeight = 29.15985F;
            this.NombreProveedor.HeaderText = "Proveedor";
            this.NombreProveedor.Name = "NombreProveedor";
            this.NombreProveedor.ReadOnly = true;
            // 
            // NombreContacto
            // 
            this.NombreContacto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NombreContacto.DataPropertyName = "NombreContacto";
            this.NombreContacto.FillWeight = 45F;
            this.NombreContacto.HeaderText = "Contacto";
            this.NombreContacto.Name = "NombreContacto";
            this.NombreContacto.ReadOnly = true;
            // 
            // TituloContacto
            // 
            this.TituloContacto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.TituloContacto.DataPropertyName = "TituloContacto";
            this.TituloContacto.FillWeight = 29.15985F;
            this.TituloContacto.HeaderText = "Titulo de Con.";
            this.TituloContacto.Name = "TituloContacto";
            this.TituloContacto.ReadOnly = true;
            this.TituloContacto.Width = 90;
            // 
            // TelefoProveedor
            // 
            this.TelefoProveedor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.TelefoProveedor.DataPropertyName = "TelefoProveedor";
            this.TelefoProveedor.FillWeight = 20.69429F;
            this.TelefoProveedor.HeaderText = "Tel Proveedor";
            this.TelefoProveedor.Name = "TelefoProveedor";
            this.TelefoProveedor.ReadOnly = true;
            this.TelefoProveedor.Width = 90;
            // 
            // TelefoContacto
            // 
            this.TelefoContacto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.TelefoContacto.DataPropertyName = "TelefoContacto";
            this.TelefoContacto.FillWeight = 15.2758F;
            this.TelefoContacto.HeaderText = "Tel Contacto";
            this.TelefoContacto.Name = "TelefoContacto";
            this.TelefoContacto.ReadOnly = true;
            this.TelefoContacto.Width = 90;
            // 
            // municipio
            // 
            this.municipio.DataPropertyName = "municipio";
            this.municipio.HeaderText = "municipio";
            this.municipio.Name = "municipio";
            this.municipio.ReadOnly = true;
            this.municipio.Visible = false;
            // 
            // idMunicipio
            // 
            this.idMunicipio.DataPropertyName = "idMunicipio";
            this.idMunicipio.HeaderText = "idMunicipio";
            this.idMunicipio.Name = "idMunicipio";
            this.idMunicipio.ReadOnly = true;
            this.idMunicipio.Visible = false;
            // 
            // dm
            // 
            this.dm.DataPropertyName = "dm";
            this.dm.HeaderText = "dm";
            this.dm.Name = "dm";
            this.dm.ReadOnly = true;
            this.dm.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(463, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 17);
            this.label3.TabIndex = 78;
            this.label3.Text = "Descripcion:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(26, 277);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 16);
            this.label4.TabIndex = 79;
            this.label4.Text = "PROVEEDOR";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescripcion.Location = new System.Drawing.Point(455, 99);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(273, 84);
            this.txtDescripcion.TabIndex = 81;
            this.txtDescripcion.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(26, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(188, 17);
            this.label5.TabIndex = 83;
            this.label5.Text = "DocuemntoAnexo(PDF,IMG)";
            // 
            // txtFile
            // 
            this.txtFile.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtFile.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFile.Location = new System.Drawing.Point(29, 173);
            this.txtFile.Name = "txtFile";
            this.txtFile.ReadOnly = true;
            this.txtFile.Size = new System.Drawing.Size(377, 23);
            this.txtFile.TabIndex = 84;
            this.txtFile.TextChanged += new System.EventHandler(this.TxtFile_TextChanged);
            // 
            // btnSeleccionar
            // 
            this.btnSeleccionar.Location = new System.Drawing.Point(29, 198);
            this.btnSeleccionar.Margin = new System.Windows.Forms.Padding(1);
            this.btnSeleccionar.Name = "btnSeleccionar";
            this.btnSeleccionar.Size = new System.Drawing.Size(129, 30);
            this.btnSeleccionar.TabIndex = 85;
            this.btnSeleccionar.Text = "Seleccionar Archivo";
            this.btnSeleccionar.UseVisualStyleBackColor = true;
            this.btnSeleccionar.Click += new System.EventHandler(this.BtnSeleccionar_Click);
            // 
            // btnVerArchivo
            // 
            this.btnVerArchivo.Location = new System.Drawing.Point(171, 200);
            this.btnVerArchivo.Margin = new System.Windows.Forms.Padding(1);
            this.btnVerArchivo.Name = "btnVerArchivo";
            this.btnVerArchivo.Size = new System.Drawing.Size(129, 28);
            this.btnVerArchivo.TabIndex = 86;
            this.btnVerArchivo.Text = "Ver Archivo";
            this.btnVerArchivo.UseVisualStyleBackColor = true;
            this.btnVerArchivo.Click += new System.EventHandler(this.BtnVerArchivo_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.LightGray;
            this.btnCancelar.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.Red;
            this.btnCancelar.Location = new System.Drawing.Point(493, 430);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(108, 34);
            this.btnCancelar.TabIndex = 88;
            this.btnCancelar.Text = "CANCELAR";
            this.btnCancelar.UseVisualStyleBackColor = false;
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.Color.LightGray;
            this.btnGuardar.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.ForeColor = System.Drawing.Color.Navy;
            this.btnGuardar.Location = new System.Drawing.Point(626, 430);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(102, 34);
            this.btnGuardar.TabIndex = 87;
            this.btnGuardar.Text = "GUARDAR";
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // Notificador
            // 
            this.Notificador.ContainerControl = this;
            // 
            // lblProveedor
            // 
            this.lblProveedor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblProveedor.AutoSize = true;
            this.lblProveedor.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProveedor.ForeColor = System.Drawing.Color.Lime;
            this.lblProveedor.Location = new System.Drawing.Point(117, 276);
            this.lblProveedor.Name = "lblProveedor";
            this.lblProveedor.Size = new System.Drawing.Size(63, 17);
            this.lblProveedor.TabIndex = 89;
            this.lblProveedor.Text = "Ninguno";
            // 
            // checkAnexo
            // 
            this.checkAnexo.AutoSize = true;
            this.checkAnexo.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkAnexo.Location = new System.Drawing.Point(234, 146);
            this.checkAnexo.Name = "checkAnexo";
            this.checkAnexo.Size = new System.Drawing.Size(116, 21);
            this.checkAnexo.TabIndex = 90;
            this.checkAnexo.Text = "CambiarAnexo";
            this.checkAnexo.UseVisualStyleBackColor = true;
            this.checkAnexo.CheckedChanged += new System.EventHandler(this.CheckAnexo_CheckedChanged);
            // 
            // checkFecha
            // 
            this.checkFecha.AutoSize = true;
            this.checkFecha.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkFecha.Location = new System.Drawing.Point(234, 55);
            this.checkFecha.Name = "checkFecha";
            this.checkFecha.Size = new System.Drawing.Size(115, 21);
            this.checkFecha.TabIndex = 91;
            this.checkFecha.Text = "CambiarFecha";
            this.checkFecha.UseVisualStyleBackColor = true;
            this.checkFecha.CheckedChanged += new System.EventHandler(this.CheckFecha_CheckedChanged);
            // 
            // checkPro
            // 
            this.checkPro.AutoSize = true;
            this.checkPro.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkPro.Location = new System.Drawing.Point(587, 272);
            this.checkPro.Name = "checkPro";
            this.checkPro.Size = new System.Drawing.Size(141, 21);
            this.checkPro.TabIndex = 92;
            this.checkPro.Text = "CambiarProveedor";
            this.checkPro.UseVisualStyleBackColor = true;
            this.checkPro.CheckedChanged += new System.EventHandler(this.CheckPro_CheckedChanged);
            // 
            // EdicionCompras
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 476);
            this.Controls.Add(this.checkPro);
            this.Controls.Add(this.checkFecha);
            this.Controls.Add(this.checkAnexo);
            this.Controls.Add(this.lblProveedor);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnVerArchivo);
            this.Controls.Add(this.btnSeleccionar);
            this.Controls.Add(this.txtFile);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtgvProvedores);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtpFecFactura);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpFecIngreso);
            this.Controls.Add(this.txtIDC);
            this.Controls.Add(this.lblUsuario);
            this.Name = "EdicionCompras";
            this.Text = "EdicionCompras";
            this.Load += new System.EventHandler(this.EdicionCompras_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvProvedores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Notificador)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.TextBox txtIDC;
        private System.Windows.Forms.Label lblUsuario;
        public System.Windows.Forms.DateTimePicker dtpFecIngreso;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.DateTimePicker dtpFecFactura;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDProveedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreProveedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreContacto;
        private System.Windows.Forms.DataGridViewTextBoxColumn TituloContacto;
        private System.Windows.Forms.DataGridViewTextBoxColumn TelefoProveedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn TelefoContacto;
        private System.Windows.Forms.DataGridViewTextBoxColumn municipio;
        private System.Windows.Forms.DataGridViewTextBoxColumn idMunicipio;
        private System.Windows.Forms.DataGridViewTextBoxColumn dm;
        private System.Windows.Forms.RichTextBox txtDescripcion;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.ErrorProvider Notificador;
        public System.Windows.Forms.Label lblProveedor;
        public System.Windows.Forms.DataGridView dtgvProvedores;
        public System.Windows.Forms.CheckBox checkPro;
        public System.Windows.Forms.CheckBox checkFecha;
        public System.Windows.Forms.CheckBox checkAnexo;
        public System.Windows.Forms.Button btnSeleccionar;
        public System.Windows.Forms.Button btnVerArchivo;
    }
}