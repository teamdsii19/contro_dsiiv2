﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModernGUI_V3.CLS;

namespace ModernGUI_V3.EDC
{
    public partial class BuscarCliente : Form
    {
        CLS.Clientes oCliente= new CLS.Clientes();
        BindingSource _Clientes = new BindingSource();

        internal Clientes OCliente { get => oCliente; set => oCliente = value; }

        public BuscarCliente()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void BuscarCliente_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        public void CargarDatos()
        {
            try
            {
                _Clientes.DataSource = CacheManager.SystemCache.TODOS_CLIENTES();
                FiltrarLocalmente();
            }
            catch { }
        }
        public void FiltrarLocalmente()
        {
            try
            {
                if (txtFiltro.TextLength > 0)
                {
                    _Clientes.Filter = "NombreContacto LIKE '%" + txtFiltro.Text + "%'";
                }
                else
                {
                    _Clientes.RemoveFilter();
                }
                dtgvClientes.AutoGenerateColumns = false;
                dtgvClientes.DataSource = _Clientes;
            }
            catch
            {

            }
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        private void btnCategorias_Click(object sender, EventArgs e)
        {
            oCliente.IDCliente1 = this.dtgvClientes.CurrentRow.Cells["IDCliente"].Value.ToString();
            oCliente.NombreContacto1 = this.dtgvClientes.CurrentRow.Cells["NombreContacto"].Value.ToString();
            Close();
        }
    }
}
