﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.EDC
{
    public partial class BuscarEmpleado : Form
    {
        BindingSource Empleados = new BindingSource();
        String _Nombre;
        String _Id;

        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string Id { get => _Id; set => _Id = value; }

        public BuscarEmpleado()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void BuscarEmpleado_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        public void CargarDatos()
        {
            try
            {
                Empleados.DataSource = CacheManager.SystemCache.TODOS_EMPLEADOS_CARGOS();
                FiltrarLocalmente();
            }
            catch { }
        }
        public void FiltrarLocalmente()
        {
            try
            {
                if (txtFiltro.TextLength > 0)
                {
                    Empleados.Filter = "NombreTransporte LIKE '%" + txtFiltro.Text + "%'";
                }
                else
                {
                    Empleados.RemoveFilter();
                }
                dtgvClientes.AutoGenerateColumns = false;
                dtgvClientes.DataSource = Empleados;
            }
            catch
            {

            }
        }

        private void btnCategorias_Click(object sender, EventArgs e)
        {
            this.Id = this.dtgvClientes.CurrentRow.Cells["IDEmpleado"].Value.ToString();
            this.Nombre = this.dtgvClientes.CurrentRow.Cells["NombreEmpleado"].Value.ToString();
            Close();
        }
    }
}
