﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.EDC
{
    public partial class DetalleEmpleado : Form
    {

        BindingSource _Sueldos = new BindingSource();
        BindingSource _Sucursales = new BindingSource();
        private static String idEmpleado;

        public static string IdEmpleado { get => idEmpleado; set => idEmpleado = value; }

        //        DataTable Pro;

        //     public string IdEmpleado { get => idEmpleado; set => idEmpleado = value; }

        public DetalleEmpleado()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        public void CargarDatos()
        {
            try
            {
                _Sucursales.DataSource = CacheManager.SystemCache.HistorialEmpleado(0,IdEmpleado);
                _Sueldos.DataSource = CacheManager.SystemCache.HistorialEmpleado(1, idEmpleado);
                FiltrarLocalmente();
            }
            catch
            {
            }
        }

//        a.IDCargo,b.NombreCargo,e.IDSucursal,e.nombre,concat(g.Departamento,' ', f.Municipio,' ', e.Direcion)
  //              as 'direccion
        public void FiltrarLocalmente()
        {
            try
            {
                dtgSucursales.AutoGenerateColumns = false;
                dtgSucursales.DataSource = _Sucursales;
                dtgSueldos.AutoGenerateColumns = false;
                dtgSueldos.DataSource = _Sueldos;
                //     tsRegistro1.Text = dtgvEmpleados.Rows.Count.ToString() + " Registro encontrados";
            }
            catch
            {

            }
        }

        private void DetalleEmpleado_Load(object sender, EventArgs e)
        {
            this.CargarDatos();
        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txtDUI_TextChanged(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtCorreo_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtTitulo_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDireccion_TextChanged(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void txtTelefono_TextChanged(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void txtSueldo_TextChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click_1(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtCargo_TextChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void fotoEmpleado_Click(object sender, EventArgs e)
        {

        }

        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void tabControlHistorico_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dtpFecha_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
    }
}
