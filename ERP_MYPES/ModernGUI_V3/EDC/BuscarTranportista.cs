﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModernGUI_V3.EDC
{
    public partial class BuscarTranportista : Form
    {
        BindingSource _Transportes = new BindingSource();
        String _NombreTrasporte;
        String _IdTransporte;

        public string NombreTrasporte { get => _NombreTrasporte; set => _NombreTrasporte = value; }
        public string IdTransporte { get => _IdTransporte; set => _IdTransporte = value; }

        public BuscarTranportista()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void BuscarTranportista_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }
        public void CargarDatos()
        {
            try
            {
                _Transportes.DataSource = CacheManager.SystemCache.TODOS_TRANSPORTES();
                dtgvClientes.AutoGenerateColumns = false;
                dtgvClientes.DataSource = _Transportes;
            }
            catch { }
        }
        public void FiltrarLocalmente()
        {
            try
            {
                if (txtFiltro.TextLength > 0)
                {
                    _Transportes.Filter = "NombreTransporte LIKE '%" + txtFiltro.Text + "%'";
                }
                else
                {
                    _Transportes.RemoveFilter();
                }
                
            }
            catch
            {

            }
        }

        private void btnCategorias_Click(object sender, EventArgs e)
        {
            this.IdTransporte= this.dtgvClientes.CurrentRow.Cells["IdTransporte"].Value.ToString();
            this.NombreTrasporte= this.dtgvClientes.CurrentRow.Cells["NombreTransporte"].Value.ToString();
            Close();
        }
    }
}
