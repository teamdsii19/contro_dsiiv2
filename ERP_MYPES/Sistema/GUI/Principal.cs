﻿using System;
using System.Windows.Forms;
using DBManager.CLS;
using CacheManager;
namespace Sistema.GUI
{
    public partial class Principal : Form
    {

        SessionManager.Sesion _SESION = SessionManager.Sesion.Instancia;

        public Principal()
        {
            InitializeComponent();
        }


        private void Principal_Load(object sender, EventArgs e)
        {
            this.LlenarCampos();
        }

        public void LlenarCampos()
        {
            
            txtCadena.Text = AppConfiguration.DesEncriptar(AppConfiguration.RecuperarValor("Cadena", "Null"));
            txtBase.Text = AppConfiguration.DesEncriptar(AppConfiguration.RecuperarValor("BaseD", "Null"));
            txtUsuario.Text = AppConfiguration.DesEncriptar(AppConfiguration.RecuperarValor("DBUsuario", "Null"));
            txtPuerto.Text = AppConfiguration.DesEncriptar(AppConfiguration.RecuperarValor("Puerto", "Null"));
            txtServidor.Text = AppConfiguration.DesEncriptar(AppConfiguration.RecuperarValor("Servidor", "Null"));
            Contraseña.Text = AppConfiguration.DesEncriptar(AppConfiguration.RecuperarValor("Contraseña", "Null"));
        }

        private void Principal_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnAplicar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("La Aplicación se conectará a la BD configurada..." , "CONFIMACION DE CAMBIOS A APLICAR", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
            {
                String CC = AppConfiguration.CrearCadenaEncriptadaMYSQL(txtServidor.Text, txtPuerto.Text, txtBase.Text, txtUsuario.Text, Contraseña.Text);
                Console.Write(CC);
                try
                {
                    AppConfiguration.EstablecerValor("Cadena", CC);
                    AppConfiguration.EstablecerValor("BaseD", AppConfiguration.Encriptar(txtBase.Text));
                    AppConfiguration.EstablecerValor("DBUsuario", AppConfiguration.Encriptar(txtUsuario.Text));
                    AppConfiguration.EstablecerValor("Puerto", AppConfiguration.Encriptar(txtPuerto.Text));
                    AppConfiguration.EstablecerValor("Servidor", AppConfiguration.Encriptar(txtServidor.Text));
                    AppConfiguration.EstablecerValor("Contraseña", AppConfiguration.Encriptar(Contraseña.Text));
                    MessageBox.Show("Guardado con éxito");
                    Close();
                }
                catch
                {
                    MessageBox.Show("Ha ocurrido un error");
                }
            }
        }
    }
}
