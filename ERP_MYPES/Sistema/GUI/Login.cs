﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using DBManager.CLS;
using System.Security.Cryptography;

namespace Sistema.GUI
{
    public partial class Login : Form
    {
        SessionManager.Sesion _SESION = SessionManager.Sesion.Instancia;
        //ATRIBUTO
        Boolean _Autorizado = false;
        //PROPIEDAD DE SOLO LECTURA
        public Boolean Autorizado
        {
            get { return _Autorizado; }
            //set { _Autorizado = value; }
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void Validar(String pUsuario,String pCredencial)
        {
            DataTable DatosUsuario = new DataTable();
            try
            {
                DatosUsuario = CacheManager.SystemCache.ValidarUsuario(pUsuario, pCredencial);
                if(DatosUsuario.Rows.Count==1)
                {
                    _Autorizado = true;
                    _SESION.OUsuario.IDUsuario = DatosUsuario.Rows[0]["IDUsuario"].ToString();
                    _SESION.OUsuario.NombreUsuario = DatosUsuario.Rows[0]["Usuario"].ToString();
                    _SESION.OUsuario.Rol = DatosUsuario.Rows[0]["Rol"].ToString();
                    _SESION.OUsuario.IDRol = DatosUsuario.Rows[0]["IDRol"].ToString();
                    _SESION.OUsuario.IDEmpleado = DatosUsuario.Rows[0]["IDEmpleado"].ToString();
                    _SESION.OUsuario.Empleado = DatosUsuario.Rows[0]["Empleado"].ToString();
                    try
                    {
                        _SESION.OUsuario.Foto =(byte[])DatosUsuario.Rows[0]["foto"];
                    }
                    catch {
                        MessageBox.Show("Fallo");
                    }

                    /*if ((byte)DatosUsuario.Rows[0]["foto"]>0)
                    {
                        byte[] fo = (byte[])DatosUsuario.Rows[0]["foto"];

                        _SESION.OUsuario.Foto =fo;
                    }*/

                    _SESION.OUsuario.ObtenerInfo();
                    Close();
                }else
                {
                    _Autorizado = false;
                    lblMensaje.Text = "USUARIO / CREDENCIAL ERRÓNEOS";
                }
            }
            catch { }
        }

        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        public static string EncodePassword(string originalPassword)
        {
            SHA1 sha1 = new SHA1CryptoServiceProvider();

            byte[] inputBytes = (new UnicodeEncoding()).GetBytes(originalPassword);
            byte[] hash = sha1.ComputeHash(inputBytes);

            return Convert.ToBase64String(hash);
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            Validar(txbUsuario.Text, txbCredencial.Text);
        }

        private void txbCredencial_TextChanged(object sender, EventArgs e)
        {

        }

        private void txbUsuario_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void txbUsuario_Enter(object sender, EventArgs e)
        {
            if (txbUsuario.Text == "USUARIO") {
                txbUsuario.Text = "";
                txbUsuario.ForeColor = Color.LightGray;
            }
        }

        private void txbUsuario_Leave(object sender, EventArgs e)
        {
            if (txbUsuario.Text == "")
            {
                txbUsuario.Text = "USUARIO";
                txbUsuario.ForeColor = Color.DimGray;
            }
        }

        private void txbCredencial_Enter(object sender, EventArgs e)
        {
            if (txbCredencial.Text == "CONTRASEÑA")
            {
                txbCredencial.Text = "";
                txbCredencial.ForeColor = Color.LightGray;
                txbCredencial.UseSystemPasswordChar = true;
            }
        }

        private void txbCredencial_Leave(object sender, EventArgs e)
        {
            if (txbCredencial.Text == "")
            {
                txbCredencial.Text = "CONTRASEÑA";
                txbCredencial.ForeColor = Color.DimGray;
                txbCredencial.UseSystemPasswordChar = false;
            }
        }

        private void Login_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void txbUsuario_KeyPress(object sender, KeyPressEventArgs e){

            if (CacheManager.Operaciones.Enter(e)) {
                Validar(txbUsuario.Text, txbCredencial.Text);
            }
        }

        private void txbCredencial_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void txbCredencial_KeyPress(object sender, KeyPressEventArgs e) {
            if (CacheManager.Operaciones.Enter(e))
            {
                Validar(txbUsuario.Text, txbCredencial.Text);
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {
            String up = EncodePassword(this.txbCredencial.Text);
            String user = this.txbUsuario.Text;
            MessageBox.Show(up);
            String sp = AppConfiguration.RecuperarValorUsarioConfig("Pass", "NULL");
            String su = AppConfiguration.RecuperarValorUsarioConfig("UsuarioInicial", "NULL");
            if (up.Equals(sp) && user.Equals(su))
            {
                Principal pri = new Principal();
                pri.ShowDialog();
            }
            else {
               MessageBox.Show("Datos equivocos "+ AppConfiguration.RecuperarValorUsarioConfig("Pass", "NULL")+" " + AppConfiguration.RecuperarValorUsarioConfig("UsuarioInicial", "NULL"));
            }
        }
    } 
}
