﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema.GUI
{
    public partial class DBPruebas : Form
    {
        public DBPruebas()
        {
            InitializeComponent();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            dtgvDatos.DataSource= oOperacion.Consultar("SELECT * FROM empleados;");
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            DBManager.CLS.DBOperacion obj = new DBManager.CLS.DBOperacion();
            Int32 FilasInsertadas = 0;
            FilasInsertadas = obj.Insertar("INSERT INTO roles(Rol) values('" + txbInsertar.Text + "');");
            if(FilasInsertadas>0)
            {
                dtgvRoles.DataSource = obj.Consultar("SELECT * FROM roles");
            }
        }
    }
}
