﻿namespace Sistema.GUI
{
    partial class Splash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Splash));
            this.Cronometro = new System.Windows.Forms.Timer(this.components);
            this.PBCargar = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PBCargar)).BeginInit();
            this.SuspendLayout();
            // 
            // Cronometro
            // 
            this.Cronometro.Interval = 3400;
            this.Cronometro.Tick += new System.EventHandler(this.Cronometro_Tick);
            // 
            // PBCargar
            // 
            this.PBCargar.BackColor = System.Drawing.SystemColors.Info;
            this.PBCargar.Image = ((System.Drawing.Image)(resources.GetObject("PBCargar.Image")));
            this.PBCargar.Location = new System.Drawing.Point(355, 76);
            this.PBCargar.Name = "PBCargar";
            this.PBCargar.Size = new System.Drawing.Size(56, 50);
            this.PBCargar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PBCargar.TabIndex = 0;
            this.PBCargar.TabStop = false;
            // 
            // Splash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(484, 311);
            this.Controls.Add(this.PBCargar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Splash";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Splash";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Splash_FormClosed);
            this.Load += new System.EventHandler(this.Splash_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PBCargar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer Cronometro;
        private System.Windows.Forms.PictureBox PBCargar;
    }
}