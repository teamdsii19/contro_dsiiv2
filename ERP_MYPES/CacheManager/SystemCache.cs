﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CacheManager
{
    public static class SystemCache
    {
        public static DataTable ParaUnaFactura(String idVenta)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"select 
                (select x.Nombre from generalidades x) as 'Empresa',
                (select x.nrc from generalidades x) as 'nrc',
                (select x.NIT from generalidades x) as 'nit',
                e.AutorizacionSerie,
                a.*,b.idProducto,c.NombreProducto,b.precioUnitario,b.cantidad,
                sum(b.cantidad*c.PrecioUnitario) as 'importe',
                sum(b.cantidad*c.PrecioUnitario)-round((b.cantidad*c.PrecioUnitario*0.13),2) as 'importeSinIVA',
                round((b.cantidad*c.PrecioUnitario*0.13),2) as 'IVA',
                sum(b.descuento) as 'descuento',
                round(sum(b.descuento)/b.cantidad,2) as 'descuentoUnitario',
                round(sum(b.cantidad*c.PrecioUnitario-descuento),2) as 'importeTotalU',
                (select round(sum(y.cantidad*y.precioUnitario)-sum(y.descuento),2) from ordenesdetalles y where y.iDOrden=a.iDOrden) as'TOTAL',
                d.Apellidos
                from ordenes a, ordenesdetalles b, productos c, empleados d, seriesfacturas e
                where a.idOrden=b.idOrden and b.idProducto=c.idProducto and d.idEmpleado=a.idEmpleado and 
                e.idSerie=a.idSerie and a.idOrden=" + idVenta + " group by b.idDetalleOrden");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }


        public static DataTable ValidarUsuario(String pUsuario, String pCredencial)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT a.IDUsuario,
            a.Usuario,
            a.IDRol,
            b.rol,
            a.IDEmpleado,
            CONCAT(c.Nombres,' ',c.Apellidos) as 'Empleado',
            (select foto from fotos fo where fo.idEmpleado=c.IDEmpleado) as 'foto'
            FROM usuarios a, roles b, empleados c
            WHERE a.IDRol=b.IDRol
            AND a.IDEmpleado=c.IDEmpleado
            AND a.Usuario='" + pUsuario+@"'
            AND a.Credencial=sha1('"+pCredencial+"');");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

/*        public static DataTable TODOSLOSPRODUCTOS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"select idProducto,NombreProducto,Alias,MedidaPorUnidad,precioUnitario,c.idCategoria,a.codigoBarras,
                        b.idProveedor,UnidadesEnStock,imagen,minimo,fecha,c.NombreCategroria,b.NombreProveedor,a.costo,
                        concat(a.NombreProducto,' ',a.MedidaPorUnidad) as 'prome'
                        from productos a,proveedores b,categorias c
                        where a.idCategoria=c.idCategoria and b.idProveedor=a.idProveedor order by nombreProducto asc;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }*/

        public static DataTable TODOSLOSPRODUCTOSCB(String codigo)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"select idProducto,NombreProducto,Alias,MedidaPorUnidad,precioUnitario,c.idCategoria,a.codigoBarras,
                        b.idProveedor,UnidadesEnStock,imagen,minimo,fecha,c.NombreCategroria,b.NombreProveedor,a.costo
                        from productos a,proveedores b,categorias c
                        where a.idCategoria=c.idCategoria and b.idProveedor=a.idProveedor and a.codigoBarras='" + codigo + "' order by nombreProducto asc;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }



        public static DataTable TODOSLOSCLIENTES()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"select IDCliente, NombreContacto, TelefoContacto, Direccion, a.idMunicipio ,b.Municipio
            from clientes a, Municipios b where b.idMunicipio=a.idMunicipio;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }


        public static DataTable TODOSLOSEMPLEADOS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT IDEmpleado,
            Nombres, Apellidos,
            CONCAT(a.Nombres,' ',a.Apellidos) as 'NombreCompleto',
            fechaNacimiento, DUI,
            NIT, Telofono,
            a.Direccion,
            notas,Titulo,Correo,b.idDepartamento,c.idMunicipio,b.departamento,c.municipio,
            ifnull((select NombreCargo from cargos x,cargos_empleado y,sucursales z where z.IDSucursal=y.IDSucursal and x.idCargo=y.idCargo and y.idEmpleado=a.IDEmpleado order by y.fechaAsignacion desc limit 1),'') as 'Cargo',
            ifnull((select x.IDCargo from cargos x,cargos_empleado y,sucursales z where z.IDSucursal=y.IDSucursal and x.idCargo=y.idCargo and y.idEmpleado=a.IDEmpleado order by y.fechaAsignacion desc limit 1),'') as 'idCargo',
            ifnull((select z.Nombre from cargos x,cargos_empleado y,sucursales z where z.IDSucursal=y.IDSucursal and x.idCargo=y.idCargo and y.idEmpleado=a.IDEmpleado order by y.fechaAsignacion desc limit 1),'') as 'nombre',
            ifnull((select z.NombreCorto from cargos x,cargos_empleado y,sucursales z where z.IDSucursal=y.IDSucursal and x.idCargo=y.idCargo and y.idEmpleado=a.IDEmpleado order by y.fechaAsignacion desc limit 1),'') as 'nombreCorto',
            ifnull((select z.idSucursal from cargos x,cargos_empleado y,sucursales z where z.IDSucursal=y.IDSucursal and x.idCargo=y.idCargo and y.idEmpleado=a.IDEmpleado order by y.fechaAsignacion desc limit 1),'') as 'idSucursal',
            ifnull((select x.valor from asignacionsueldo x where x.idEmpleado=a.IDEmpleado order by x.fechaAsignacion desc limit 1),'') as 'sueldo',
            ifnull((select x.idAsignacion from asignacionsueldo x where x.idEmpleado=a.IDEmpleado order by x.fechaAsignacion desc limit 1),'') as 'idSueldo',
            (select x.foto from fotos x where x.idEmpleado=a.IDEmpleado order by x.fecha desc limit 1) as 'foto',
            (select x.idfoto from fotos x where x.idEmpleado=a.IDEmpleado order by x.fecha desc limit 1) as 'idfoto',
            ifnull((select x.descripcion from cargos x,cargos_empleado y,sucursales z where z.IDSucursal=y.IDSucursal and x.idCargo=y.idCargo and y.idEmpleado=a.IDEmpleado order by y.fechaAsignacion desc limit 1),'') as 'des',
            ifnull((select y.IDCargoEmpleado from cargos x,cargos_empleado y,sucursales z where z.IDSucursal=y.IDSucursal and x.idCargo=y.idCargo and y.idEmpleado=a.IDEmpleado order by y.fechaAsignacion desc limit 1),'') as 'idce'
            from empleados a, departamentos b, municipios c 
            where c.idMunicipio=a.idMunicipio and b.idDepartamento =c.idDepartamento
            order by Apellidos, Nombres asc;
            ");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }

        public static DataTable TODOSLOSPRODUCTOSSUCURSAL(String idSucursal)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"select a.IDProducto ,a.NombreProducto,a.Alias,a.MedidaPorUnidad,b.precioUnitario,b.cantidad,a.costo,a.codigoBarras,
                    a.imagen,a.minimo,a.fecha,a.idCategoria,a.idProveedor,a.costo,e.NombreCategroria,d.NombreProveedor
                    from productos a, productosporsucursal b, sucursales c,proveedores d, categorias e
                    where a.idProducto=b.idProducto and b.idSucursal=c.idSucursal and d.idProveedor=a.idProveedor 
                    and e.idCategoria=a.idcategoria and c.idSucursal=" + idSucursal + ";");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }

        public static DataTable TODOSLOSPRODUCTOS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"select idProducto,NombreProducto,Alias,MedidaPorUnidad,precioUnitario,c.idCategoria,a.codigoBarras,
                        b.idProveedor,UnidadesEnStock,imagen,minimo,fecha,c.NombreCategroria,b.NombreProveedor,a.costo
                        from productos a,proveedores b,categorias c
                        where a.idCategoria=c.idCategoria and b.idProveedor=a.idProveedor order by nombreProducto asc;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }

        public static DataTable Productos_Compra(String idCompra)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT cd.idComprasDetalles, cd.idCompra, pr.idProducto, cd.cantidad, cd.precioCosto ,pr.NombreProducto,
            (SELECT UnidadesEnStock FROM productos pr2 WHERE cd.idProducto=pr2.IDProducto) as 'UnidadesEnStock'
            FROM comprasdetalles cd,productos pr WHERE cd.idProducto=pr.IDProducto and idCompra="+idCompra+";");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }

        public static DataTable TODOSLASCOMPRAS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT com.IDCompra,pr.NombreProveedor,pr.NombreContacto,pr.TelefoProveedor, 
                                pr.TelefoContacto,
                                pr.IDProveedor, FechaFactura, FechaIngreso, descripcion, AnexoDocumento
                                FROM compras com, proveedores pr
                                WHERE com.IDProveedor=pr.IDProveedor
                                ORDER BY FechaIngreso desc
                                ;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }
        public static DataTable TODASLASCATEGORIAS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"select IDCategoria,NombreCategroria,descripcion from categorias order by NombreCategroria asc;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }

        public static DataTable TODOSLOSPROVEEDORES()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"select IDProveedor,NombreProveedor,NombreContacto,TituloContacto,TelefoProveedor,
                TelefoContacto, a.Direccion ,b.municipio,b.idMunicipio,concat(c.Departamento,', ',b.Municipio) as 'dm'
                from proveedores a, municipios b, departamentos c where a.idmunicipio=b.idmunicipio and c.idDepartamento=b.idDepartamento;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }
        public static DataTable HistorialEmpleado(int tipo,String idEmpleado)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            if (tipo == 0) // Sucursal-Cargo
            {
                Sentencia.Append(@"select a.IDCargo,b.NombreCargo,e.IDSucursal,e.nombre,concat(f.Municipio,' ',e.Direcion) 
                as 'direccion',a.fechaAsignacion as 'fecha' from cargos_empleado a,cargos b,empleados c, sucursales e,municipios f, departamentos g
                where a.idEmpleado=c.idEmpleado and e.idSucursal=a.idSucursal and  a.idCargo=b.idCargo 
                and f.idMunicipio=e.idMunicipio and g.idDepartamento=f.idDepartamento and c.idEmpleado="+idEmpleado+@" order by a.fechaAsignacion,a.idCargo asc");
            }
            else
            { // Sueldo
                Sentencia.Append(@"select idAsignacion,valor,fechaAsignacion,IDEmpleado
                     from asignacionsueldo where idEmpleado="+idEmpleado+@" order by fechaAsignacion,idAsignacion asc;");
            }
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }



        public static DataTable TODOSLOSUSUARIOS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT usu.idUsuario, usu.usuario, usu.Credencial, 
            concat(emp.Nombres,' ',emp.Apellidos) as 'Empleado',ro.rol as 'Rol',usu.Estado,usu.idRol,usu.idEmpleado 
            FROM usuarios usu, empleados emp, roles ro
            WHERE usu.idEmpleado=emp.IDEmpleado and usu.idRol=ro.idRol;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }

        public static DataTable TODOSLOSROLES()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"select IDRol, Rol from Roles;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }

        public static DataTable TODOSDEPARTAMENTOS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT idDepartamento, Departamento FROM departamentos;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }

        public static DataTable TODOSMUNICIPIOS(String idDep)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT idMunicipio, Municipio FROM municipios WHERE idDepartamento="+ idDep + ";");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }

        public static DataTable PermisosUsuario(String pUsuario)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"select d.IDOpcion, d.opcion from usuarios a, permisos c, 
            opciones d where a.Usuario='" + pUsuario + @"' and
            a.IDRol = c.IDRol and c.IDOpcion = d.IDOpcion;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }
        public static DataTable TODOSLOSROLES(String pUsuario)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"select idRol, roles from roles order by roles asc;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }
        public static DataTable TODOSLOSPERMIDOS(String pUsuario)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"select a.idOpcion, a.opcion,a.clasificacion,a.clasificacion,b.idPermiso,(selec 1 from permisos x,opciones y where y.idOpcion=a.idOpcion and y.idOpcion=x.idOpcion) as 'asignado' from Opciones a, permisos b where b.idOpcion= order by roles asc;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }
        public static DataTable PERMISOSASIGNADOS(String idRol)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"select 
                ifnull((select '1' from permisos z where z.idRol="+idRol+ @" and z.idOpcion=a.idOpcion),'0') as 'Asignado',
                ifnull((select idPermisos from permisos z where z.idRol=" + idRol + @" and z.idOpcion=a.idOpcion),'') as 'idPermiso',
                a.idOpcion,
                a.opcion,
                a.Clasificacion
                from opciones a;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

        public static DataTable TODOS_CLIENTES()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT IDCliente, NombreContacto, TelefoContacto, Direccion,
            mu.Municipio, dep.Departamento, cl.idMunicipio ,dep.idDepartamento
            FROM clientes cl, municipios mu, departamentos dep
            where cl.idMunicipio=mu.idMunicipio and mu.idDepartamento=dep.idDepartamento;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

        public static DataTable TODOS_EMPLEADOS_CARGOS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT ce.IDEmpleado, concat(em.Nombres,' ', em.Apellidos)  as 'NombreEmpleado', ce.descripcion 
            FROM cargos_empleado ce, empleados em
            WHERE ce.IDEmpleado=em.IDEmpleado AND IDCargo=3;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

        public static DataTable ComprasPorID(String id)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT c.IDCompra, c.IDProveedor, c.FechaFactura, c.FechaIngreso, c.descripcion, c.AnexoDocumento,
            p.NombreProveedor,p.NombreContacto
            FROM compras c, proveedores p
            WHERE c.IDProveedor=p.IDProveedor and IDCompra="+ id + ";");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }


        public static DataTable PedidosporEstado(String ESTADO)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT p.IDPedido, p.IDCliente, p.IDSucursal, p.FechaPedido, p.FechaEntrega, p.EstadoPedido,
            c.NombreContacto as 'NombreCliente',s.Nombre as 'Sucursal', p.NumPedido
            FROM pedidos p, clientes c,sucursales s
            WHERE p.IDCliente=c.IDCliente and p.IDSucursal=s.IDSucursal and
            EstadoPedido='"+ESTADO+ "' ORDER BY p.IDPedido DESC;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }

        public static DataTable ProductosPorPedido(String idpedido)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT dp.IDPedido, dp.IDProducto, dp.Cantidad as 'cantidad', dp.IDDestallesPedido,
            pr.NombreProducto,pr.PrecioUnitario, 
            round((dp.Cantidad*pr.PrecioUnitario),2) as'Subtotal'
            FROM detallespedidos dp, productos pr
            WHERE dp.IDProducto=pr.IDProducto AND dp.IDPedido='"+idpedido+"';");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }

        public static DataTable TODOS_ENVIOS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT idEnvio, fecha_Requerido, fecha_Entrega, idRepartidor, 
            en.IdTransporte, IDPedido, en.idMunicipio, en.direccion,
            mu.Municipio, tr.TipoTransporte
            FROM envios en, municipios mu, transportes tr
            WHERE en.idMunicipio=mu.idMunicipio and en.IdTransporte= tr.IDTransporte
            ORDER BY fecha_Requerido;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }

        public static DataTable TODOS_TRANSPORTES()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT IDTransporte, NombreTransporte, TituloContacto, TelefonoTransporte,TipoTransporte 
            FROM transportes ;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }

        public static bool AgregarAStock(String idProducto,String Cant)
        {
            Boolean Guardado = false;
            int tot = UnidadesStock(idProducto) + Convert.ToInt32( Cant);
            String Sentencia = "UPDATE productos SET UnidadesEnStock='"+ tot + "' WHERE IDProducto='"+ idProducto + "';";
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            MessageBox.Show(Sentencia.ToString());
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;

            }
            return Guardado;
        }

        public static bool DescontarStock(String idProducto, String Cant)
        {
            Boolean Guardado = false;
            int tot = UnidadesStock(idProducto) - Convert.ToInt32(Cant);
            String Sentencia = "UPDATE productos SET UnidadesEnStock='"+tot+"' WHERE `IDProducto`='"+idProducto+"';";
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            MessageBox.Show(Sentencia.ToString());
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;

            }
            return Guardado;
        }

        public static int UnidadesStock(String idpRO)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT UnidadesEnStock FROM productos WHERE IDProducto='"+ idpRO + "';");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Convert.ToInt32(Resultado.Rows[0][0].ToString());
        }

        public static void AlgoritmoEliminacionDetalesPedidos(String idPedido)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT dp.IDProducto, dp.Cantidad FROM detallespedidos dp
            WHERE dp.IDPedido="+idPedido+";");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            for (int i=0;i < Resultado.Rows.Count;i++) {
                AgregarAStock(Resultado.Rows[i][0].ToString(), Resultado.Rows[i][1].ToString());
                
            }
        }

        public static bool ActualizarEstadoPedido(String idPedido, String Esatdo)
        {
            Boolean Guardado = false;
            String Sentencia = "UPDATE pedidos SET EstadoPedido='"+ Esatdo + "' WHERE IDPedido='"+ idPedido + "';";
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            MessageBox.Show(Sentencia.ToString());
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;

            }
            return Guardado;
        }

        public static String NuevoNumeroPedido() {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT NumPedido FROM pedidos ORDER BY NumPedido desc limit 0,1;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            int valor = Convert.ToInt32(Resultado.Rows[0][0].ToString())+1;
            if (NuevoNumeroPedidoYE(valor))
            {
                valor= valor + 1;
            }
            return valor+"";
        }

        public static Boolean NuevoNumeroPedidoYE(int num)
        {
            DataTable Resultado = new DataTable();
            Boolean valor = false;
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT NumPedido FROM pedidos WHERE NumPedido="+num+";");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            if (Resultado.Rows.Count > 0)
            {
                valor = true;
            }
            
            return valor;
        }

        public static String IDPedidoNumero(String numpedido)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT IDPedido FROM pedidos WHERE NumPedido="+ numpedido + ";");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado.Rows[0][0].ToString();
        }

    }
}