﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DBManager.CLS;

namespace CacheManager
{
   public class Operaciones
    {
        public static void SoloLetras(KeyPressEventArgs evento)
        {
            if (Char.IsLetter(evento.KeyChar))
            {
                evento.Handled = false;
            }
            else if (Char.IsSeparator(evento.KeyChar))
            {
                evento.Handled = false;
            }
            else if (Char.IsControl(evento.KeyChar))
            {
                evento.Handled = false;
            }
            else
            {
                evento.Handled = true;
            }
        }

        public static void CargarImagenSO(PictureBox MyPictureb)
        {
            OpenFileDialog opf = new OpenFileDialog();
            opf.Filter = "Archivo de Imagen |*.jpg| Archivo PNG|*.png| PDF |*.pdf";
            try
            {
                DialogResult resultado = opf.ShowDialog();
                if (resultado == DialogResult.OK)
                {
                    MyPictureb.Image = Image.FromFile(opf.FileName);
                }
            }
            catch
            {
                MessageBox.Show("Error al intentar Cargar la imagen");
            }
        }


        public static byte[] PrepararImagenParaGuardar(PictureBox miPictureBox)
        {
            byte[] resultado = null;
            MemoryStream memoryStream = new MemoryStream();
            try
            {
                miPictureBox.Image.Save(memoryStream,miPictureBox.Image.RawFormat);
                resultado = memoryStream.ToArray();
            }
            catch
            {
                MessageBox.Show("Entro al catch");
                resultado = null;
            }
            return resultado;
        }

        // Cuando ya tengo cargado en un objeto de tipo Empleado por Ej. el byte[] que contiene la imagen :V
        public static void MostrarImagen(PictureBox miPictureBox, byte[] img)
        {
            try
            {
                MemoryStream mest = new MemoryStream(img);
                miPictureBox.Image = Image.FromStream(mest);
            }
            catch(Exception e) {
                MessageBox.Show(e.StackTrace);
            }
        }

        public static MemoryStream PrepararImagenParaMostrar(byte[] img)
        {
            try
            {
                return new MemoryStream(img);
            }
            catch
            {
                return null;
            }
        }

        public static void SoloNumeros(KeyPressEventArgs evento)
        {
            if (Char.IsNumber(evento.KeyChar))
            {
                evento.Handled = false;
            }
            else if (Char.IsSeparator(evento.KeyChar))
            {
                evento.Handled = false;
            }
            else if (Char.IsControl(evento.KeyChar))
            {
                evento.Handled = false;
            }
            else
            {
                evento.Handled = true;
            }
        }

        public static void SoloNumerosSinEspacio(KeyPressEventArgs evento)
        {
            if (Char.IsDigit(evento.KeyChar))
            {
                evento.Handled = false;
            }
            else if (Char.IsControl(evento.KeyChar))
            {
                evento.Handled = false;
            }
            else
            {
                evento.Handled = true;
            }
        }

        public static void SinCaracteresEspeciales(KeyPressEventArgs evento)
        {
            if (Char.IsDigit(evento.KeyChar))
            {
                evento.Handled = false;
            }
            else if (Char.IsControl(evento.KeyChar))
            {
                evento.Handled = false;
            }
            else if (Char.IsLetter(evento.KeyChar))
            {
                evento.Handled = false;
            }
            else
            {
                evento.Handled = true;
            }
        }

        public static byte[] ObjetoAByteArray(object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        public static ArrayList LlenarComboBox(ComboBox myCombo,String PK_DB, String OtroCampo,String tabla){
            ArrayList resultado = new ArrayList();
            StringBuilder con = new StringBuilder();
            con.Append(@"select "+PK_DB+","+OtroCampo+" from "+tabla);
            DBManager.CLS.DBOperacion cargar = new DBManager.CLS.DBOperacion();
            try
            {
                DataTable data = cargar.Consultar(con.ToString());
                if (data.Rows.Count > 0)
                {
                    for (int i=0;i<data.Rows.Count;i++) {
                        resultado.Add(data.Rows[i][0].ToString());
                        myCombo.Items.Add(data.Rows[i][1].ToString());
                    }
                }
                else {
                    resultado = null;
                }
            }
            catch {
                Console.Write("ERROR AL CARGAR COMBO");
            }
            return resultado; 
        }

        public static ArrayList LlenarComboBox(ComboBox myCombo, String PK_DB, String OtroCampo, String tabla,String condicion)
        {
            ArrayList resultado = new ArrayList();
            StringBuilder con = new StringBuilder();
            con.Append(@"select " + PK_DB + "," + OtroCampo + " from " + tabla);
            DBManager.CLS.DBOperacion cargar = new DBManager.CLS.DBOperacion();
            try
            {
                DataTable data = cargar.Consultar(con.ToString());
                if (data.Rows.Count > 0)
                {
                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        resultado.Add(data.Rows[i][0].ToString());
                        myCombo.Items.Add(data.Rows[i][1].ToString());
                    }
                }
                else
                {
                    resultado = null;
                }
            }
            catch
            {
                Console.Write("ERROR AL CARGAR COMBO");
            }
            return resultado;
        }

        public static void SinCaracteresEspecialesConEspacio(KeyPressEventArgs evento)
        {
            if (Char.IsDigit(evento.KeyChar))
            {
                evento.Handled = false;
            }
            else if (Char.IsControl(evento.KeyChar))
            {
                evento.Handled = false;
            }
            else if (Char.IsLetter(evento.KeyChar))
            {
                evento.Handled = false;
            }
            else if (Char.IsSeparator(evento.KeyChar))
            {
                evento.Handled = false;
            }
            else
            {
                evento.Handled = true;
            }
        }

        public static void NumerosDecimales(KeyPressEventArgs evento)
        {
            if (Char.IsDigit(evento.KeyChar) || (int)evento.KeyChar == 46)
            {
                evento.Handled = false;
            }
            else if (Char.IsControl(evento.KeyChar))
            {
                evento.Handled = false;
            }
            else
            {
                evento.Handled = true;
            }
        }

        public static Boolean Enter(KeyPressEventArgs evento)
        {
            bool re = false;
            if ((int)evento.KeyChar == (int)Keys.Enter)
            {
                re = true;
            }
            return re;
        }

        public static byte[] Convertirbyte(String RutaFile){
            try { 
            byte[] arreglo = File.ReadAllBytes(RutaFile);
                return arreglo;
            }
            catch { 
            return null;
            }
        }

        public static String GuardarCopia(String RutaG,String NameFile,byte[] Arreglo)
        {
            try{
            String rutaSalida = RutaG+ "\\" + NameFile;
            File.WriteAllBytes(rutaSalida, Arreglo);
                return rutaSalida;
            }
            catch { 
                 return null;
            }
        }
        public static void VerArchivo(String Ruta)
        {
            Process pro = new Process();
            pro.StartInfo.FileName = Ruta;
            pro.Start();
        }

        public static void EliminarArchivo(String Ruta)
        {
            File.Delete(Ruta);
        }

    }
}
