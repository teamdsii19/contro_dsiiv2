﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBManager.Properties;

namespace DBManager.CLS
{
    public class AppConfiguration
    {
        public static void EstablecerValor(String campo, String valor)
        {
            // Crear el objeto de configuracion
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            // Borrar la configuracion Actual
            config.AppSettings.Settings.Remove(campo);
            //Guardo
            config.Save(ConfigurationSaveMode.Modified);
            //Forzar la recarga de la seleccion
            ConfigurationManager.RefreshSection("appSettings");
            //Guardar la config.
            config.AppSettings.Settings.Add(campo, valor);
            //Guardo
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

        public static String RecuperarValor(String campo, String defecto)
        {
            String valor = ConfigurationManager.AppSettings[campo];
            if (valor == null)
            {
                valor = defecto;
            }
            return valor;
        }
        public static bool RecuperarValor(String campo, bool defecto)
        {
            bool valor =Convert.ToBoolean (ConfigurationManager.AppSettings[campo]);
            if (valor == null)
            {
                valor = defecto;
            }
            return valor;
        }

        /// Encripta una cadena
        public static string Encriptar(string _cadenaAencriptar)
        {
            string result = string.Empty;
            try
            {
                byte[] encryted = System.Text.Encoding.Unicode.GetBytes(_cadenaAencriptar);
                result = Convert.ToBase64String(encryted);
            }
            catch
            {
                Console.Write("Error al encriptar ............");
            }
            return result;
        }

        //          String CadenaConexion="Server=localhost;Port=3306;Database=sistema;Uid=root;Pwd=admin";
        public static String CrearCadenaEncriptadaMYSQL(String server, String port, String DB, String User, String Pass)
        {
            String result = "";
            StringBuilder cadena = new StringBuilder();
            cadena.Append(@"Server=" + server + @";Port=" + port + @";Database=" + DB + @";Uid=" + User + ";Pwd=" + Pass);
            result = Encriptar(cadena.ToString());
            return result;
        }

        public static string DesEncriptar(string _cadenaAdesencriptar)
        {
            string result = string.Empty;
            try
            {
                byte[] decryted = Convert.FromBase64String(_cadenaAdesencriptar);
                result = System.Text.Encoding.Unicode.GetString(decryted);
            }
            catch
            {
                Console.Write("Erroraso ............ al desencriptar");
            }
            return result;
        }

        public static String RecuperarValorUsarioConfig(String campo, String PorDefecto)
        {
            String resul = PorDefecto;
            try
            {
                resul = (String)Settings.Default[campo];
            }
            catch
            {
                resul = PorDefecto;
            }
            return resul;
        }

        public static Boolean RecuperarValorUsarioConfig(String campo, Boolean PorDefecto)
        {
            Boolean resul = PorDefecto;
            try
            {
                resul = (Boolean)Settings.Default[campo];
            }
            catch
            {
                resul = PorDefecto;
            }
            return resul;
        }

        public static int RecuperarValorUsarioConfig(String campo, int PorDefecto)
        {
            int resul = PorDefecto;
            try
            {
                resul = (int)Settings.Default[campo];
            }
            catch
            {
                resul = PorDefecto;
            }
            return resul;
        }

        public static double RecuperarValorUsarioConfig(String campo, double PorDefecto)
        {
            double resul = PorDefecto;
            try
            {
                resul = (double)Settings.Default[campo];
            }
            catch
            {
                resul = PorDefecto;
            }
            return resul;
        }

        public static Color RecuperarValorUsarioConfig(String campo, Color PorDefecto)
        {
            Color resul = PorDefecto;
            try
            {
                resul = (Color)Settings.Default[campo];
            }
            catch
            {
                resul = PorDefecto;
            }
            return resul;
        }

        public static void EstablecerValorUsarioConfig(String campo, String valor)
        {
            Settings.Default[campo] = valor;
            Settings.Default.Save();
        }

        public static void EstablecerValorUsarioConfig(String campo, Boolean valor)
        {
            Settings.Default[campo] = valor;
            Settings.Default.Save();
        }

        public static void EstablecerValorUsarioConfig(String campo, int valor)
        {
            Settings.Default[campo] = valor;
            Settings.Default.Save();
        }

        public static void EstablecerValorUsarioConfig(String campo, double valor)
        {
            Settings.Default[campo] = valor;
            Settings.Default.Save();
        }

        public static void EstablecerValorUsarioConfig(String campo, Color valor)
        {
            Settings.Default[campo] = valor;
            Settings.Default.Save();
        }
    }
}
