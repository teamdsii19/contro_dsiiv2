﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;

namespace DBManager.CLS
{
    public class DBOperacion:DBConexion
    {
        public DataTable Consultar(String pConsulta)
        {
            return EjecutarConsulta(pConsulta);
        }

        public Int32 Insertar(String pSentencia)
        {
            return EjecutarSentencia(pSentencia);
        }
        public Int32 Actualizar(String pSentencia)
        {
            return EjecutarSentencia(pSentencia);
        }

        public Int32 GuardarFoto(String idEmpleado, byte[] img)
        {
            int FilasAfectadas = 0;
            if (base.Conectar())
            {
                MySqlCommand Comando = new MySqlCommand("insert into fotos (idEmpleado,fecha,foto) values(@idEmpleado,@fecha,@foto);", base.oConexion);
                //                Comando.CommandText = query;
                //              Comando.Connection = base.oConexion;
                Comando.Parameters.Add("@idEmpleado", MySqlDbType.UInt32).Value = Convert.ToUInt32(idEmpleado);
                Comando.Parameters.Add("@fecha", MySqlDbType.Date).Value = DateTime.Today;
                Comando.Parameters.Add("@foto", MySqlDbType.Blob).Value = img;
                MessageBox.Show(Comando.CommandText);
                FilasAfectadas = Comando.ExecuteNonQuery();
                base.Desconectar();
            }
            return FilasAfectadas;
        }

        public Int32 GuardarGenerico(MySqlCommand paraEjecucion)
        {
            int FilasAfectadas = 0;
            if (base.Conectar())
            {
                paraEjecucion.Connection = base.oConexion;
                FilasAfectadas = paraEjecucion.ExecuteNonQuery();
                base.Desconectar();
            }
            return FilasAfectadas;
        }

        public Int32 ActualizarGenerico(MySqlCommand paraEjecucion)
        {
            int FilasAfectadas = 0;
            if (base.Conectar())
            {
                paraEjecucion.Connection = base.oConexion;
                FilasAfectadas = paraEjecucion.ExecuteNonQuery();
                base.Desconectar();
            }
            return FilasAfectadas;
        }

        public Int32 ActualizarFoto(String idEmpleado, byte[] img)
        {
            int FilasAfectadas = 0;
            if (base.Conectar())
            {
                MySqlCommand Comando = new MySqlCommand("update fotos set fecha =@fecha,foto=@foto where idEmpleado=" + idEmpleado, base.oConexion);
                Comando.Parameters.Add("@fecha", MySqlDbType.Date).Value = DateTime.Today;
                Comando.Parameters.Add("@foto", MySqlDbType.Blob).Value = img;
                MessageBox.Show(Comando.CommandText);
                FilasAfectadas = Comando.ExecuteNonQuery();
                base.Desconectar();
            }
            return FilasAfectadas;
        }

        public Int32 Eliminar(String pSentencia)
        {
            return EjecutarSentencia(pSentencia);
        }

        private Int32 EjecutarSentencia(String pSentencia)
        {
            Int32 FilasAfectadas = 0;
            if(base.Conectar())
            {
                MySqlCommand Comando = new MySqlCommand();
                Comando.CommandText = pSentencia;
                Comando.Connection = base.oConexion;
                FilasAfectadas=Comando.ExecuteNonQuery();
                base.Desconectar();
            }
            return FilasAfectadas;
        }

        private DataTable EjecutarConsulta(String pConsulta)
        {
            DataTable Resultado = new DataTable();
            if(base.Conectar())
            {
                MySqlDataAdapter Adaptador = new MySqlDataAdapter();
                MySqlCommand Comando = new MySqlCommand();
                Comando.CommandText = pConsulta;
                Comando.Connection = base.oConexion;
                Adaptador.SelectCommand = Comando;
                Adaptador.Fill(Resultado);
                base.Desconectar();
            }
            return Resultado;
        }
    }
}
